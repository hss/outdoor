PGDMP     
                     x            redmine    9.6.16 %   10.10 (Ubuntu 10.10-0ubuntu0.18.04.1) <   �           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                       false            �           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                       false            �           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                       false            �           1262    16385    redmine    DATABASE     e   CREATE DATABASE redmine WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'C' LC_CTYPE = 'C';
    DROP DATABASE redmine;
             redmine    false                        2615    2200    public    SCHEMA        CREATE SCHEMA public;
    DROP SCHEMA public;
             postgres    false            �           0    0    SCHEMA public    COMMENT     6   COMMENT ON SCHEMA public IS 'standard public schema';
                  postgres    false    3                        3079    12744    plpgsql 	   EXTENSION     ?   CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;
    DROP EXTENSION plpgsql;
                  false            �           0    0    EXTENSION plpgsql    COMMENT     @   COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';
                       false    1            �            1259    24584    ar_internal_metadata    TABLE     �   CREATE TABLE public.ar_internal_metadata (
    key character varying NOT NULL,
    value character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);
 (   DROP TABLE public.ar_internal_metadata;
       public         redmine    false    3            �            1259    24594    attachments    TABLE     �  CREATE TABLE public.attachments (
    id integer NOT NULL,
    container_id integer,
    container_type character varying(30),
    filename character varying DEFAULT ''::character varying NOT NULL,
    disk_filename character varying DEFAULT ''::character varying NOT NULL,
    filesize bigint DEFAULT 0 NOT NULL,
    content_type character varying DEFAULT ''::character varying,
    digest character varying(64) DEFAULT ''::character varying NOT NULL,
    downloads integer DEFAULT 0 NOT NULL,
    author_id integer DEFAULT 0 NOT NULL,
    created_on timestamp without time zone,
    description character varying,
    disk_directory character varying
);
    DROP TABLE public.attachments;
       public         redmine    false    3            �            1259    24592    attachments_id_seq    SEQUENCE     {   CREATE SEQUENCE public.attachments_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE public.attachments_id_seq;
       public       redmine    false    3    188            �           0    0    attachments_id_seq    SEQUENCE OWNED BY     I   ALTER SEQUENCE public.attachments_id_seq OWNED BY public.attachments.id;
            public       redmine    false    187            �            1259    24614    auth_sources    TABLE     �  CREATE TABLE public.auth_sources (
    id integer NOT NULL,
    type character varying(30) DEFAULT ''::character varying NOT NULL,
    name character varying(60) DEFAULT ''::character varying NOT NULL,
    host character varying(60),
    port integer,
    account character varying,
    account_password character varying DEFAULT ''::character varying,
    base_dn character varying(255),
    attr_login character varying(30),
    attr_firstname character varying(30),
    attr_lastname character varying(30),
    attr_mail character varying(30),
    onthefly_register boolean DEFAULT false NOT NULL,
    tls boolean DEFAULT false NOT NULL,
    filter text,
    timeout integer,
    verify_peer boolean DEFAULT true NOT NULL
);
     DROP TABLE public.auth_sources;
       public         redmine    false    3            �            1259    24612    auth_sources_id_seq    SEQUENCE     |   CREATE SEQUENCE public.auth_sources_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE public.auth_sources_id_seq;
       public       redmine    false    190    3            �           0    0    auth_sources_id_seq    SEQUENCE OWNED BY     K   ALTER SEQUENCE public.auth_sources_id_seq OWNED BY public.auth_sources.id;
            public       redmine    false    189                       1259    25176    boards    TABLE     g  CREATE TABLE public.boards (
    id integer NOT NULL,
    project_id integer NOT NULL,
    name character varying DEFAULT ''::character varying NOT NULL,
    description character varying,
    "position" integer,
    topics_count integer DEFAULT 0 NOT NULL,
    messages_count integer DEFAULT 0 NOT NULL,
    last_message_id integer,
    parent_id integer
);
    DROP TABLE public.boards;
       public         redmine    false    3                       1259    25174    boards_id_seq    SEQUENCE     v   CREATE SEQUENCE public.boards_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 $   DROP SEQUENCE public.boards_id_seq;
       public       redmine    false    259    3            �           0    0    boards_id_seq    SEQUENCE OWNED BY     ?   ALTER SEQUENCE public.boards_id_seq OWNED BY public.boards.id;
            public       redmine    false    258            �            1259    25123    changes    TABLE     3  CREATE TABLE public.changes (
    id integer NOT NULL,
    changeset_id integer NOT NULL,
    action character varying(1) DEFAULT ''::character varying NOT NULL,
    path text NOT NULL,
    from_path text,
    from_revision character varying,
    revision character varying,
    branch character varying
);
    DROP TABLE public.changes;
       public         redmine    false    3            �            1259    25121    changes_id_seq    SEQUENCE     w   CREATE SEQUENCE public.changes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE public.changes_id_seq;
       public       redmine    false    3    252            �           0    0    changes_id_seq    SEQUENCE OWNED BY     A   ALTER SEQUENCE public.changes_id_seq OWNED BY public.changes.id;
            public       redmine    false    251                       1259    25591    changeset_parents    TABLE     m   CREATE TABLE public.changeset_parents (
    changeset_id integer NOT NULL,
    parent_id integer NOT NULL
);
 %   DROP TABLE public.changeset_parents;
       public         redmine    false    3            �            1259    25111 
   changesets    TABLE     <  CREATE TABLE public.changesets (
    id integer NOT NULL,
    repository_id integer NOT NULL,
    revision character varying NOT NULL,
    committer character varying,
    committed_on timestamp without time zone NOT NULL,
    comments text,
    commit_date date,
    scmid character varying,
    user_id integer
);
    DROP TABLE public.changesets;
       public         redmine    false    3            �            1259    25109    changesets_id_seq    SEQUENCE     z   CREATE SEQUENCE public.changesets_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE public.changesets_id_seq;
       public       redmine    false    250    3            �           0    0    changesets_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE public.changesets_id_seq OWNED BY public.changesets.id;
            public       redmine    false    249            �            1259    25156    changesets_issues    TABLE     l   CREATE TABLE public.changesets_issues (
    changeset_id integer NOT NULL,
    issue_id integer NOT NULL
);
 %   DROP TABLE public.changesets_issues;
       public         redmine    false    3            �            1259    24949    comments    TABLE     ^  CREATE TABLE public.comments (
    id integer NOT NULL,
    commented_type character varying(30) DEFAULT ''::character varying NOT NULL,
    commented_id integer DEFAULT 0 NOT NULL,
    author_id integer DEFAULT 0 NOT NULL,
    content text,
    created_on timestamp without time zone NOT NULL,
    updated_on timestamp without time zone NOT NULL
);
    DROP TABLE public.comments;
       public         redmine    false    3            �            1259    24947    comments_id_seq    SEQUENCE     x   CREATE SEQUENCE public.comments_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE public.comments_id_seq;
       public       redmine    false    232    3            �           0    0    comments_id_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE public.comments_id_seq OWNED BY public.comments.id;
            public       redmine    false    231                       1259    25775    custom_field_enumerations    TABLE     �   CREATE TABLE public.custom_field_enumerations (
    id integer NOT NULL,
    custom_field_id integer NOT NULL,
    name character varying NOT NULL,
    active boolean DEFAULT true NOT NULL,
    "position" integer DEFAULT 1 NOT NULL
);
 -   DROP TABLE public.custom_field_enumerations;
       public         redmine    false    3                       1259    25773     custom_field_enumerations_id_seq    SEQUENCE     �   CREATE SEQUENCE public.custom_field_enumerations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 7   DROP SEQUENCE public.custom_field_enumerations_id_seq;
       public       redmine    false    3    285            �           0    0     custom_field_enumerations_id_seq    SEQUENCE OWNED BY     e   ALTER SEQUENCE public.custom_field_enumerations_id_seq OWNED BY public.custom_field_enumerations.id;
            public       redmine    false    284            �            1259    24628    custom_fields    TABLE     F  CREATE TABLE public.custom_fields (
    id integer NOT NULL,
    type character varying(30) DEFAULT ''::character varying NOT NULL,
    name character varying(30) DEFAULT ''::character varying NOT NULL,
    field_format character varying(30) DEFAULT ''::character varying NOT NULL,
    possible_values text,
    regexp character varying DEFAULT ''::character varying,
    min_length integer,
    max_length integer,
    is_required boolean DEFAULT false NOT NULL,
    is_for_all boolean DEFAULT false NOT NULL,
    is_filter boolean DEFAULT false NOT NULL,
    "position" integer,
    searchable boolean DEFAULT false,
    default_value text,
    editable boolean DEFAULT true,
    visible boolean DEFAULT true NOT NULL,
    multiple boolean DEFAULT false,
    format_store text,
    description text,
    dmsf_not_inheritable boolean
);
 !   DROP TABLE public.custom_fields;
       public         redmine    false    3            �            1259    24626    custom_fields_id_seq    SEQUENCE     }   CREATE SEQUENCE public.custom_fields_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.custom_fields_id_seq;
       public       redmine    false    192    3            �           0    0    custom_fields_id_seq    SEQUENCE OWNED BY     M   ALTER SEQUENCE public.custom_fields_id_seq OWNED BY public.custom_fields.id;
            public       redmine    false    191            �            1259    24645    custom_fields_projects    TABLE     �   CREATE TABLE public.custom_fields_projects (
    custom_field_id integer DEFAULT 0 NOT NULL,
    project_id integer DEFAULT 0 NOT NULL
);
 *   DROP TABLE public.custom_fields_projects;
       public         redmine    false    3                       1259    25666    custom_fields_roles    TABLE     p   CREATE TABLE public.custom_fields_roles (
    custom_field_id integer NOT NULL,
    role_id integer NOT NULL
);
 '   DROP TABLE public.custom_fields_roles;
       public         redmine    false    3            �            1259    24650    custom_fields_trackers    TABLE     �   CREATE TABLE public.custom_fields_trackers (
    custom_field_id integer DEFAULT 0 NOT NULL,
    tracker_id integer DEFAULT 0 NOT NULL
);
 *   DROP TABLE public.custom_fields_trackers;
       public         redmine    false    3            �            1259    24657    custom_values    TABLE     �   CREATE TABLE public.custom_values (
    id integer NOT NULL,
    customized_type character varying(30) DEFAULT ''::character varying NOT NULL,
    customized_id integer DEFAULT 0 NOT NULL,
    custom_field_id integer DEFAULT 0 NOT NULL,
    value text
);
 !   DROP TABLE public.custom_values;
       public         redmine    false    3            �            1259    24655    custom_values_id_seq    SEQUENCE     }   CREATE SEQUENCE public.custom_values_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.custom_values_id_seq;
       public       redmine    false    196    3            �           0    0    custom_values_id_seq    SEQUENCE OWNED BY     M   ALTER SEQUENCE public.custom_values_id_seq OWNED BY public.custom_values.id;
            public       redmine    false    195            '           1259    27373    dmsf_file_revision_accesses    TABLE     (  CREATE TABLE public.dmsf_file_revision_accesses (
    id integer NOT NULL,
    dmsf_file_revision_id integer NOT NULL,
    action integer DEFAULT 0 NOT NULL,
    user_id integer NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);
 /   DROP TABLE public.dmsf_file_revision_accesses;
       public         redmine    false    3            &           1259    27371 "   dmsf_file_revision_accesses_id_seq    SEQUENCE     �   CREATE SEQUENCE public.dmsf_file_revision_accesses_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 9   DROP SEQUENCE public.dmsf_file_revision_accesses_id_seq;
       public       redmine    false    295    3            �           0    0 "   dmsf_file_revision_accesses_id_seq    SEQUENCE OWNED BY     i   ALTER SEQUENCE public.dmsf_file_revision_accesses_id_seq OWNED BY public.dmsf_file_revision_accesses.id;
            public       redmine    false    294            #           1259    27344    dmsf_file_revisions    TABLE     �  CREATE TABLE public.dmsf_file_revisions (
    id integer NOT NULL,
    dmsf_file_id integer NOT NULL,
    source_dmsf_file_revision_id integer,
    name character varying NOT NULL,
    disk_filename character varying NOT NULL,
    size bigint,
    mime_type character varying,
    title character varying NOT NULL,
    description text,
    workflow integer,
    major_version integer NOT NULL,
    minor_version integer NOT NULL,
    comment text,
    deleted integer DEFAULT 0 NOT NULL,
    deleted_by_user_id integer,
    user_id integer NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    dmsf_workflow_id integer,
    dmsf_workflow_assigned_by_user_id integer,
    dmsf_workflow_assigned_at timestamp without time zone,
    dmsf_workflow_started_by_user_id integer,
    dmsf_workflow_started_at timestamp without time zone,
    digest character varying(64) DEFAULT ''::character varying NOT NULL
);
 '   DROP TABLE public.dmsf_file_revisions;
       public         redmine    false    3            "           1259    27342    dmsf_file_revisions_id_seq    SEQUENCE     �   CREATE SEQUENCE public.dmsf_file_revisions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 1   DROP SEQUENCE public.dmsf_file_revisions_id_seq;
       public       redmine    false    3    291            �           0    0    dmsf_file_revisions_id_seq    SEQUENCE OWNED BY     Y   ALTER SEQUENCE public.dmsf_file_revisions_id_seq OWNED BY public.dmsf_file_revisions.id;
            public       redmine    false    290            !           1259    27331 
   dmsf_files    TABLE     x  CREATE TABLE public.dmsf_files (
    id integer NOT NULL,
    project_id integer NOT NULL,
    dmsf_folder_id integer,
    name character varying NOT NULL,
    notification boolean DEFAULT false,
    deleted integer DEFAULT 0 NOT NULL,
    deleted_by_user_id integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);
    DROP TABLE public.dmsf_files;
       public         redmine    false    3                        1259    27329    dmsf_files_id_seq    SEQUENCE     z   CREATE SEQUENCE public.dmsf_files_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE public.dmsf_files_id_seq;
       public       redmine    false    3    289            �           0    0    dmsf_files_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE public.dmsf_files_id_seq OWNED BY public.dmsf_files.id;
            public       redmine    false    288            5           1259    27541    dmsf_folder_permissions    TABLE     �   CREATE TABLE public.dmsf_folder_permissions (
    id integer NOT NULL,
    dmsf_folder_id integer,
    object_id integer NOT NULL,
    object_type character varying(30) NOT NULL
);
 +   DROP TABLE public.dmsf_folder_permissions;
       public         redmine    false    3            4           1259    27539    dmsf_folder_permissions_id_seq    SEQUENCE     �   CREATE SEQUENCE public.dmsf_folder_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 5   DROP SEQUENCE public.dmsf_folder_permissions_id_seq;
       public       redmine    false    309    3            �           0    0    dmsf_folder_permissions_id_seq    SEQUENCE OWNED BY     a   ALTER SEQUENCE public.dmsf_folder_permissions_id_seq OWNED BY public.dmsf_folder_permissions.id;
            public       redmine    false    308                       1259    27319    dmsf_folders    TABLE     �  CREATE TABLE public.dmsf_folders (
    id integer NOT NULL,
    project_id integer NOT NULL,
    dmsf_folder_id integer,
    title character varying NOT NULL,
    description text,
    notification boolean DEFAULT false,
    user_id integer NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    deleted integer DEFAULT 0 NOT NULL,
    deleted_by_user_id integer,
    system boolean DEFAULT false NOT NULL
);
     DROP TABLE public.dmsf_folders;
       public         redmine    false    3                       1259    27317    dmsf_folders_id_seq    SEQUENCE     |   CREATE SEQUENCE public.dmsf_folders_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE public.dmsf_folders_id_seq;
       public       redmine    false    287    3            �           0    0    dmsf_folders_id_seq    SEQUENCE OWNED BY     K   ALTER SEQUENCE public.dmsf_folders_id_seq OWNED BY public.dmsf_folders.id;
            public       redmine    false    286            1           1259    27428 
   dmsf_links    TABLE     �  CREATE TABLE public.dmsf_links (
    id integer NOT NULL,
    target_project_id integer NOT NULL,
    target_id integer,
    target_type character varying(10) NOT NULL,
    name character varying NOT NULL,
    project_id integer NOT NULL,
    dmsf_folder_id integer,
    deleted integer DEFAULT 0 NOT NULL,
    deleted_by_user_id integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    external_url character varying,
    user_id integer
);
    DROP TABLE public.dmsf_links;
       public         redmine    false    3            0           1259    27426    dmsf_links_id_seq    SEQUENCE     z   CREATE SEQUENCE public.dmsf_links_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE public.dmsf_links_id_seq;
       public       redmine    false    3    305            �           0    0    dmsf_links_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE public.dmsf_links_id_seq OWNED BY public.dmsf_links.id;
            public       redmine    false    304            %           1259    27356 
   dmsf_locks    TABLE     �  CREATE TABLE public.dmsf_locks (
    id integer NOT NULL,
    entity_id integer NOT NULL,
    user_id integer NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    entity_type integer NOT NULL,
    lock_type_cd integer NOT NULL,
    lock_scope_cd integer NOT NULL,
    uuid character varying(36),
    expires_at timestamp without time zone,
    dmsf_file_last_revision_id integer
);
    DROP TABLE public.dmsf_locks;
       public         redmine    false    3            $           1259    27354    dmsf_locks_id_seq    SEQUENCE     z   CREATE SEQUENCE public.dmsf_locks_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE public.dmsf_locks_id_seq;
       public       redmine    false    293    3            �           0    0    dmsf_locks_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE public.dmsf_locks_id_seq OWNED BY public.dmsf_locks.id;
            public       redmine    false    292            3           1259    27523    dmsf_public_urls    TABLE     9  CREATE TABLE public.dmsf_public_urls (
    id integer NOT NULL,
    token character varying(32) NOT NULL,
    dmsf_file_id integer NOT NULL,
    user_id integer NOT NULL,
    expire_at timestamp without time zone NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);
 $   DROP TABLE public.dmsf_public_urls;
       public         redmine    false    3            2           1259    27521    dmsf_public_urls_id_seq    SEQUENCE     �   CREATE SEQUENCE public.dmsf_public_urls_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public.dmsf_public_urls_id_seq;
       public       redmine    false    3    307            �           0    0    dmsf_public_urls_id_seq    SEQUENCE OWNED BY     S   ALTER SEQUENCE public.dmsf_public_urls_id_seq OWNED BY public.dmsf_public_urls.id;
            public       redmine    false    306            /           1259    27412    dmsf_workflow_step_actions    TABLE       CREATE TABLE public.dmsf_workflow_step_actions (
    id integer NOT NULL,
    dmsf_workflow_step_assignment_id integer NOT NULL,
    action integer NOT NULL,
    note text,
    created_at timestamp without time zone NOT NULL,
    author_id integer NOT NULL
);
 .   DROP TABLE public.dmsf_workflow_step_actions;
       public         redmine    false    3            .           1259    27410 !   dmsf_workflow_step_actions_id_seq    SEQUENCE     �   CREATE SEQUENCE public.dmsf_workflow_step_actions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 8   DROP SEQUENCE public.dmsf_workflow_step_actions_id_seq;
       public       redmine    false    3    303            �           0    0 !   dmsf_workflow_step_actions_id_seq    SEQUENCE OWNED BY     g   ALTER SEQUENCE public.dmsf_workflow_step_actions_id_seq OWNED BY public.dmsf_workflow_step_actions.id;
            public       redmine    false    302            -           1259    27403    dmsf_workflow_step_assignments    TABLE     �   CREATE TABLE public.dmsf_workflow_step_assignments (
    id integer NOT NULL,
    dmsf_workflow_step_id integer NOT NULL,
    user_id integer NOT NULL,
    dmsf_file_revision_id integer NOT NULL
);
 2   DROP TABLE public.dmsf_workflow_step_assignments;
       public         redmine    false    3            ,           1259    27401 %   dmsf_workflow_step_assignments_id_seq    SEQUENCE     �   CREATE SEQUENCE public.dmsf_workflow_step_assignments_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 <   DROP SEQUENCE public.dmsf_workflow_step_assignments_id_seq;
       public       redmine    false    3    301            �           0    0 %   dmsf_workflow_step_assignments_id_seq    SEQUENCE OWNED BY     o   ALTER SEQUENCE public.dmsf_workflow_step_assignments_id_seq OWNED BY public.dmsf_workflow_step_assignments.id;
            public       redmine    false    300            +           1259    27394    dmsf_workflow_steps    TABLE     �   CREATE TABLE public.dmsf_workflow_steps (
    id integer NOT NULL,
    dmsf_workflow_id integer NOT NULL,
    step integer NOT NULL,
    user_id integer NOT NULL,
    operator integer NOT NULL,
    name character varying(30)
);
 '   DROP TABLE public.dmsf_workflow_steps;
       public         redmine    false    3            *           1259    27392    dmsf_workflow_steps_id_seq    SEQUENCE     �   CREATE SEQUENCE public.dmsf_workflow_steps_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 1   DROP SEQUENCE public.dmsf_workflow_steps_id_seq;
       public       redmine    false    3    299            �           0    0    dmsf_workflow_steps_id_seq    SEQUENCE OWNED BY     Y   ALTER SEQUENCE public.dmsf_workflow_steps_id_seq OWNED BY public.dmsf_workflow_steps.id;
            public       redmine    false    298            )           1259    27382    dmsf_workflows    TABLE       CREATE TABLE public.dmsf_workflows (
    id integer NOT NULL,
    name character varying NOT NULL,
    project_id integer,
    status integer DEFAULT 1 NOT NULL,
    updated_on timestamp without time zone,
    created_on timestamp without time zone,
    author_id integer
);
 "   DROP TABLE public.dmsf_workflows;
       public         redmine    false    3            (           1259    27380    dmsf_workflows_id_seq    SEQUENCE     ~   CREATE SEQUENCE public.dmsf_workflows_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ,   DROP SEQUENCE public.dmsf_workflows_id_seq;
       public       redmine    false    3    297            �           0    0    dmsf_workflows_id_seq    SEQUENCE OWNED BY     O   ALTER SEQUENCE public.dmsf_workflows_id_seq OWNED BY public.dmsf_workflows.id;
            public       redmine    false    296            �            1259    24671 	   documents    TABLE       CREATE TABLE public.documents (
    id integer NOT NULL,
    project_id integer DEFAULT 0 NOT NULL,
    category_id integer DEFAULT 0 NOT NULL,
    title character varying DEFAULT ''::character varying NOT NULL,
    description text,
    created_on timestamp without time zone
);
    DROP TABLE public.documents;
       public         redmine    false    3            �            1259    24669    documents_id_seq    SEQUENCE     y   CREATE SEQUENCE public.documents_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE public.documents_id_seq;
       public       redmine    false    198    3            �           0    0    documents_id_seq    SEQUENCE OWNED BY     E   ALTER SEQUENCE public.documents_id_seq OWNED BY public.documents.id;
            public       redmine    false    197            9           1259    30390    easy_entity_assignments    TABLE     %  CREATE TABLE public.easy_entity_assignments (
    id integer NOT NULL,
    entity_from_type character varying,
    entity_from_id integer,
    entity_to_type character varying,
    entity_to_id integer,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);
 +   DROP TABLE public.easy_entity_assignments;
       public         redmine    false    3            8           1259    30388    easy_entity_assignments_id_seq    SEQUENCE     �   CREATE SEQUENCE public.easy_entity_assignments_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 5   DROP SEQUENCE public.easy_entity_assignments_id_seq;
       public       redmine    false    3    313            �           0    0    easy_entity_assignments_id_seq    SEQUENCE OWNED BY     a   ALTER SEQUENCE public.easy_entity_assignments_id_seq OWNED BY public.easy_entity_assignments.id;
            public       redmine    false    312            7           1259    30372    easy_settings    TABLE     �   CREATE TABLE public.easy_settings (
    id integer NOT NULL,
    type character varying,
    name character varying,
    value text,
    project_id integer
);
 !   DROP TABLE public.easy_settings;
       public         redmine    false    3            6           1259    30370    easy_settings_id_seq    SEQUENCE     }   CREATE SEQUENCE public.easy_settings_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.easy_settings_id_seq;
       public       redmine    false    311    3            �           0    0    easy_settings_id_seq    SEQUENCE OWNED BY     M   ALTER SEQUENCE public.easy_settings_id_seq OWNED BY public.easy_settings.id;
            public       redmine    false    310                       1259    25693    email_addresses    TABLE     J  CREATE TABLE public.email_addresses (
    id integer NOT NULL,
    user_id integer NOT NULL,
    address character varying NOT NULL,
    is_default boolean DEFAULT false NOT NULL,
    notify boolean DEFAULT true NOT NULL,
    created_on timestamp without time zone NOT NULL,
    updated_on timestamp without time zone NOT NULL
);
 #   DROP TABLE public.email_addresses;
       public         redmine    false    3                       1259    25691    email_addresses_id_seq    SEQUENCE        CREATE SEQUENCE public.email_addresses_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE public.email_addresses_id_seq;
       public       redmine    false    278    3            �           0    0    email_addresses_id_seq    SEQUENCE OWNED BY     Q   ALTER SEQUENCE public.email_addresses_id_seq OWNED BY public.email_addresses.id;
            public       redmine    false    277            	           1259    25241    enabled_modules    TABLE     ~   CREATE TABLE public.enabled_modules (
    id integer NOT NULL,
    project_id integer,
    name character varying NOT NULL
);
 #   DROP TABLE public.enabled_modules;
       public         redmine    false    3                       1259    25239    enabled_modules_id_seq    SEQUENCE        CREATE SEQUENCE public.enabled_modules_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE public.enabled_modules_id_seq;
       public       redmine    false    3    265            �           0    0    enabled_modules_id_seq    SEQUENCE OWNED BY     Q   ALTER SEQUENCE public.enabled_modules_id_seq OWNED BY public.enabled_modules.id;
            public       redmine    false    264            �            1259    24686    enumerations    TABLE     j  CREATE TABLE public.enumerations (
    id integer NOT NULL,
    name character varying(30) DEFAULT ''::character varying NOT NULL,
    "position" integer,
    is_default boolean DEFAULT false NOT NULL,
    type character varying,
    active boolean DEFAULT true NOT NULL,
    project_id integer,
    parent_id integer,
    position_name character varying(30)
);
     DROP TABLE public.enumerations;
       public         redmine    false    3            �            1259    24684    enumerations_id_seq    SEQUENCE     |   CREATE SEQUENCE public.enumerations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE public.enumerations_id_seq;
       public       redmine    false    200    3            �           0    0    enumerations_id_seq    SEQUENCE OWNED BY     K   ALTER SEQUENCE public.enumerations_id_seq OWNED BY public.enumerations.id;
            public       redmine    false    199                       1259    25411    groups_users    TABLE     b   CREATE TABLE public.groups_users (
    group_id integer NOT NULL,
    user_id integer NOT NULL
);
     DROP TABLE public.groups_users;
       public         redmine    false    3                       1259    25741    import_items    TABLE     �   CREATE TABLE public.import_items (
    id integer NOT NULL,
    import_id integer NOT NULL,
    "position" integer NOT NULL,
    obj_id integer,
    message text
);
     DROP TABLE public.import_items;
       public         redmine    false    3                       1259    25739    import_items_id_seq    SEQUENCE     |   CREATE SEQUENCE public.import_items_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE public.import_items_id_seq;
       public       redmine    false    3    283            �           0    0    import_items_id_seq    SEQUENCE OWNED BY     K   ALTER SEQUENCE public.import_items_id_seq OWNED BY public.import_items.id;
            public       redmine    false    282                       1259    25729    imports    TABLE     V  CREATE TABLE public.imports (
    id integer NOT NULL,
    type character varying,
    user_id integer NOT NULL,
    filename character varying,
    settings text,
    total_items integer,
    finished boolean DEFAULT false NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);
    DROP TABLE public.imports;
       public         redmine    false    3                       1259    25727    imports_id_seq    SEQUENCE     w   CREATE SEQUENCE public.imports_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE public.imports_id_seq;
       public       redmine    false    281    3            �           0    0    imports_id_seq    SEQUENCE OWNED BY     A   ALTER SEQUENCE public.imports_id_seq OWNED BY public.imports.id;
            public       redmine    false    280            �            1259    24696    issue_categories    TABLE     �   CREATE TABLE public.issue_categories (
    id integer NOT NULL,
    project_id integer DEFAULT 0 NOT NULL,
    name character varying(60) DEFAULT ''::character varying NOT NULL,
    assigned_to_id integer
);
 $   DROP TABLE public.issue_categories;
       public         redmine    false    3            �            1259    24694    issue_categories_id_seq    SEQUENCE     �   CREATE SEQUENCE public.issue_categories_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public.issue_categories_id_seq;
       public       redmine    false    3    202            �           0    0    issue_categories_id_seq    SEQUENCE OWNED BY     S   ALTER SEQUENCE public.issue_categories_id_seq OWNED BY public.issue_categories.id;
            public       redmine    false    201                       1259    25162    issue_relations    TABLE     �   CREATE TABLE public.issue_relations (
    id integer NOT NULL,
    issue_from_id integer NOT NULL,
    issue_to_id integer NOT NULL,
    relation_type character varying DEFAULT ''::character varying NOT NULL,
    delay integer
);
 #   DROP TABLE public.issue_relations;
       public         redmine    false    3                        1259    25160    issue_relations_id_seq    SEQUENCE        CREATE SEQUENCE public.issue_relations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE public.issue_relations_id_seq;
       public       redmine    false    3    257            �           0    0    issue_relations_id_seq    SEQUENCE OWNED BY     Q   ALTER SEQUENCE public.issue_relations_id_seq OWNED BY public.issue_relations.id;
            public       redmine    false    256            �            1259    24722    issue_statuses    TABLE     �   CREATE TABLE public.issue_statuses (
    id integer NOT NULL,
    name character varying(30) DEFAULT ''::character varying NOT NULL,
    is_closed boolean DEFAULT false NOT NULL,
    "position" integer,
    default_done_ratio integer
);
 "   DROP TABLE public.issue_statuses;
       public         redmine    false    3            �            1259    24720    issue_statuses_id_seq    SEQUENCE     ~   CREATE SEQUENCE public.issue_statuses_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ,   DROP SEQUENCE public.issue_statuses_id_seq;
       public       redmine    false    204    3            �           0    0    issue_statuses_id_seq    SEQUENCE OWNED BY     O   ALTER SEQUENCE public.issue_statuses_id_seq OWNED BY public.issue_statuses.id;
            public       redmine    false    203            �            1259    24734    issues    TABLE     1  CREATE TABLE public.issues (
    id integer NOT NULL,
    tracker_id integer NOT NULL,
    project_id integer NOT NULL,
    subject character varying DEFAULT ''::character varying NOT NULL,
    description text,
    due_date date,
    category_id integer,
    status_id integer NOT NULL,
    assigned_to_id integer,
    priority_id integer NOT NULL,
    fixed_version_id integer,
    author_id integer NOT NULL,
    lock_version integer DEFAULT 0 NOT NULL,
    created_on timestamp without time zone,
    updated_on timestamp without time zone,
    start_date date,
    done_ratio integer DEFAULT 0 NOT NULL,
    estimated_hours double precision,
    parent_id integer,
    root_id integer,
    lft integer,
    rgt integer,
    is_private boolean DEFAULT false NOT NULL,
    closed_on timestamp without time zone
);
    DROP TABLE public.issues;
       public         redmine    false    3            �            1259    24732    issues_id_seq    SEQUENCE     v   CREATE SEQUENCE public.issues_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 $   DROP SEQUENCE public.issues_id_seq;
       public       redmine    false    206    3            �           0    0    issues_id_seq    SEQUENCE OWNED BY     ?   ALTER SEQUENCE public.issues_id_seq OWNED BY public.issues.id;
            public       redmine    false    205            �            1259    24913    journal_details    TABLE     &  CREATE TABLE public.journal_details (
    id integer NOT NULL,
    journal_id integer DEFAULT 0 NOT NULL,
    property character varying(30) DEFAULT ''::character varying NOT NULL,
    prop_key character varying(30) DEFAULT ''::character varying NOT NULL,
    old_value text,
    value text
);
 #   DROP TABLE public.journal_details;
       public         redmine    false    3            �            1259    24911    journal_details_id_seq    SEQUENCE        CREATE SEQUENCE public.journal_details_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE public.journal_details_id_seq;
       public       redmine    false    3    228            �           0    0    journal_details_id_seq    SEQUENCE OWNED BY     Q   ALTER SEQUENCE public.journal_details_id_seq OWNED BY public.journal_details.id;
            public       redmine    false    227            �            1259    24899    journals    TABLE     [  CREATE TABLE public.journals (
    id integer NOT NULL,
    journalized_id integer DEFAULT 0 NOT NULL,
    journalized_type character varying(30) DEFAULT ''::character varying NOT NULL,
    user_id integer DEFAULT 0 NOT NULL,
    notes text,
    created_on timestamp without time zone NOT NULL,
    private_notes boolean DEFAULT false NOT NULL
);
    DROP TABLE public.journals;
       public         redmine    false    3            �            1259    24897    journals_id_seq    SEQUENCE     x   CREATE SEQUENCE public.journals_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE public.journals_id_seq;
       public       redmine    false    226    3            �           0    0    journals_id_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE public.journals_id_seq OWNED BY public.journals.id;
            public       redmine    false    225                       1259    25405    member_roles    TABLE     �   CREATE TABLE public.member_roles (
    id integer NOT NULL,
    member_id integer NOT NULL,
    role_id integer NOT NULL,
    inherited_from integer
);
     DROP TABLE public.member_roles;
       public         redmine    false    3                       1259    25403    member_roles_id_seq    SEQUENCE     |   CREATE SEQUENCE public.member_roles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE public.member_roles_id_seq;
       public       redmine    false    3    272            �           0    0    member_roles_id_seq    SEQUENCE OWNED BY     K   ALTER SEQUENCE public.member_roles_id_seq OWNED BY public.member_roles.id;
            public       redmine    false    271            �            1259    24753    members    TABLE     b  CREATE TABLE public.members (
    id integer NOT NULL,
    user_id integer DEFAULT 0 NOT NULL,
    project_id integer DEFAULT 0 NOT NULL,
    created_on timestamp without time zone,
    mail_notification boolean DEFAULT false NOT NULL,
    dmsf_mail_notification boolean,
    dmsf_title_format text,
    dmsf_fast_links boolean DEFAULT false NOT NULL
);
    DROP TABLE public.members;
       public         redmine    false    3            �            1259    24751    members_id_seq    SEQUENCE     w   CREATE SEQUENCE public.members_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE public.members_id_seq;
       public       redmine    false    208    3            �           0    0    members_id_seq    SEQUENCE OWNED BY     A   ALTER SEQUENCE public.members_id_seq OWNED BY public.members.id;
            public       redmine    false    207                       1259    25192    messages    TABLE     �  CREATE TABLE public.messages (
    id integer NOT NULL,
    board_id integer NOT NULL,
    parent_id integer,
    subject character varying DEFAULT ''::character varying NOT NULL,
    content text,
    author_id integer,
    replies_count integer DEFAULT 0 NOT NULL,
    last_reply_id integer,
    created_on timestamp without time zone NOT NULL,
    updated_on timestamp without time zone NOT NULL,
    locked boolean DEFAULT false,
    sticky integer DEFAULT 0
);
    DROP TABLE public.messages;
       public         redmine    false    3                       1259    25190    messages_id_seq    SEQUENCE     x   CREATE SEQUENCE public.messages_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE public.messages_id_seq;
       public       redmine    false    261    3            �           0    0    messages_id_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE public.messages_id_seq OWNED BY public.messages.id;
            public       redmine    false    260            �            1259    24764    news    TABLE     s  CREATE TABLE public.news (
    id integer NOT NULL,
    project_id integer,
    title character varying(60) DEFAULT ''::character varying NOT NULL,
    summary character varying(255) DEFAULT ''::character varying,
    description text,
    author_id integer DEFAULT 0 NOT NULL,
    created_on timestamp without time zone,
    comments_count integer DEFAULT 0 NOT NULL
);
    DROP TABLE public.news;
       public         redmine    false    3            �            1259    24762    news_id_seq    SEQUENCE     t   CREATE SEQUENCE public.news_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 "   DROP SEQUENCE public.news_id_seq;
       public       redmine    false    3    210            �           0    0    news_id_seq    SEQUENCE OWNED BY     ;   ALTER SEQUENCE public.news_id_seq OWNED BY public.news.id;
            public       redmine    false    209                       1259    25367 #   open_id_authentication_associations    TABLE     �   CREATE TABLE public.open_id_authentication_associations (
    id integer NOT NULL,
    issued integer,
    lifetime integer,
    handle character varying,
    assoc_type character varying,
    server_url bytea,
    secret bytea
);
 7   DROP TABLE public.open_id_authentication_associations;
       public         redmine    false    3                       1259    25365 *   open_id_authentication_associations_id_seq    SEQUENCE     �   CREATE SEQUENCE public.open_id_authentication_associations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 A   DROP SEQUENCE public.open_id_authentication_associations_id_seq;
       public       redmine    false    3    268            �           0    0 *   open_id_authentication_associations_id_seq    SEQUENCE OWNED BY     y   ALTER SEQUENCE public.open_id_authentication_associations_id_seq OWNED BY public.open_id_authentication_associations.id;
            public       redmine    false    267                       1259    25378    open_id_authentication_nonces    TABLE     �   CREATE TABLE public.open_id_authentication_nonces (
    id integer NOT NULL,
    "timestamp" integer NOT NULL,
    server_url character varying,
    salt character varying NOT NULL
);
 1   DROP TABLE public.open_id_authentication_nonces;
       public         redmine    false    3                       1259    25376 $   open_id_authentication_nonces_id_seq    SEQUENCE     �   CREATE SEQUENCE public.open_id_authentication_nonces_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ;   DROP SEQUENCE public.open_id_authentication_nonces_id_seq;
       public       redmine    false    3    270            �           0    0 $   open_id_authentication_nonces_id_seq    SEQUENCE OWNED BY     m   ALTER SEQUENCE public.open_id_authentication_nonces_id_seq OWNED BY public.open_id_authentication_nonces.id;
            public       redmine    false    269            �            1259    24800    projects    TABLE     �  CREATE TABLE public.projects (
    id integer NOT NULL,
    name character varying DEFAULT ''::character varying NOT NULL,
    description text,
    homepage character varying DEFAULT ''::character varying,
    is_public boolean DEFAULT true NOT NULL,
    parent_id integer,
    created_on timestamp without time zone,
    updated_on timestamp without time zone,
    identifier character varying,
    status integer DEFAULT 1 NOT NULL,
    lft integer,
    rgt integer,
    inherit_members boolean DEFAULT false NOT NULL,
    default_version_id integer,
    default_assigned_to_id integer,
    dmsf_description text,
    dmsf_notification boolean DEFAULT false,
    dmsf_act_as_attachable integer DEFAULT 1 NOT NULL
);
    DROP TABLE public.projects;
       public         redmine    false    3            �            1259    24798    projects_id_seq    SEQUENCE     x   CREATE SEQUENCE public.projects_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE public.projects_id_seq;
       public       redmine    false    3    212            �           0    0    projects_id_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE public.projects_id_seq OWNED BY public.projects.id;
            public       redmine    false    211            
           1259    25296    projects_trackers    TABLE     �   CREATE TABLE public.projects_trackers (
    project_id integer DEFAULT 0 NOT NULL,
    tracker_id integer DEFAULT 0 NOT NULL
);
 %   DROP TABLE public.projects_trackers;
       public         redmine    false    3            �            1259    24972    queries    TABLE     m  CREATE TABLE public.queries (
    id integer NOT NULL,
    project_id integer,
    name character varying DEFAULT ''::character varying NOT NULL,
    filters text,
    user_id integer DEFAULT 0 NOT NULL,
    column_names text,
    sort_criteria text,
    group_by character varying,
    type character varying,
    visibility integer DEFAULT 0,
    options text
);
    DROP TABLE public.queries;
       public         redmine    false    3            �            1259    24970    queries_id_seq    SEQUENCE     w   CREATE SEQUENCE public.queries_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE public.queries_id_seq;
       public       redmine    false    3    234            �           0    0    queries_id_seq    SEQUENCE OWNED BY     A   ALTER SEQUENCE public.queries_id_seq OWNED BY public.queries.id;
            public       redmine    false    233                       1259    25652    queries_roles    TABLE     c   CREATE TABLE public.queries_roles (
    query_id integer NOT NULL,
    role_id integer NOT NULL
);
 !   DROP TABLE public.queries_roles;
       public         redmine    false    3            �            1259    24986    repositories    TABLE     �  CREATE TABLE public.repositories (
    id integer NOT NULL,
    project_id integer DEFAULT 0 NOT NULL,
    url character varying DEFAULT ''::character varying NOT NULL,
    login character varying(60) DEFAULT ''::character varying,
    password character varying DEFAULT ''::character varying,
    root_url character varying(255) DEFAULT ''::character varying,
    type character varying,
    path_encoding character varying(64) DEFAULT NULL::character varying,
    log_encoding character varying(64) DEFAULT NULL::character varying,
    extra_info text,
    identifier character varying,
    is_default boolean DEFAULT false,
    created_on timestamp without time zone
);
     DROP TABLE public.repositories;
       public         redmine    false    3            �            1259    24984    repositories_id_seq    SEQUENCE     |   CREATE SEQUENCE public.repositories_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE public.repositories_id_seq;
       public       redmine    false    3    236            �           0    0    repositories_id_seq    SEQUENCE OWNED BY     K   ALTER SEQUENCE public.repositories_id_seq OWNED BY public.repositories.id;
            public       redmine    false    235            �            1259    24816    roles    TABLE     O  CREATE TABLE public.roles (
    id integer NOT NULL,
    name character varying(30) DEFAULT ''::character varying NOT NULL,
    "position" integer,
    assignable boolean DEFAULT true,
    builtin integer DEFAULT 0 NOT NULL,
    permissions text,
    issues_visibility character varying(30) DEFAULT 'default'::character varying NOT NULL,
    users_visibility character varying(30) DEFAULT 'all'::character varying NOT NULL,
    time_entries_visibility character varying(30) DEFAULT 'all'::character varying NOT NULL,
    all_roles_managed boolean DEFAULT true NOT NULL,
    settings text
);
    DROP TABLE public.roles;
       public         redmine    false    3            �            1259    24814    roles_id_seq    SEQUENCE     u   CREATE SEQUENCE public.roles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 #   DROP SEQUENCE public.roles_id_seq;
       public       redmine    false    3    214            �           0    0    roles_id_seq    SEQUENCE OWNED BY     =   ALTER SEQUENCE public.roles_id_seq OWNED BY public.roles.id;
            public       redmine    false    213                       1259    25723    roles_managed_roles    TABLE     p   CREATE TABLE public.roles_managed_roles (
    role_id integer NOT NULL,
    managed_role_id integer NOT NULL
);
 '   DROP TABLE public.roles_managed_roles;
       public         redmine    false    3            �            1259    24576    schema_migrations    TABLE     R   CREATE TABLE public.schema_migrations (
    version character varying NOT NULL
);
 %   DROP TABLE public.schema_migrations;
       public         redmine    false    3            �            1259    24999    settings    TABLE     �   CREATE TABLE public.settings (
    id integer NOT NULL,
    name character varying(255) DEFAULT ''::character varying NOT NULL,
    value text,
    updated_on timestamp without time zone
);
    DROP TABLE public.settings;
       public         redmine    false    3            �            1259    24997    settings_id_seq    SEQUENCE     x   CREATE SEQUENCE public.settings_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE public.settings_id_seq;
       public       redmine    false    238    3            �           0    0    settings_id_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE public.settings_id_seq OWNED BY public.settings.id;
            public       redmine    false    237            �            1259    25101    time_entries    TABLE     �  CREATE TABLE public.time_entries (
    id integer NOT NULL,
    project_id integer NOT NULL,
    user_id integer NOT NULL,
    issue_id integer,
    hours double precision NOT NULL,
    comments character varying(1024),
    activity_id integer NOT NULL,
    spent_on date NOT NULL,
    tyear integer NOT NULL,
    tmonth integer NOT NULL,
    tweek integer NOT NULL,
    created_on timestamp without time zone NOT NULL,
    updated_on timestamp without time zone NOT NULL
);
     DROP TABLE public.time_entries;
       public         redmine    false    3            �            1259    25099    time_entries_id_seq    SEQUENCE     |   CREATE SEQUENCE public.time_entries_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE public.time_entries_id_seq;
       public       redmine    false    3    248            �           0    0    time_entries_id_seq    SEQUENCE OWNED BY     K   ALTER SEQUENCE public.time_entries_id_seq OWNED BY public.time_entries.id;
            public       redmine    false    247            �            1259    24825    tokens    TABLE     R  CREATE TABLE public.tokens (
    id integer NOT NULL,
    user_id integer DEFAULT 0 NOT NULL,
    action character varying(30) DEFAULT ''::character varying NOT NULL,
    value character varying(40) DEFAULT ''::character varying NOT NULL,
    created_on timestamp without time zone NOT NULL,
    updated_on timestamp without time zone
);
    DROP TABLE public.tokens;
       public         redmine    false    3            �            1259    24823    tokens_id_seq    SEQUENCE     v   CREATE SEQUENCE public.tokens_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 $   DROP SEQUENCE public.tokens_id_seq;
       public       redmine    false    3    216            �           0    0    tokens_id_seq    SEQUENCE OWNED BY     ?   ALTER SEQUENCE public.tokens_id_seq OWNED BY public.tokens.id;
            public       redmine    false    215            �            1259    24836    trackers    TABLE     <  CREATE TABLE public.trackers (
    id integer NOT NULL,
    name character varying(30) DEFAULT ''::character varying NOT NULL,
    is_in_chlog boolean DEFAULT false NOT NULL,
    "position" integer,
    is_in_roadmap boolean DEFAULT true NOT NULL,
    fields_bits integer DEFAULT 0,
    default_status_id integer
);
    DROP TABLE public.trackers;
       public         redmine    false    3            �            1259    24834    trackers_id_seq    SEQUENCE     x   CREATE SEQUENCE public.trackers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE public.trackers_id_seq;
       public       redmine    false    3    218            �           0    0    trackers_id_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE public.trackers_id_seq OWNED BY public.trackers.id;
            public       redmine    false    217            �            1259    24929    user_preferences    TABLE     �   CREATE TABLE public.user_preferences (
    id integer NOT NULL,
    user_id integer DEFAULT 0 NOT NULL,
    others text,
    hide_mail boolean DEFAULT true,
    time_zone character varying
);
 $   DROP TABLE public.user_preferences;
       public         redmine    false    3            �            1259    24927    user_preferences_id_seq    SEQUENCE     �   CREATE SEQUENCE public.user_preferences_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public.user_preferences_id_seq;
       public       redmine    false    3    230            �           0    0    user_preferences_id_seq    SEQUENCE OWNED BY     S   ALTER SEQUENCE public.user_preferences_id_seq OWNED BY public.user_preferences.id;
            public       redmine    false    229            �            1259    24846    users    TABLE     �  CREATE TABLE public.users (
    id integer NOT NULL,
    login character varying DEFAULT ''::character varying NOT NULL,
    hashed_password character varying(40) DEFAULT ''::character varying NOT NULL,
    firstname character varying(30) DEFAULT ''::character varying NOT NULL,
    lastname character varying(255) DEFAULT ''::character varying NOT NULL,
    admin boolean DEFAULT false NOT NULL,
    status integer DEFAULT 1 NOT NULL,
    last_login_on timestamp without time zone,
    language character varying(5) DEFAULT ''::character varying,
    auth_source_id integer,
    created_on timestamp without time zone,
    updated_on timestamp without time zone,
    type character varying,
    identity_url character varying,
    mail_notification character varying DEFAULT ''::character varying NOT NULL,
    salt character varying(64),
    must_change_passwd boolean DEFAULT false NOT NULL,
    passwd_changed_on timestamp without time zone
);
    DROP TABLE public.users;
       public         redmine    false    3            �            1259    24844    users_id_seq    SEQUENCE     u   CREATE SEQUENCE public.users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 #   DROP SEQUENCE public.users_id_seq;
       public       redmine    false    220    3            �           0    0    users_id_seq    SEQUENCE OWNED BY     =   ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;
            public       redmine    false    219            �            1259    24863    versions    TABLE       CREATE TABLE public.versions (
    id integer NOT NULL,
    project_id integer DEFAULT 0 NOT NULL,
    name character varying DEFAULT ''::character varying NOT NULL,
    description character varying DEFAULT ''::character varying,
    effective_date date,
    created_on timestamp without time zone,
    updated_on timestamp without time zone,
    wiki_page_title character varying,
    status character varying DEFAULT 'open'::character varying,
    sharing character varying DEFAULT 'none'::character varying NOT NULL
);
    DROP TABLE public.versions;
       public         redmine    false    3            �            1259    24861    versions_id_seq    SEQUENCE     x   CREATE SEQUENCE public.versions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE public.versions_id_seq;
       public       redmine    false    3    222            �           0    0    versions_id_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE public.versions_id_seq OWNED BY public.versions.id;
            public       redmine    false    221            �            1259    25145    watchers    TABLE     �   CREATE TABLE public.watchers (
    id integer NOT NULL,
    watchable_type character varying DEFAULT ''::character varying NOT NULL,
    watchable_id integer DEFAULT 0 NOT NULL,
    user_id integer
);
    DROP TABLE public.watchers;
       public         redmine    false    3            �            1259    25143    watchers_id_seq    SEQUENCE     x   CREATE SEQUENCE public.watchers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE public.watchers_id_seq;
       public       redmine    false    3    254            �           0    0    watchers_id_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE public.watchers_id_seq OWNED BY public.watchers.id;
            public       redmine    false    253            �            1259    25079    wiki_content_versions    TABLE     �  CREATE TABLE public.wiki_content_versions (
    id integer NOT NULL,
    wiki_content_id integer NOT NULL,
    page_id integer NOT NULL,
    author_id integer,
    data bytea,
    compression character varying(6) DEFAULT ''::character varying,
    comments character varying(1024) DEFAULT ''::character varying,
    updated_on timestamp without time zone NOT NULL,
    version integer NOT NULL
);
 )   DROP TABLE public.wiki_content_versions;
       public         redmine    false    3            �            1259    25077    wiki_content_versions_id_seq    SEQUENCE     �   CREATE SEQUENCE public.wiki_content_versions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 3   DROP SEQUENCE public.wiki_content_versions_id_seq;
       public       redmine    false    3    246            �           0    0    wiki_content_versions_id_seq    SEQUENCE OWNED BY     ]   ALTER SEQUENCE public.wiki_content_versions_id_seq OWNED BY public.wiki_content_versions.id;
            public       redmine    false    245            �            1259    25066    wiki_contents    TABLE       CREATE TABLE public.wiki_contents (
    id integer NOT NULL,
    page_id integer NOT NULL,
    author_id integer,
    text text,
    comments character varying(1024) DEFAULT ''::character varying,
    updated_on timestamp without time zone NOT NULL,
    version integer NOT NULL
);
 !   DROP TABLE public.wiki_contents;
       public         redmine    false    3            �            1259    25064    wiki_contents_id_seq    SEQUENCE     }   CREATE SEQUENCE public.wiki_contents_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.wiki_contents_id_seq;
       public       redmine    false    244    3            �           0    0    wiki_contents_id_seq    SEQUENCE OWNED BY     M   ALTER SEQUENCE public.wiki_contents_id_seq OWNED BY public.wiki_contents.id;
            public       redmine    false    243            �            1259    25057 
   wiki_pages    TABLE     �   CREATE TABLE public.wiki_pages (
    id integer NOT NULL,
    wiki_id integer NOT NULL,
    title character varying(255) NOT NULL,
    created_on timestamp without time zone NOT NULL,
    protected boolean DEFAULT false NOT NULL,
    parent_id integer
);
    DROP TABLE public.wiki_pages;
       public         redmine    false    3            �            1259    25055    wiki_pages_id_seq    SEQUENCE     z   CREATE SEQUENCE public.wiki_pages_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE public.wiki_pages_id_seq;
       public       redmine    false    242    3            �           0    0    wiki_pages_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE public.wiki_pages_id_seq OWNED BY public.wiki_pages.id;
            public       redmine    false    241                       1259    25229    wiki_redirects    TABLE     �   CREATE TABLE public.wiki_redirects (
    id integer NOT NULL,
    wiki_id integer NOT NULL,
    title character varying,
    redirects_to character varying,
    created_on timestamp without time zone NOT NULL,
    redirects_to_wiki_id integer NOT NULL
);
 "   DROP TABLE public.wiki_redirects;
       public         redmine    false    3                       1259    25227    wiki_redirects_id_seq    SEQUENCE     ~   CREATE SEQUENCE public.wiki_redirects_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ,   DROP SEQUENCE public.wiki_redirects_id_seq;
       public       redmine    false    3    263            �           0    0    wiki_redirects_id_seq    SEQUENCE OWNED BY     O   ALTER SEQUENCE public.wiki_redirects_id_seq OWNED BY public.wiki_redirects.id;
            public       redmine    false    262            �            1259    25047    wikis    TABLE     �   CREATE TABLE public.wikis (
    id integer NOT NULL,
    project_id integer NOT NULL,
    start_page character varying(255) NOT NULL,
    status integer DEFAULT 1 NOT NULL
);
    DROP TABLE public.wikis;
       public         redmine    false    3            �            1259    25045    wikis_id_seq    SEQUENCE     u   CREATE SEQUENCE public.wikis_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 #   DROP SEQUENCE public.wikis_id_seq;
       public       redmine    false    3    240            �           0    0    wikis_id_seq    SEQUENCE OWNED BY     =   ALTER SEQUENCE public.wikis_id_seq OWNED BY public.wikis.id;
            public       redmine    false    239            �            1259    24878 	   workflows    TABLE     �  CREATE TABLE public.workflows (
    id integer NOT NULL,
    tracker_id integer DEFAULT 0 NOT NULL,
    old_status_id integer DEFAULT 0 NOT NULL,
    new_status_id integer DEFAULT 0 NOT NULL,
    role_id integer DEFAULT 0 NOT NULL,
    assignee boolean DEFAULT false NOT NULL,
    author boolean DEFAULT false NOT NULL,
    type character varying(30),
    field_name character varying(30),
    rule character varying(30)
);
    DROP TABLE public.workflows;
       public         redmine    false    3            �            1259    24876    workflows_id_seq    SEQUENCE     y   CREATE SEQUENCE public.workflows_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE public.workflows_id_seq;
       public       redmine    false    3    224            �           0    0    workflows_id_seq    SEQUENCE OWNED BY     E   ALTER SEQUENCE public.workflows_id_seq OWNED BY public.workflows.id;
            public       redmine    false    223            �
           2604    24597    attachments id    DEFAULT     p   ALTER TABLE ONLY public.attachments ALTER COLUMN id SET DEFAULT nextval('public.attachments_id_seq'::regclass);
 =   ALTER TABLE public.attachments ALTER COLUMN id DROP DEFAULT;
       public       redmine    false    188    187    188            �
           2604    24617    auth_sources id    DEFAULT     r   ALTER TABLE ONLY public.auth_sources ALTER COLUMN id SET DEFAULT nextval('public.auth_sources_id_seq'::regclass);
 >   ALTER TABLE public.auth_sources ALTER COLUMN id DROP DEFAULT;
       public       redmine    false    190    189    190            �           2604    25179 	   boards id    DEFAULT     f   ALTER TABLE ONLY public.boards ALTER COLUMN id SET DEFAULT nextval('public.boards_id_seq'::regclass);
 8   ALTER TABLE public.boards ALTER COLUMN id DROP DEFAULT;
       public       redmine    false    259    258    259            �           2604    25126 
   changes id    DEFAULT     h   ALTER TABLE ONLY public.changes ALTER COLUMN id SET DEFAULT nextval('public.changes_id_seq'::regclass);
 9   ALTER TABLE public.changes ALTER COLUMN id DROP DEFAULT;
       public       redmine    false    252    251    252            �           2604    25114    changesets id    DEFAULT     n   ALTER TABLE ONLY public.changesets ALTER COLUMN id SET DEFAULT nextval('public.changesets_id_seq'::regclass);
 <   ALTER TABLE public.changesets ALTER COLUMN id DROP DEFAULT;
       public       redmine    false    250    249    250            g           2604    24952    comments id    DEFAULT     j   ALTER TABLE ONLY public.comments ALTER COLUMN id SET DEFAULT nextval('public.comments_id_seq'::regclass);
 :   ALTER TABLE public.comments ALTER COLUMN id DROP DEFAULT;
       public       redmine    false    232    231    232            �           2604    25778    custom_field_enumerations id    DEFAULT     �   ALTER TABLE ONLY public.custom_field_enumerations ALTER COLUMN id SET DEFAULT nextval('public.custom_field_enumerations_id_seq'::regclass);
 K   ALTER TABLE public.custom_field_enumerations ALTER COLUMN id DROP DEFAULT;
       public       redmine    false    285    284    285            �
           2604    24631    custom_fields id    DEFAULT     t   ALTER TABLE ONLY public.custom_fields ALTER COLUMN id SET DEFAULT nextval('public.custom_fields_id_seq'::regclass);
 ?   ALTER TABLE public.custom_fields ALTER COLUMN id DROP DEFAULT;
       public       redmine    false    191    192    192            
           2604    24660    custom_values id    DEFAULT     t   ALTER TABLE ONLY public.custom_values ALTER COLUMN id SET DEFAULT nextval('public.custom_values_id_seq'::regclass);
 ?   ALTER TABLE public.custom_values ALTER COLUMN id DROP DEFAULT;
       public       redmine    false    196    195    196            �           2604    27376    dmsf_file_revision_accesses id    DEFAULT     �   ALTER TABLE ONLY public.dmsf_file_revision_accesses ALTER COLUMN id SET DEFAULT nextval('public.dmsf_file_revision_accesses_id_seq'::regclass);
 M   ALTER TABLE public.dmsf_file_revision_accesses ALTER COLUMN id DROP DEFAULT;
       public       redmine    false    295    294    295            �           2604    27347    dmsf_file_revisions id    DEFAULT     �   ALTER TABLE ONLY public.dmsf_file_revisions ALTER COLUMN id SET DEFAULT nextval('public.dmsf_file_revisions_id_seq'::regclass);
 E   ALTER TABLE public.dmsf_file_revisions ALTER COLUMN id DROP DEFAULT;
       public       redmine    false    291    290    291            �           2604    27334    dmsf_files id    DEFAULT     n   ALTER TABLE ONLY public.dmsf_files ALTER COLUMN id SET DEFAULT nextval('public.dmsf_files_id_seq'::regclass);
 <   ALTER TABLE public.dmsf_files ALTER COLUMN id DROP DEFAULT;
       public       redmine    false    288    289    289            �           2604    27544    dmsf_folder_permissions id    DEFAULT     �   ALTER TABLE ONLY public.dmsf_folder_permissions ALTER COLUMN id SET DEFAULT nextval('public.dmsf_folder_permissions_id_seq'::regclass);
 I   ALTER TABLE public.dmsf_folder_permissions ALTER COLUMN id DROP DEFAULT;
       public       redmine    false    308    309    309            �           2604    27322    dmsf_folders id    DEFAULT     r   ALTER TABLE ONLY public.dmsf_folders ALTER COLUMN id SET DEFAULT nextval('public.dmsf_folders_id_seq'::regclass);
 >   ALTER TABLE public.dmsf_folders ALTER COLUMN id DROP DEFAULT;
       public       redmine    false    287    286    287            �           2604    27431    dmsf_links id    DEFAULT     n   ALTER TABLE ONLY public.dmsf_links ALTER COLUMN id SET DEFAULT nextval('public.dmsf_links_id_seq'::regclass);
 <   ALTER TABLE public.dmsf_links ALTER COLUMN id DROP DEFAULT;
       public       redmine    false    304    305    305            �           2604    27359    dmsf_locks id    DEFAULT     n   ALTER TABLE ONLY public.dmsf_locks ALTER COLUMN id SET DEFAULT nextval('public.dmsf_locks_id_seq'::regclass);
 <   ALTER TABLE public.dmsf_locks ALTER COLUMN id DROP DEFAULT;
       public       redmine    false    292    293    293            �           2604    27526    dmsf_public_urls id    DEFAULT     z   ALTER TABLE ONLY public.dmsf_public_urls ALTER COLUMN id SET DEFAULT nextval('public.dmsf_public_urls_id_seq'::regclass);
 B   ALTER TABLE public.dmsf_public_urls ALTER COLUMN id DROP DEFAULT;
       public       redmine    false    307    306    307            �           2604    27415    dmsf_workflow_step_actions id    DEFAULT     �   ALTER TABLE ONLY public.dmsf_workflow_step_actions ALTER COLUMN id SET DEFAULT nextval('public.dmsf_workflow_step_actions_id_seq'::regclass);
 L   ALTER TABLE public.dmsf_workflow_step_actions ALTER COLUMN id DROP DEFAULT;
       public       redmine    false    303    302    303            �           2604    27406 !   dmsf_workflow_step_assignments id    DEFAULT     �   ALTER TABLE ONLY public.dmsf_workflow_step_assignments ALTER COLUMN id SET DEFAULT nextval('public.dmsf_workflow_step_assignments_id_seq'::regclass);
 P   ALTER TABLE public.dmsf_workflow_step_assignments ALTER COLUMN id DROP DEFAULT;
       public       redmine    false    301    300    301            �           2604    27397    dmsf_workflow_steps id    DEFAULT     �   ALTER TABLE ONLY public.dmsf_workflow_steps ALTER COLUMN id SET DEFAULT nextval('public.dmsf_workflow_steps_id_seq'::regclass);
 E   ALTER TABLE public.dmsf_workflow_steps ALTER COLUMN id DROP DEFAULT;
       public       redmine    false    299    298    299            �           2604    27385    dmsf_workflows id    DEFAULT     v   ALTER TABLE ONLY public.dmsf_workflows ALTER COLUMN id SET DEFAULT nextval('public.dmsf_workflows_id_seq'::regclass);
 @   ALTER TABLE public.dmsf_workflows ALTER COLUMN id DROP DEFAULT;
       public       redmine    false    297    296    297                       2604    24674    documents id    DEFAULT     l   ALTER TABLE ONLY public.documents ALTER COLUMN id SET DEFAULT nextval('public.documents_id_seq'::regclass);
 ;   ALTER TABLE public.documents ALTER COLUMN id DROP DEFAULT;
       public       redmine    false    198    197    198            �           2604    30393    easy_entity_assignments id    DEFAULT     �   ALTER TABLE ONLY public.easy_entity_assignments ALTER COLUMN id SET DEFAULT nextval('public.easy_entity_assignments_id_seq'::regclass);
 I   ALTER TABLE public.easy_entity_assignments ALTER COLUMN id DROP DEFAULT;
       public       redmine    false    313    312    313            �           2604    30375    easy_settings id    DEFAULT     t   ALTER TABLE ONLY public.easy_settings ALTER COLUMN id SET DEFAULT nextval('public.easy_settings_id_seq'::regclass);
 ?   ALTER TABLE public.easy_settings ALTER COLUMN id DROP DEFAULT;
       public       redmine    false    310    311    311            �           2604    25696    email_addresses id    DEFAULT     x   ALTER TABLE ONLY public.email_addresses ALTER COLUMN id SET DEFAULT nextval('public.email_addresses_id_seq'::regclass);
 A   ALTER TABLE public.email_addresses ALTER COLUMN id DROP DEFAULT;
       public       redmine    false    278    277    278            �           2604    25244    enabled_modules id    DEFAULT     x   ALTER TABLE ONLY public.enabled_modules ALTER COLUMN id SET DEFAULT nextval('public.enabled_modules_id_seq'::regclass);
 A   ALTER TABLE public.enabled_modules ALTER COLUMN id DROP DEFAULT;
       public       redmine    false    264    265    265                       2604    24689    enumerations id    DEFAULT     r   ALTER TABLE ONLY public.enumerations ALTER COLUMN id SET DEFAULT nextval('public.enumerations_id_seq'::regclass);
 >   ALTER TABLE public.enumerations ALTER COLUMN id DROP DEFAULT;
       public       redmine    false    199    200    200            �           2604    25744    import_items id    DEFAULT     r   ALTER TABLE ONLY public.import_items ALTER COLUMN id SET DEFAULT nextval('public.import_items_id_seq'::regclass);
 >   ALTER TABLE public.import_items ALTER COLUMN id DROP DEFAULT;
       public       redmine    false    283    282    283            �           2604    25732 
   imports id    DEFAULT     h   ALTER TABLE ONLY public.imports ALTER COLUMN id SET DEFAULT nextval('public.imports_id_seq'::regclass);
 9   ALTER TABLE public.imports ALTER COLUMN id DROP DEFAULT;
       public       redmine    false    280    281    281                       2604    24699    issue_categories id    DEFAULT     z   ALTER TABLE ONLY public.issue_categories ALTER COLUMN id SET DEFAULT nextval('public.issue_categories_id_seq'::regclass);
 B   ALTER TABLE public.issue_categories ALTER COLUMN id DROP DEFAULT;
       public       redmine    false    201    202    202            �           2604    25165    issue_relations id    DEFAULT     x   ALTER TABLE ONLY public.issue_relations ALTER COLUMN id SET DEFAULT nextval('public.issue_relations_id_seq'::regclass);
 A   ALTER TABLE public.issue_relations ALTER COLUMN id DROP DEFAULT;
       public       redmine    false    257    256    257                       2604    24725    issue_statuses id    DEFAULT     v   ALTER TABLE ONLY public.issue_statuses ALTER COLUMN id SET DEFAULT nextval('public.issue_statuses_id_seq'::regclass);
 @   ALTER TABLE public.issue_statuses ALTER COLUMN id DROP DEFAULT;
       public       redmine    false    204    203    204                       2604    24737 	   issues id    DEFAULT     f   ALTER TABLE ONLY public.issues ALTER COLUMN id SET DEFAULT nextval('public.issues_id_seq'::regclass);
 8   ALTER TABLE public.issues ALTER COLUMN id DROP DEFAULT;
       public       redmine    false    206    205    206            `           2604    24916    journal_details id    DEFAULT     x   ALTER TABLE ONLY public.journal_details ALTER COLUMN id SET DEFAULT nextval('public.journal_details_id_seq'::regclass);
 A   ALTER TABLE public.journal_details ALTER COLUMN id DROP DEFAULT;
       public       redmine    false    228    227    228            [           2604    24902    journals id    DEFAULT     j   ALTER TABLE ONLY public.journals ALTER COLUMN id SET DEFAULT nextval('public.journals_id_seq'::regclass);
 :   ALTER TABLE public.journals ALTER COLUMN id DROP DEFAULT;
       public       redmine    false    225    226    226            �           2604    25408    member_roles id    DEFAULT     r   ALTER TABLE ONLY public.member_roles ALTER COLUMN id SET DEFAULT nextval('public.member_roles_id_seq'::regclass);
 >   ALTER TABLE public.member_roles ALTER COLUMN id DROP DEFAULT;
       public       redmine    false    271    272    272            !           2604    24756 
   members id    DEFAULT     h   ALTER TABLE ONLY public.members ALTER COLUMN id SET DEFAULT nextval('public.members_id_seq'::regclass);
 9   ALTER TABLE public.members ALTER COLUMN id DROP DEFAULT;
       public       redmine    false    207    208    208            �           2604    25195    messages id    DEFAULT     j   ALTER TABLE ONLY public.messages ALTER COLUMN id SET DEFAULT nextval('public.messages_id_seq'::regclass);
 :   ALTER TABLE public.messages ALTER COLUMN id DROP DEFAULT;
       public       redmine    false    260    261    261            &           2604    24767    news id    DEFAULT     b   ALTER TABLE ONLY public.news ALTER COLUMN id SET DEFAULT nextval('public.news_id_seq'::regclass);
 6   ALTER TABLE public.news ALTER COLUMN id DROP DEFAULT;
       public       redmine    false    210    209    210            �           2604    25370 &   open_id_authentication_associations id    DEFAULT     �   ALTER TABLE ONLY public.open_id_authentication_associations ALTER COLUMN id SET DEFAULT nextval('public.open_id_authentication_associations_id_seq'::regclass);
 U   ALTER TABLE public.open_id_authentication_associations ALTER COLUMN id DROP DEFAULT;
       public       redmine    false    267    268    268            �           2604    25381     open_id_authentication_nonces id    DEFAULT     �   ALTER TABLE ONLY public.open_id_authentication_nonces ALTER COLUMN id SET DEFAULT nextval('public.open_id_authentication_nonces_id_seq'::regclass);
 O   ALTER TABLE public.open_id_authentication_nonces ALTER COLUMN id DROP DEFAULT;
       public       redmine    false    270    269    270            +           2604    24803    projects id    DEFAULT     j   ALTER TABLE ONLY public.projects ALTER COLUMN id SET DEFAULT nextval('public.projects_id_seq'::regclass);
 :   ALTER TABLE public.projects ALTER COLUMN id DROP DEFAULT;
       public       redmine    false    212    211    212            k           2604    24975 
   queries id    DEFAULT     h   ALTER TABLE ONLY public.queries ALTER COLUMN id SET DEFAULT nextval('public.queries_id_seq'::regclass);
 9   ALTER TABLE public.queries ALTER COLUMN id DROP DEFAULT;
       public       redmine    false    233    234    234            o           2604    24989    repositories id    DEFAULT     r   ALTER TABLE ONLY public.repositories ALTER COLUMN id SET DEFAULT nextval('public.repositories_id_seq'::regclass);
 >   ALTER TABLE public.repositories ALTER COLUMN id DROP DEFAULT;
       public       redmine    false    236    235    236            3           2604    24819    roles id    DEFAULT     d   ALTER TABLE ONLY public.roles ALTER COLUMN id SET DEFAULT nextval('public.roles_id_seq'::regclass);
 7   ALTER TABLE public.roles ALTER COLUMN id DROP DEFAULT;
       public       redmine    false    214    213    214            x           2604    25002    settings id    DEFAULT     j   ALTER TABLE ONLY public.settings ALTER COLUMN id SET DEFAULT nextval('public.settings_id_seq'::regclass);
 :   ALTER TABLE public.settings ALTER COLUMN id DROP DEFAULT;
       public       redmine    false    238    237    238            �           2604    25104    time_entries id    DEFAULT     r   ALTER TABLE ONLY public.time_entries ALTER COLUMN id SET DEFAULT nextval('public.time_entries_id_seq'::regclass);
 >   ALTER TABLE public.time_entries ALTER COLUMN id DROP DEFAULT;
       public       redmine    false    247    248    248            ;           2604    24828 	   tokens id    DEFAULT     f   ALTER TABLE ONLY public.tokens ALTER COLUMN id SET DEFAULT nextval('public.tokens_id_seq'::regclass);
 8   ALTER TABLE public.tokens ALTER COLUMN id DROP DEFAULT;
       public       redmine    false    215    216    216            ?           2604    24839    trackers id    DEFAULT     j   ALTER TABLE ONLY public.trackers ALTER COLUMN id SET DEFAULT nextval('public.trackers_id_seq'::regclass);
 :   ALTER TABLE public.trackers ALTER COLUMN id DROP DEFAULT;
       public       redmine    false    217    218    218            d           2604    24932    user_preferences id    DEFAULT     z   ALTER TABLE ONLY public.user_preferences ALTER COLUMN id SET DEFAULT nextval('public.user_preferences_id_seq'::regclass);
 B   ALTER TABLE public.user_preferences ALTER COLUMN id DROP DEFAULT;
       public       redmine    false    229    230    230            D           2604    24849    users id    DEFAULT     d   ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);
 7   ALTER TABLE public.users ALTER COLUMN id DROP DEFAULT;
       public       redmine    false    219    220    220            O           2604    24866    versions id    DEFAULT     j   ALTER TABLE ONLY public.versions ALTER COLUMN id SET DEFAULT nextval('public.versions_id_seq'::regclass);
 :   ALTER TABLE public.versions ALTER COLUMN id DROP DEFAULT;
       public       redmine    false    221    222    222            �           2604    25148    watchers id    DEFAULT     j   ALTER TABLE ONLY public.watchers ALTER COLUMN id SET DEFAULT nextval('public.watchers_id_seq'::regclass);
 :   ALTER TABLE public.watchers ALTER COLUMN id DROP DEFAULT;
       public       redmine    false    253    254    254            �           2604    25082    wiki_content_versions id    DEFAULT     �   ALTER TABLE ONLY public.wiki_content_versions ALTER COLUMN id SET DEFAULT nextval('public.wiki_content_versions_id_seq'::regclass);
 G   ALTER TABLE public.wiki_content_versions ALTER COLUMN id DROP DEFAULT;
       public       redmine    false    245    246    246            ~           2604    25069    wiki_contents id    DEFAULT     t   ALTER TABLE ONLY public.wiki_contents ALTER COLUMN id SET DEFAULT nextval('public.wiki_contents_id_seq'::regclass);
 ?   ALTER TABLE public.wiki_contents ALTER COLUMN id DROP DEFAULT;
       public       redmine    false    243    244    244            |           2604    25060    wiki_pages id    DEFAULT     n   ALTER TABLE ONLY public.wiki_pages ALTER COLUMN id SET DEFAULT nextval('public.wiki_pages_id_seq'::regclass);
 <   ALTER TABLE public.wiki_pages ALTER COLUMN id DROP DEFAULT;
       public       redmine    false    242    241    242            �           2604    25232    wiki_redirects id    DEFAULT     v   ALTER TABLE ONLY public.wiki_redirects ALTER COLUMN id SET DEFAULT nextval('public.wiki_redirects_id_seq'::regclass);
 @   ALTER TABLE public.wiki_redirects ALTER COLUMN id DROP DEFAULT;
       public       redmine    false    263    262    263            z           2604    25050    wikis id    DEFAULT     d   ALTER TABLE ONLY public.wikis ALTER COLUMN id SET DEFAULT nextval('public.wikis_id_seq'::regclass);
 7   ALTER TABLE public.wikis ALTER COLUMN id DROP DEFAULT;
       public       redmine    false    239    240    240            T           2604    24881    workflows id    DEFAULT     l   ALTER TABLE ONLY public.workflows ALTER COLUMN id SET DEFAULT nextval('public.workflows_id_seq'::regclass);
 ;   ALTER TABLE public.workflows ALTER COLUMN id DROP DEFAULT;
       public       redmine    false    224    223    224            ,          0    24584    ar_internal_metadata 
   TABLE DATA               R   COPY public.ar_internal_metadata (key, value, created_at, updated_at) FROM stdin;
    public       redmine    false    186   ��      .          0    24594    attachments 
   TABLE DATA               �   COPY public.attachments (id, container_id, container_type, filename, disk_filename, filesize, content_type, digest, downloads, author_id, created_on, description, disk_directory) FROM stdin;
    public       redmine    false    188   
�      0          0    24614    auth_sources 
   TABLE DATA               �   COPY public.auth_sources (id, type, name, host, port, account, account_password, base_dn, attr_login, attr_firstname, attr_lastname, attr_mail, onthefly_register, tls, filter, timeout, verify_peer) FROM stdin;
    public       redmine    false    190   ��      u          0    25176    boards 
   TABLE DATA               �   COPY public.boards (id, project_id, name, description, "position", topics_count, messages_count, last_message_id, parent_id) FROM stdin;
    public       redmine    false    259   ݽ      n          0    25123    changes 
   TABLE DATA               m   COPY public.changes (id, changeset_id, action, path, from_path, from_revision, revision, branch) FROM stdin;
    public       redmine    false    252   ��      �          0    25591    changeset_parents 
   TABLE DATA               D   COPY public.changeset_parents (changeset_id, parent_id) FROM stdin;
    public       redmine    false    274   �      l          0    25111 
   changesets 
   TABLE DATA               �   COPY public.changesets (id, repository_id, revision, committer, committed_on, comments, commit_date, scmid, user_id) FROM stdin;
    public       redmine    false    250   4�      q          0    25156    changesets_issues 
   TABLE DATA               C   COPY public.changesets_issues (changeset_id, issue_id) FROM stdin;
    public       redmine    false    255   Q�      Z          0    24949    comments 
   TABLE DATA               p   COPY public.comments (id, commented_type, commented_id, author_id, content, created_on, updated_on) FROM stdin;
    public       redmine    false    232   n�      �          0    25775    custom_field_enumerations 
   TABLE DATA               b   COPY public.custom_field_enumerations (id, custom_field_id, name, active, "position") FROM stdin;
    public       redmine    false    285   ��      2          0    24628    custom_fields 
   TABLE DATA                 COPY public.custom_fields (id, type, name, field_format, possible_values, regexp, min_length, max_length, is_required, is_for_all, is_filter, "position", searchable, default_value, editable, visible, multiple, format_store, description, dmsf_not_inheritable) FROM stdin;
    public       redmine    false    192   ��      3          0    24645    custom_fields_projects 
   TABLE DATA               M   COPY public.custom_fields_projects (custom_field_id, project_id) FROM stdin;
    public       redmine    false    193   ��      �          0    25666    custom_fields_roles 
   TABLE DATA               G   COPY public.custom_fields_roles (custom_field_id, role_id) FROM stdin;
    public       redmine    false    276   ֿ      4          0    24650    custom_fields_trackers 
   TABLE DATA               M   COPY public.custom_fields_trackers (custom_field_id, tracker_id) FROM stdin;
    public       redmine    false    194   �      6          0    24657    custom_values 
   TABLE DATA               c   COPY public.custom_values (id, customized_type, customized_id, custom_field_id, value) FROM stdin;
    public       redmine    false    196   �      �          0    27373    dmsf_file_revision_accesses 
   TABLE DATA               y   COPY public.dmsf_file_revision_accesses (id, dmsf_file_revision_id, action, user_id, created_at, updated_at) FROM stdin;
    public       redmine    false    295   ��      �          0    27344    dmsf_file_revisions 
   TABLE DATA               �  COPY public.dmsf_file_revisions (id, dmsf_file_id, source_dmsf_file_revision_id, name, disk_filename, size, mime_type, title, description, workflow, major_version, minor_version, comment, deleted, deleted_by_user_id, user_id, created_at, updated_at, dmsf_workflow_id, dmsf_workflow_assigned_by_user_id, dmsf_workflow_assigned_at, dmsf_workflow_started_by_user_id, dmsf_workflow_started_at, digest) FROM stdin;
    public       redmine    false    291   B�      �          0    27331 
   dmsf_files 
   TABLE DATA               �   COPY public.dmsf_files (id, project_id, dmsf_folder_id, name, notification, deleted, deleted_by_user_id, created_at, updated_at) FROM stdin;
    public       redmine    false    289   �      �          0    27541    dmsf_folder_permissions 
   TABLE DATA               ]   COPY public.dmsf_folder_permissions (id, dmsf_folder_id, object_id, object_type) FROM stdin;
    public       redmine    false    309   3�      �          0    27319    dmsf_folders 
   TABLE DATA               �   COPY public.dmsf_folders (id, project_id, dmsf_folder_id, title, description, notification, user_id, created_at, updated_at, deleted, deleted_by_user_id, system) FROM stdin;
    public       redmine    false    287   P�      �          0    27428 
   dmsf_links 
   TABLE DATA               �   COPY public.dmsf_links (id, target_project_id, target_id, target_type, name, project_id, dmsf_folder_id, deleted, deleted_by_user_id, created_at, updated_at, external_url, user_id) FROM stdin;
    public       redmine    false    305   m�      �          0    27356 
   dmsf_locks 
   TABLE DATA               �   COPY public.dmsf_locks (id, entity_id, user_id, created_at, updated_at, entity_type, lock_type_cd, lock_scope_cd, uuid, expires_at, dmsf_file_last_revision_id) FROM stdin;
    public       redmine    false    293    �      �          0    27523    dmsf_public_urls 
   TABLE DATA               o   COPY public.dmsf_public_urls (id, token, dmsf_file_id, user_id, expire_at, created_at, updated_at) FROM stdin;
    public       redmine    false    307   ��      �          0    27412    dmsf_workflow_step_actions 
   TABLE DATA                  COPY public.dmsf_workflow_step_actions (id, dmsf_workflow_step_assignment_id, action, note, created_at, author_id) FROM stdin;
    public       redmine    false    303   ��      �          0    27403    dmsf_workflow_step_assignments 
   TABLE DATA               s   COPY public.dmsf_workflow_step_assignments (id, dmsf_workflow_step_id, user_id, dmsf_file_revision_id) FROM stdin;
    public       redmine    false    301   ��      �          0    27394    dmsf_workflow_steps 
   TABLE DATA               b   COPY public.dmsf_workflow_steps (id, dmsf_workflow_id, step, user_id, operator, name) FROM stdin;
    public       redmine    false    299   ��      �          0    27382    dmsf_workflows 
   TABLE DATA               i   COPY public.dmsf_workflows (id, name, project_id, status, updated_on, created_on, author_id) FROM stdin;
    public       redmine    false    297   ��      8          0    24671 	   documents 
   TABLE DATA               `   COPY public.documents (id, project_id, category_id, title, description, created_on) FROM stdin;
    public       redmine    false    198   �      �          0    30390    easy_entity_assignments 
   TABLE DATA               �   COPY public.easy_entity_assignments (id, entity_from_type, entity_from_id, entity_to_type, entity_to_id, created_at, updated_at) FROM stdin;
    public       redmine    false    313   �      �          0    30372    easy_settings 
   TABLE DATA               J   COPY public.easy_settings (id, type, name, value, project_id) FROM stdin;
    public       redmine    false    311   2�      �          0    25693    email_addresses 
   TABLE DATA               k   COPY public.email_addresses (id, user_id, address, is_default, notify, created_on, updated_on) FROM stdin;
    public       redmine    false    278   O�      {          0    25241    enabled_modules 
   TABLE DATA               ?   COPY public.enabled_modules (id, project_id, name) FROM stdin;
    public       redmine    false    265   ��      :          0    24686    enumerations 
   TABLE DATA               |   COPY public.enumerations (id, name, "position", is_default, type, active, project_id, parent_id, position_name) FROM stdin;
    public       redmine    false    200   �      �          0    25411    groups_users 
   TABLE DATA               9   COPY public.groups_users (group_id, user_id) FROM stdin;
    public       redmine    false    273   N�      �          0    25741    import_items 
   TABLE DATA               R   COPY public.import_items (id, import_id, "position", obj_id, message) FROM stdin;
    public       redmine    false    283   k�      �          0    25729    imports 
   TABLE DATA               w   COPY public.imports (id, type, user_id, filename, settings, total_items, finished, created_at, updated_at) FROM stdin;
    public       redmine    false    281   w      <          0    24696    issue_categories 
   TABLE DATA               P   COPY public.issue_categories (id, project_id, name, assigned_to_id) FROM stdin;
    public       redmine    false    202   �{      s          0    25162    issue_relations 
   TABLE DATA               _   COPY public.issue_relations (id, issue_from_id, issue_to_id, relation_type, delay) FROM stdin;
    public       redmine    false    257   N|      >          0    24722    issue_statuses 
   TABLE DATA               ]   COPY public.issue_statuses (id, name, is_closed, "position", default_done_ratio) FROM stdin;
    public       redmine    false    204   k|      @          0    24734    issues 
   TABLE DATA               2  COPY public.issues (id, tracker_id, project_id, subject, description, due_date, category_id, status_id, assigned_to_id, priority_id, fixed_version_id, author_id, lock_version, created_on, updated_on, start_date, done_ratio, estimated_hours, parent_id, root_id, lft, rgt, is_private, closed_on) FROM stdin;
    public       redmine    false    206   �|      V          0    24913    journal_details 
   TABLE DATA               _   COPY public.journal_details (id, journal_id, property, prop_key, old_value, value) FROM stdin;
    public       redmine    false    228   =�      T          0    24899    journals 
   TABLE DATA               s   COPY public.journals (id, journalized_id, journalized_type, user_id, notes, created_on, private_notes) FROM stdin;
    public       redmine    false    226   ��      �          0    25405    member_roles 
   TABLE DATA               N   COPY public.member_roles (id, member_id, role_id, inherited_from) FROM stdin;
    public       redmine    false    272   �      B          0    24753    members 
   TABLE DATA               �   COPY public.members (id, user_id, project_id, created_on, mail_notification, dmsf_mail_notification, dmsf_title_format, dmsf_fast_links) FROM stdin;
    public       redmine    false    208   �      w          0    25192    messages 
   TABLE DATA               �   COPY public.messages (id, board_id, parent_id, subject, content, author_id, replies_count, last_reply_id, created_on, updated_on, locked, sticky) FROM stdin;
    public       redmine    false    261   V�      D          0    24764    news 
   TABLE DATA               r   COPY public.news (id, project_id, title, summary, description, author_id, created_on, comments_count) FROM stdin;
    public       redmine    false    210   s�      ~          0    25367 #   open_id_authentication_associations 
   TABLE DATA               {   COPY public.open_id_authentication_associations (id, issued, lifetime, handle, assoc_type, server_url, secret) FROM stdin;
    public       redmine    false    268   ��      �          0    25378    open_id_authentication_nonces 
   TABLE DATA               Z   COPY public.open_id_authentication_nonces (id, "timestamp", server_url, salt) FROM stdin;
    public       redmine    false    270   ��      F          0    24800    projects 
   TABLE DATA               	  COPY public.projects (id, name, description, homepage, is_public, parent_id, created_on, updated_on, identifier, status, lft, rgt, inherit_members, default_version_id, default_assigned_to_id, dmsf_description, dmsf_notification, dmsf_act_as_attachable) FROM stdin;
    public       redmine    false    212   ʖ      |          0    25296    projects_trackers 
   TABLE DATA               C   COPY public.projects_trackers (project_id, tracker_id) FROM stdin;
    public       redmine    false    266   @�      \          0    24972    queries 
   TABLE DATA               �   COPY public.queries (id, project_id, name, filters, user_id, column_names, sort_criteria, group_by, type, visibility, options) FROM stdin;
    public       redmine    false    234   g�      �          0    25652    queries_roles 
   TABLE DATA               :   COPY public.queries_roles (query_id, role_id) FROM stdin;
    public       redmine    false    275   ��      ^          0    24986    repositories 
   TABLE DATA               �   COPY public.repositories (id, project_id, url, login, password, root_url, type, path_encoding, log_encoding, extra_info, identifier, is_default, created_on) FROM stdin;
    public       redmine    false    236   ��      H          0    24816    roles 
   TABLE DATA               �   COPY public.roles (id, name, "position", assignable, builtin, permissions, issues_visibility, users_visibility, time_entries_visibility, all_roles_managed, settings) FROM stdin;
    public       redmine    false    214   ��      �          0    25723    roles_managed_roles 
   TABLE DATA               G   COPY public.roles_managed_roles (role_id, managed_role_id) FROM stdin;
    public       redmine    false    279   �      +          0    24576    schema_migrations 
   TABLE DATA               4   COPY public.schema_migrations (version) FROM stdin;
    public       redmine    false    185   /�      `          0    24999    settings 
   TABLE DATA               ?   COPY public.settings (id, name, value, updated_on) FROM stdin;
    public       redmine    false    238   ��      j          0    25101    time_entries 
   TABLE DATA               �   COPY public.time_entries (id, project_id, user_id, issue_id, hours, comments, activity_id, spent_on, tyear, tmonth, tweek, created_on, updated_on) FROM stdin;
    public       redmine    false    248   �      J          0    24825    tokens 
   TABLE DATA               T   COPY public.tokens (id, user_id, action, value, created_on, updated_on) FROM stdin;
    public       redmine    false    216   �      L          0    24836    trackers 
   TABLE DATA               t   COPY public.trackers (id, name, is_in_chlog, "position", is_in_roadmap, fields_bits, default_status_id) FROM stdin;
    public       redmine    false    218   �      X          0    24929    user_preferences 
   TABLE DATA               U   COPY public.user_preferences (id, user_id, others, hide_mail, time_zone) FROM stdin;
    public       redmine    false    230   q�      N          0    24846    users 
   TABLE DATA               �   COPY public.users (id, login, hashed_password, firstname, lastname, admin, status, last_login_on, language, auth_source_id, created_on, updated_on, type, identity_url, mail_notification, salt, must_change_passwd, passwd_changed_on) FROM stdin;
    public       redmine    false    220   ��      P          0    24863    versions 
   TABLE DATA               �   COPY public.versions (id, project_id, name, description, effective_date, created_on, updated_on, wiki_page_title, status, sharing) FROM stdin;
    public       redmine    false    222   '�      p          0    25145    watchers 
   TABLE DATA               M   COPY public.watchers (id, watchable_type, watchable_id, user_id) FROM stdin;
    public       redmine    false    254   O�      h          0    25079    wiki_content_versions 
   TABLE DATA               �   COPY public.wiki_content_versions (id, wiki_content_id, page_id, author_id, data, compression, comments, updated_on, version) FROM stdin;
    public       redmine    false    246   z�      f          0    25066    wiki_contents 
   TABLE DATA               d   COPY public.wiki_contents (id, page_id, author_id, text, comments, updated_on, version) FROM stdin;
    public       redmine    false    244   ��      d          0    25057 
   wiki_pages 
   TABLE DATA               Z   COPY public.wiki_pages (id, wiki_id, title, created_on, protected, parent_id) FROM stdin;
    public       redmine    false    242   ��      y          0    25229    wiki_redirects 
   TABLE DATA               l   COPY public.wiki_redirects (id, wiki_id, title, redirects_to, created_on, redirects_to_wiki_id) FROM stdin;
    public       redmine    false    263   ѱ      b          0    25047    wikis 
   TABLE DATA               C   COPY public.wikis (id, project_id, start_page, status) FROM stdin;
    public       redmine    false    240   �      R          0    24878 	   workflows 
   TABLE DATA               �   COPY public.workflows (id, tracker_id, old_status_id, new_status_id, role_id, assignee, author, type, field_name, rule) FROM stdin;
    public       redmine    false    224   �      �           0    0    attachments_id_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('public.attachments_id_seq', 19, true);
            public       redmine    false    187            �           0    0    auth_sources_id_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('public.auth_sources_id_seq', 1, false);
            public       redmine    false    189            �           0    0    boards_id_seq    SEQUENCE SET     <   SELECT pg_catalog.setval('public.boards_id_seq', 1, false);
            public       redmine    false    258            �           0    0    changes_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('public.changes_id_seq', 1, false);
            public       redmine    false    251            �           0    0    changesets_id_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('public.changesets_id_seq', 1, false);
            public       redmine    false    249            �           0    0    comments_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('public.comments_id_seq', 1, false);
            public       redmine    false    231            �           0    0     custom_field_enumerations_id_seq    SEQUENCE SET     O   SELECT pg_catalog.setval('public.custom_field_enumerations_id_seq', 1, false);
            public       redmine    false    284            �           0    0    custom_fields_id_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public.custom_fields_id_seq', 15, true);
            public       redmine    false    191            �           0    0    custom_values_id_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('public.custom_values_id_seq', 174, true);
            public       redmine    false    195            �           0    0 "   dmsf_file_revision_accesses_id_seq    SEQUENCE SET     R   SELECT pg_catalog.setval('public.dmsf_file_revision_accesses_id_seq', 189, true);
            public       redmine    false    294            �           0    0    dmsf_file_revisions_id_seq    SEQUENCE SET     I   SELECT pg_catalog.setval('public.dmsf_file_revisions_id_seq', 41, true);
            public       redmine    false    290            �           0    0    dmsf_files_id_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('public.dmsf_files_id_seq', 21, true);
            public       redmine    false    288            �           0    0    dmsf_folder_permissions_id_seq    SEQUENCE SET     M   SELECT pg_catalog.setval('public.dmsf_folder_permissions_id_seq', 1, false);
            public       redmine    false    308            �           0    0    dmsf_folders_id_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('public.dmsf_folders_id_seq', 2, true);
            public       redmine    false    286            �           0    0    dmsf_links_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('public.dmsf_links_id_seq', 4, true);
            public       redmine    false    304            �           0    0    dmsf_locks_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('public.dmsf_locks_id_seq', 1, true);
            public       redmine    false    292            �           0    0    dmsf_public_urls_id_seq    SEQUENCE SET     F   SELECT pg_catalog.setval('public.dmsf_public_urls_id_seq', 1, false);
            public       redmine    false    306                        0    0 !   dmsf_workflow_step_actions_id_seq    SEQUENCE SET     P   SELECT pg_catalog.setval('public.dmsf_workflow_step_actions_id_seq', 1, false);
            public       redmine    false    302                       0    0 %   dmsf_workflow_step_assignments_id_seq    SEQUENCE SET     T   SELECT pg_catalog.setval('public.dmsf_workflow_step_assignments_id_seq', 1, false);
            public       redmine    false    300                       0    0    dmsf_workflow_steps_id_seq    SEQUENCE SET     H   SELECT pg_catalog.setval('public.dmsf_workflow_steps_id_seq', 1, true);
            public       redmine    false    298                       0    0    dmsf_workflows_id_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public.dmsf_workflows_id_seq', 2, true);
            public       redmine    false    296                       0    0    documents_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('public.documents_id_seq', 6, true);
            public       redmine    false    197                       0    0    easy_entity_assignments_id_seq    SEQUENCE SET     M   SELECT pg_catalog.setval('public.easy_entity_assignments_id_seq', 1, false);
            public       redmine    false    312                       0    0    easy_settings_id_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public.easy_settings_id_seq', 1, false);
            public       redmine    false    310                       0    0    email_addresses_id_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('public.email_addresses_id_seq', 2, true);
            public       redmine    false    277                       0    0    enabled_modules_id_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('public.enabled_modules_id_seq', 22, true);
            public       redmine    false    264            	           0    0    enumerations_id_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('public.enumerations_id_seq', 14, true);
            public       redmine    false    199            
           0    0    import_items_id_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('public.import_items_id_seq', 6022, true);
            public       redmine    false    282                       0    0    imports_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('public.imports_id_seq', 15, true);
            public       redmine    false    280                       0    0    issue_categories_id_seq    SEQUENCE SET     F   SELECT pg_catalog.setval('public.issue_categories_id_seq', 36, true);
            public       redmine    false    201                       0    0    issue_relations_id_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('public.issue_relations_id_seq', 2, true);
            public       redmine    false    256                       0    0    issue_statuses_id_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public.issue_statuses_id_seq', 6, true);
            public       redmine    false    203                       0    0    issues_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('public.issues_id_seq', 855, true);
            public       redmine    false    205                       0    0    journal_details_id_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('public.journal_details_id_seq', 40, true);
            public       redmine    false    227                       0    0    journals_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('public.journals_id_seq', 47, true);
            public       redmine    false    225                       0    0    member_roles_id_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('public.member_roles_id_seq', 3, true);
            public       redmine    false    271                       0    0    members_id_seq    SEQUENCE SET     <   SELECT pg_catalog.setval('public.members_id_seq', 2, true);
            public       redmine    false    207                       0    0    messages_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('public.messages_id_seq', 1, false);
            public       redmine    false    260                       0    0    news_id_seq    SEQUENCE SET     :   SELECT pg_catalog.setval('public.news_id_seq', 1, false);
            public       redmine    false    209                       0    0 *   open_id_authentication_associations_id_seq    SEQUENCE SET     Y   SELECT pg_catalog.setval('public.open_id_authentication_associations_id_seq', 1, false);
            public       redmine    false    267                       0    0 $   open_id_authentication_nonces_id_seq    SEQUENCE SET     S   SELECT pg_catalog.setval('public.open_id_authentication_nonces_id_seq', 1, false);
            public       redmine    false    269                       0    0    projects_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('public.projects_id_seq', 3, true);
            public       redmine    false    211                       0    0    queries_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('public.queries_id_seq', 1, false);
            public       redmine    false    233                       0    0    repositories_id_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('public.repositories_id_seq', 1, false);
            public       redmine    false    235                       0    0    roles_id_seq    SEQUENCE SET     :   SELECT pg_catalog.setval('public.roles_id_seq', 5, true);
            public       redmine    false    213                       0    0    settings_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('public.settings_id_seq', 30, true);
            public       redmine    false    237                       0    0    time_entries_id_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('public.time_entries_id_seq', 7, true);
            public       redmine    false    247                       0    0    tokens_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('public.tokens_id_seq', 133, true);
            public       redmine    false    215                       0    0    trackers_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('public.trackers_id_seq', 3, true);
            public       redmine    false    217                        0    0    user_preferences_id_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('public.user_preferences_id_seq', 4, true);
            public       redmine    false    229            !           0    0    users_id_seq    SEQUENCE SET     :   SELECT pg_catalog.setval('public.users_id_seq', 5, true);
            public       redmine    false    219            "           0    0    versions_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('public.versions_id_seq', 324, true);
            public       redmine    false    221            #           0    0    watchers_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('public.watchers_id_seq', 1, true);
            public       redmine    false    253            $           0    0    wiki_content_versions_id_seq    SEQUENCE SET     K   SELECT pg_catalog.setval('public.wiki_content_versions_id_seq', 1, false);
            public       redmine    false    245            %           0    0    wiki_contents_id_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public.wiki_contents_id_seq', 1, false);
            public       redmine    false    243            &           0    0    wiki_pages_id_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('public.wiki_pages_id_seq', 1, false);
            public       redmine    false    241            '           0    0    wiki_redirects_id_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('public.wiki_redirects_id_seq', 1, false);
            public       redmine    false    262            (           0    0    wikis_id_seq    SEQUENCE SET     :   SELECT pg_catalog.setval('public.wikis_id_seq', 2, true);
            public       redmine    false    239            )           0    0    workflows_id_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('public.workflows_id_seq', 144, true);
            public       redmine    false    223            �           2606    24591 .   ar_internal_metadata ar_internal_metadata_pkey 
   CONSTRAINT     m   ALTER TABLE ONLY public.ar_internal_metadata
    ADD CONSTRAINT ar_internal_metadata_pkey PRIMARY KEY (key);
 X   ALTER TABLE ONLY public.ar_internal_metadata DROP CONSTRAINT ar_internal_metadata_pkey;
       public         redmine    false    186            �           2606    24611    attachments attachments_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY public.attachments
    ADD CONSTRAINT attachments_pkey PRIMARY KEY (id);
 F   ALTER TABLE ONLY public.attachments DROP CONSTRAINT attachments_pkey;
       public         redmine    false    188            �           2606    24625    auth_sources auth_sources_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.auth_sources
    ADD CONSTRAINT auth_sources_pkey PRIMARY KEY (id);
 H   ALTER TABLE ONLY public.auth_sources DROP CONSTRAINT auth_sources_pkey;
       public         redmine    false    190            _           2606    25188    boards boards_pkey 
   CONSTRAINT     P   ALTER TABLE ONLY public.boards
    ADD CONSTRAINT boards_pkey PRIMARY KEY (id);
 <   ALTER TABLE ONLY public.boards DROP CONSTRAINT boards_pkey;
       public         redmine    false    259            P           2606    25133    changes changes_pkey 
   CONSTRAINT     R   ALTER TABLE ONLY public.changes
    ADD CONSTRAINT changes_pkey PRIMARY KEY (id);
 >   ALTER TABLE ONLY public.changes DROP CONSTRAINT changes_pkey;
       public         redmine    false    252            I           2606    25119    changesets changesets_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY public.changesets
    ADD CONSTRAINT changesets_pkey PRIMARY KEY (id);
 D   ALTER TABLE ONLY public.changesets DROP CONSTRAINT changesets_pkey;
       public         redmine    false    250            $           2606    24960    comments comments_pkey 
   CONSTRAINT     T   ALTER TABLE ONLY public.comments
    ADD CONSTRAINT comments_pkey PRIMARY KEY (id);
 @   ALTER TABLE ONLY public.comments DROP CONSTRAINT comments_pkey;
       public         redmine    false    232            �           2606    25785 8   custom_field_enumerations custom_field_enumerations_pkey 
   CONSTRAINT     v   ALTER TABLE ONLY public.custom_field_enumerations
    ADD CONSTRAINT custom_field_enumerations_pkey PRIMARY KEY (id);
 b   ALTER TABLE ONLY public.custom_field_enumerations DROP CONSTRAINT custom_field_enumerations_pkey;
       public         redmine    false    285            �           2606    24644     custom_fields custom_fields_pkey 
   CONSTRAINT     ^   ALTER TABLE ONLY public.custom_fields
    ADD CONSTRAINT custom_fields_pkey PRIMARY KEY (id);
 J   ALTER TABLE ONLY public.custom_fields DROP CONSTRAINT custom_fields_pkey;
       public         redmine    false    192            �           2606    24668     custom_values custom_values_pkey 
   CONSTRAINT     ^   ALTER TABLE ONLY public.custom_values
    ADD CONSTRAINT custom_values_pkey PRIMARY KEY (id);
 J   ALTER TABLE ONLY public.custom_values DROP CONSTRAINT custom_values_pkey;
       public         redmine    false    196            �           2606    27379 <   dmsf_file_revision_accesses dmsf_file_revision_accesses_pkey 
   CONSTRAINT     z   ALTER TABLE ONLY public.dmsf_file_revision_accesses
    ADD CONSTRAINT dmsf_file_revision_accesses_pkey PRIMARY KEY (id);
 f   ALTER TABLE ONLY public.dmsf_file_revision_accesses DROP CONSTRAINT dmsf_file_revision_accesses_pkey;
       public         redmine    false    295            �           2606    27353 ,   dmsf_file_revisions dmsf_file_revisions_pkey 
   CONSTRAINT     j   ALTER TABLE ONLY public.dmsf_file_revisions
    ADD CONSTRAINT dmsf_file_revisions_pkey PRIMARY KEY (id);
 V   ALTER TABLE ONLY public.dmsf_file_revisions DROP CONSTRAINT dmsf_file_revisions_pkey;
       public         redmine    false    291            �           2606    27341    dmsf_files dmsf_files_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY public.dmsf_files
    ADD CONSTRAINT dmsf_files_pkey PRIMARY KEY (id);
 D   ALTER TABLE ONLY public.dmsf_files DROP CONSTRAINT dmsf_files_pkey;
       public         redmine    false    289            �           2606    27546 4   dmsf_folder_permissions dmsf_folder_permissions_pkey 
   CONSTRAINT     r   ALTER TABLE ONLY public.dmsf_folder_permissions
    ADD CONSTRAINT dmsf_folder_permissions_pkey PRIMARY KEY (id);
 ^   ALTER TABLE ONLY public.dmsf_folder_permissions DROP CONSTRAINT dmsf_folder_permissions_pkey;
       public         redmine    false    309            �           2606    27328    dmsf_folders dmsf_folders_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.dmsf_folders
    ADD CONSTRAINT dmsf_folders_pkey PRIMARY KEY (id);
 H   ALTER TABLE ONLY public.dmsf_folders DROP CONSTRAINT dmsf_folders_pkey;
       public         redmine    false    287            �           2606    27437    dmsf_links dmsf_links_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY public.dmsf_links
    ADD CONSTRAINT dmsf_links_pkey PRIMARY KEY (id);
 D   ALTER TABLE ONLY public.dmsf_links DROP CONSTRAINT dmsf_links_pkey;
       public         redmine    false    305            �           2606    27362    dmsf_locks dmsf_locks_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY public.dmsf_locks
    ADD CONSTRAINT dmsf_locks_pkey PRIMARY KEY (id);
 D   ALTER TABLE ONLY public.dmsf_locks DROP CONSTRAINT dmsf_locks_pkey;
       public         redmine    false    293            �           2606    27528 &   dmsf_public_urls dmsf_public_urls_pkey 
   CONSTRAINT     d   ALTER TABLE ONLY public.dmsf_public_urls
    ADD CONSTRAINT dmsf_public_urls_pkey PRIMARY KEY (id);
 P   ALTER TABLE ONLY public.dmsf_public_urls DROP CONSTRAINT dmsf_public_urls_pkey;
       public         redmine    false    307            �           2606    27420 :   dmsf_workflow_step_actions dmsf_workflow_step_actions_pkey 
   CONSTRAINT     x   ALTER TABLE ONLY public.dmsf_workflow_step_actions
    ADD CONSTRAINT dmsf_workflow_step_actions_pkey PRIMARY KEY (id);
 d   ALTER TABLE ONLY public.dmsf_workflow_step_actions DROP CONSTRAINT dmsf_workflow_step_actions_pkey;
       public         redmine    false    303            �           2606    27408 B   dmsf_workflow_step_assignments dmsf_workflow_step_assignments_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public.dmsf_workflow_step_assignments
    ADD CONSTRAINT dmsf_workflow_step_assignments_pkey PRIMARY KEY (id);
 l   ALTER TABLE ONLY public.dmsf_workflow_step_assignments DROP CONSTRAINT dmsf_workflow_step_assignments_pkey;
       public         redmine    false    301            �           2606    27399 ,   dmsf_workflow_steps dmsf_workflow_steps_pkey 
   CONSTRAINT     j   ALTER TABLE ONLY public.dmsf_workflow_steps
    ADD CONSTRAINT dmsf_workflow_steps_pkey PRIMARY KEY (id);
 V   ALTER TABLE ONLY public.dmsf_workflow_steps DROP CONSTRAINT dmsf_workflow_steps_pkey;
       public         redmine    false    299            �           2606    27390 "   dmsf_workflows dmsf_workflows_pkey 
   CONSTRAINT     `   ALTER TABLE ONLY public.dmsf_workflows
    ADD CONSTRAINT dmsf_workflows_pkey PRIMARY KEY (id);
 L   ALTER TABLE ONLY public.dmsf_workflows DROP CONSTRAINT dmsf_workflows_pkey;
       public         redmine    false    297            �           2606    24682    documents documents_pkey 
   CONSTRAINT     V   ALTER TABLE ONLY public.documents
    ADD CONSTRAINT documents_pkey PRIMARY KEY (id);
 B   ALTER TABLE ONLY public.documents DROP CONSTRAINT documents_pkey;
       public         redmine    false    198            �           2606    30398 4   easy_entity_assignments easy_entity_assignments_pkey 
   CONSTRAINT     r   ALTER TABLE ONLY public.easy_entity_assignments
    ADD CONSTRAINT easy_entity_assignments_pkey PRIMARY KEY (id);
 ^   ALTER TABLE ONLY public.easy_entity_assignments DROP CONSTRAINT easy_entity_assignments_pkey;
       public         redmine    false    313            �           2606    30380     easy_settings easy_settings_pkey 
   CONSTRAINT     ^   ALTER TABLE ONLY public.easy_settings
    ADD CONSTRAINT easy_settings_pkey PRIMARY KEY (id);
 J   ALTER TABLE ONLY public.easy_settings DROP CONSTRAINT easy_settings_pkey;
       public         redmine    false    311            �           2606    25703 $   email_addresses email_addresses_pkey 
   CONSTRAINT     b   ALTER TABLE ONLY public.email_addresses
    ADD CONSTRAINT email_addresses_pkey PRIMARY KEY (id);
 N   ALTER TABLE ONLY public.email_addresses DROP CONSTRAINT email_addresses_pkey;
       public         redmine    false    278            n           2606    25249 $   enabled_modules enabled_modules_pkey 
   CONSTRAINT     b   ALTER TABLE ONLY public.enabled_modules
    ADD CONSTRAINT enabled_modules_pkey PRIMARY KEY (id);
 N   ALTER TABLE ONLY public.enabled_modules DROP CONSTRAINT enabled_modules_pkey;
       public         redmine    false    265            �           2606    24693    enumerations enumerations_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.enumerations
    ADD CONSTRAINT enumerations_pkey PRIMARY KEY (id);
 H   ALTER TABLE ONLY public.enumerations DROP CONSTRAINT enumerations_pkey;
       public         redmine    false    200            �           2606    25749    import_items import_items_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.import_items
    ADD CONSTRAINT import_items_pkey PRIMARY KEY (id);
 H   ALTER TABLE ONLY public.import_items DROP CONSTRAINT import_items_pkey;
       public         redmine    false    283            �           2606    25738    imports imports_pkey 
   CONSTRAINT     R   ALTER TABLE ONLY public.imports
    ADD CONSTRAINT imports_pkey PRIMARY KEY (id);
 >   ALTER TABLE ONLY public.imports DROP CONSTRAINT imports_pkey;
       public         redmine    false    281            �           2606    24703 &   issue_categories issue_categories_pkey 
   CONSTRAINT     d   ALTER TABLE ONLY public.issue_categories
    ADD CONSTRAINT issue_categories_pkey PRIMARY KEY (id);
 P   ALTER TABLE ONLY public.issue_categories DROP CONSTRAINT issue_categories_pkey;
       public         redmine    false    202            ]           2606    25171 $   issue_relations issue_relations_pkey 
   CONSTRAINT     b   ALTER TABLE ONLY public.issue_relations
    ADD CONSTRAINT issue_relations_pkey PRIMARY KEY (id);
 N   ALTER TABLE ONLY public.issue_relations DROP CONSTRAINT issue_relations_pkey;
       public         redmine    false    257            �           2606    24731 "   issue_statuses issue_statuses_pkey 
   CONSTRAINT     `   ALTER TABLE ONLY public.issue_statuses
    ADD CONSTRAINT issue_statuses_pkey PRIMARY KEY (id);
 L   ALTER TABLE ONLY public.issue_statuses DROP CONSTRAINT issue_statuses_pkey;
       public         redmine    false    204            �           2606    24749    issues issues_pkey 
   CONSTRAINT     P   ALTER TABLE ONLY public.issues
    ADD CONSTRAINT issues_pkey PRIMARY KEY (id);
 <   ALTER TABLE ONLY public.issues DROP CONSTRAINT issues_pkey;
       public         redmine    false    206                       2606    24924 $   journal_details journal_details_pkey 
   CONSTRAINT     b   ALTER TABLE ONLY public.journal_details
    ADD CONSTRAINT journal_details_pkey PRIMARY KEY (id);
 N   ALTER TABLE ONLY public.journal_details DROP CONSTRAINT journal_details_pkey;
       public         redmine    false    228                       2606    24910    journals journals_pkey 
   CONSTRAINT     T   ALTER TABLE ONLY public.journals
    ADD CONSTRAINT journals_pkey PRIMARY KEY (id);
 @   ALTER TABLE ONLY public.journals DROP CONSTRAINT journals_pkey;
       public         redmine    false    226            z           2606    25410    member_roles member_roles_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.member_roles
    ADD CONSTRAINT member_roles_pkey PRIMARY KEY (id);
 H   ALTER TABLE ONLY public.member_roles DROP CONSTRAINT member_roles_pkey;
       public         redmine    false    272            �           2606    24761    members members_pkey 
   CONSTRAINT     R   ALTER TABLE ONLY public.members
    ADD CONSTRAINT members_pkey PRIMARY KEY (id);
 >   ALTER TABLE ONLY public.members DROP CONSTRAINT members_pkey;
       public         redmine    false    208            h           2606    25202    messages messages_pkey 
   CONSTRAINT     T   ALTER TABLE ONLY public.messages
    ADD CONSTRAINT messages_pkey PRIMARY KEY (id);
 @   ALTER TABLE ONLY public.messages DROP CONSTRAINT messages_pkey;
       public         redmine    false    261            �           2606    24775    news news_pkey 
   CONSTRAINT     L   ALTER TABLE ONLY public.news
    ADD CONSTRAINT news_pkey PRIMARY KEY (id);
 8   ALTER TABLE ONLY public.news DROP CONSTRAINT news_pkey;
       public         redmine    false    210            s           2606    25375 L   open_id_authentication_associations open_id_authentication_associations_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public.open_id_authentication_associations
    ADD CONSTRAINT open_id_authentication_associations_pkey PRIMARY KEY (id);
 v   ALTER TABLE ONLY public.open_id_authentication_associations DROP CONSTRAINT open_id_authentication_associations_pkey;
       public         redmine    false    268            u           2606    25386 @   open_id_authentication_nonces open_id_authentication_nonces_pkey 
   CONSTRAINT     ~   ALTER TABLE ONLY public.open_id_authentication_nonces
    ADD CONSTRAINT open_id_authentication_nonces_pkey PRIMARY KEY (id);
 j   ALTER TABLE ONLY public.open_id_authentication_nonces DROP CONSTRAINT open_id_authentication_nonces_pkey;
       public         redmine    false    270            �           2606    24813    projects projects_pkey 
   CONSTRAINT     T   ALTER TABLE ONLY public.projects
    ADD CONSTRAINT projects_pkey PRIMARY KEY (id);
 @   ALTER TABLE ONLY public.projects DROP CONSTRAINT projects_pkey;
       public         redmine    false    212            *           2606    24983    queries queries_pkey 
   CONSTRAINT     R   ALTER TABLE ONLY public.queries
    ADD CONSTRAINT queries_pkey PRIMARY KEY (id);
 >   ALTER TABLE ONLY public.queries DROP CONSTRAINT queries_pkey;
       public         redmine    false    234            -           2606    24996    repositories repositories_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.repositories
    ADD CONSTRAINT repositories_pkey PRIMARY KEY (id);
 H   ALTER TABLE ONLY public.repositories DROP CONSTRAINT repositories_pkey;
       public         redmine    false    236                        2606    24822    roles roles_pkey 
   CONSTRAINT     N   ALTER TABLE ONLY public.roles
    ADD CONSTRAINT roles_pkey PRIMARY KEY (id);
 :   ALTER TABLE ONLY public.roles DROP CONSTRAINT roles_pkey;
       public         redmine    false    214            �           2606    24583 (   schema_migrations schema_migrations_pkey 
   CONSTRAINT     k   ALTER TABLE ONLY public.schema_migrations
    ADD CONSTRAINT schema_migrations_pkey PRIMARY KEY (version);
 R   ALTER TABLE ONLY public.schema_migrations DROP CONSTRAINT schema_migrations_pkey;
       public         redmine    false    185            0           2606    25008    settings settings_pkey 
   CONSTRAINT     T   ALTER TABLE ONLY public.settings
    ADD CONSTRAINT settings_pkey PRIMARY KEY (id);
 @   ALTER TABLE ONLY public.settings DROP CONSTRAINT settings_pkey;
       public         redmine    false    238            F           2606    25106    time_entries time_entries_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.time_entries
    ADD CONSTRAINT time_entries_pkey PRIMARY KEY (id);
 H   ALTER TABLE ONLY public.time_entries DROP CONSTRAINT time_entries_pkey;
       public         redmine    false    248                       2606    24833    tokens tokens_pkey 
   CONSTRAINT     P   ALTER TABLE ONLY public.tokens
    ADD CONSTRAINT tokens_pkey PRIMARY KEY (id);
 <   ALTER TABLE ONLY public.tokens DROP CONSTRAINT tokens_pkey;
       public         redmine    false    216                       2606    24843    trackers trackers_pkey 
   CONSTRAINT     T   ALTER TABLE ONLY public.trackers
    ADD CONSTRAINT trackers_pkey PRIMARY KEY (id);
 @   ALTER TABLE ONLY public.trackers DROP CONSTRAINT trackers_pkey;
       public         redmine    false    218            "           2606    24938 &   user_preferences user_preferences_pkey 
   CONSTRAINT     d   ALTER TABLE ONLY public.user_preferences
    ADD CONSTRAINT user_preferences_pkey PRIMARY KEY (id);
 P   ALTER TABLE ONLY public.user_preferences DROP CONSTRAINT user_preferences_pkey;
       public         redmine    false    230                       2606    24860    users users_pkey 
   CONSTRAINT     N   ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);
 :   ALTER TABLE ONLY public.users DROP CONSTRAINT users_pkey;
       public         redmine    false    220                       2606    24874    versions versions_pkey 
   CONSTRAINT     T   ALTER TABLE ONLY public.versions
    ADD CONSTRAINT versions_pkey PRIMARY KEY (id);
 @   ALTER TABLE ONLY public.versions DROP CONSTRAINT versions_pkey;
       public         redmine    false    222            U           2606    25155    watchers watchers_pkey 
   CONSTRAINT     T   ALTER TABLE ONLY public.watchers
    ADD CONSTRAINT watchers_pkey PRIMARY KEY (id);
 @   ALTER TABLE ONLY public.watchers DROP CONSTRAINT watchers_pkey;
       public         redmine    false    254            ?           2606    25089 0   wiki_content_versions wiki_content_versions_pkey 
   CONSTRAINT     n   ALTER TABLE ONLY public.wiki_content_versions
    ADD CONSTRAINT wiki_content_versions_pkey PRIMARY KEY (id);
 Z   ALTER TABLE ONLY public.wiki_content_versions DROP CONSTRAINT wiki_content_versions_pkey;
       public         redmine    false    246            <           2606    25075     wiki_contents wiki_contents_pkey 
   CONSTRAINT     ^   ALTER TABLE ONLY public.wiki_contents
    ADD CONSTRAINT wiki_contents_pkey PRIMARY KEY (id);
 J   ALTER TABLE ONLY public.wiki_contents DROP CONSTRAINT wiki_contents_pkey;
       public         redmine    false    244            7           2606    25062    wiki_pages wiki_pages_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY public.wiki_pages
    ADD CONSTRAINT wiki_pages_pkey PRIMARY KEY (id);
 D   ALTER TABLE ONLY public.wiki_pages DROP CONSTRAINT wiki_pages_pkey;
       public         redmine    false    242            k           2606    25237 "   wiki_redirects wiki_redirects_pkey 
   CONSTRAINT     `   ALTER TABLE ONLY public.wiki_redirects
    ADD CONSTRAINT wiki_redirects_pkey PRIMARY KEY (id);
 L   ALTER TABLE ONLY public.wiki_redirects DROP CONSTRAINT wiki_redirects_pkey;
       public         redmine    false    263            2           2606    25053    wikis wikis_pkey 
   CONSTRAINT     N   ALTER TABLE ONLY public.wikis
    ADD CONSTRAINT wikis_pkey PRIMARY KEY (id);
 :   ALTER TABLE ONLY public.wikis DROP CONSTRAINT wikis_pkey;
       public         redmine    false    240                       2606    24887    workflows workflows_pkey 
   CONSTRAINT     V   ALTER TABLE ONLY public.workflows
    ADD CONSTRAINT workflows_pkey PRIMARY KEY (id);
 B   ALTER TABLE ONLY public.workflows DROP CONSTRAINT workflows_pkey;
       public         redmine    false    224            `           1259    25189    boards_project_id    INDEX     J   CREATE INDEX boards_project_id ON public.boards USING btree (project_id);
 %   DROP INDEX public.boards_project_id;
       public         redmine    false    259            |           1259    25594    changeset_parents_changeset_ids    INDEX     e   CREATE INDEX changeset_parents_changeset_ids ON public.changeset_parents USING btree (changeset_id);
 3   DROP INDEX public.changeset_parents_changeset_ids;
       public         redmine    false    274            }           1259    25595    changeset_parents_parent_ids    INDEX     _   CREATE INDEX changeset_parents_parent_ids ON public.changeset_parents USING btree (parent_id);
 0   DROP INDEX public.changeset_parents_parent_ids;
       public         redmine    false    274            Q           1259    25134    changesets_changeset_id    INDEX     S   CREATE INDEX changesets_changeset_id ON public.changes USING btree (changeset_id);
 +   DROP INDEX public.changesets_changeset_id;
       public         redmine    false    252            W           1259    25159    changesets_issues_ids    INDEX     l   CREATE UNIQUE INDEX changesets_issues_ids ON public.changesets_issues USING btree (changeset_id, issue_id);
 )   DROP INDEX public.changesets_issues_ids;
       public         redmine    false    255    255            J           1259    25340    changesets_repos_rev    INDEX     e   CREATE UNIQUE INDEX changesets_repos_rev ON public.changesets USING btree (repository_id, revision);
 (   DROP INDEX public.changesets_repos_rev;
       public         redmine    false    250    250            K           1259    25510    changesets_repos_scmid    INDEX     ]   CREATE INDEX changesets_repos_scmid ON public.changesets USING btree (repository_id, scmid);
 *   DROP INDEX public.changesets_repos_scmid;
       public         redmine    false    250    250                       1259    25669    custom_fields_roles_ids    INDEX     r   CREATE UNIQUE INDEX custom_fields_roles_ids ON public.custom_fields_roles USING btree (custom_field_id, role_id);
 +   DROP INDEX public.custom_fields_roles_ids;
       public         redmine    false    276    276            �           1259    25226    custom_values_customized    INDEX     l   CREATE INDEX custom_values_customized ON public.custom_values USING btree (customized_type, customized_id);
 ,   DROP INDEX public.custom_values_customized;
       public         redmine    false    196    196            �           1259    24683    documents_project_id    INDEX     P   CREATE INDEX documents_project_id ON public.documents USING btree (project_id);
 (   DROP INDEX public.documents_project_id;
       public         redmine    false    198            o           1259    25250    enabled_modules_project_id    INDEX     \   CREATE INDEX enabled_modules_project_id ON public.enabled_modules USING btree (project_id);
 .   DROP INDEX public.enabled_modules_project_id;
       public         redmine    false    265            �           1259    30399    entity_assignment_idx    INDEX     �   CREATE UNIQUE INDEX entity_assignment_idx ON public.easy_entity_assignments USING btree (entity_from_type, entity_from_id, entity_to_type, entity_to_id);
 )   DROP INDEX public.entity_assignment_idx;
       public         redmine    false    313    313    313    313            �           1259    30400    entity_assignment_idx_from    INDEX     h   CREATE INDEX entity_assignment_idx_from ON public.easy_entity_assignments USING btree (entity_from_id);
 .   DROP INDEX public.entity_assignment_idx_from;
       public         redmine    false    313            �           1259    30401    entity_assignment_idx_to    INDEX     d   CREATE INDEX entity_assignment_idx_to ON public.easy_entity_assignments USING btree (entity_to_id);
 ,   DROP INDEX public.entity_assignment_idx_to;
       public         redmine    false    313            {           1259    25414    groups_users_ids    INDEX     ]   CREATE UNIQUE INDEX groups_users_ids ON public.groups_users USING btree (group_id, user_id);
 $   DROP INDEX public.groups_users_ids;
       public         redmine    false    273    273            �           1259    27421 %   idx_dmsf_wfstepact_on_wfstepassign_id    INDEX     �   CREATE INDEX idx_dmsf_wfstepact_on_wfstepassign_id ON public.dmsf_workflow_step_actions USING btree (dmsf_workflow_step_assignment_id);
 9   DROP INDEX public.idx_dmsf_wfstepact_on_wfstepassign_id;
       public         redmine    false    303            �           1259    25457    index_attachments_on_author_id    INDEX     [   CREATE INDEX index_attachments_on_author_id ON public.attachments USING btree (author_id);
 2   DROP INDEX public.index_attachments_on_author_id;
       public         redmine    false    188            �           1259    25619 4   index_attachments_on_container_id_and_container_type    INDEX     �   CREATE INDEX index_attachments_on_container_id_and_container_type ON public.attachments USING btree (container_id, container_type);
 H   DROP INDEX public.index_attachments_on_container_id_and_container_type;
       public         redmine    false    188    188            �           1259    25472    index_attachments_on_created_on    INDEX     ]   CREATE INDEX index_attachments_on_created_on ON public.attachments USING btree (created_on);
 3   DROP INDEX public.index_attachments_on_created_on;
       public         redmine    false    188            �           1259    25797 "   index_attachments_on_disk_filename    INDEX     c   CREATE INDEX index_attachments_on_disk_filename ON public.attachments USING btree (disk_filename);
 6   DROP INDEX public.index_attachments_on_disk_filename;
       public         redmine    false    188            �           1259    25429 !   index_auth_sources_on_id_and_type    INDEX     ^   CREATE INDEX index_auth_sources_on_id_and_type ON public.auth_sources USING btree (id, type);
 5   DROP INDEX public.index_auth_sources_on_id_and_type;
       public         redmine    false    190    190            a           1259    25437    index_boards_on_last_message_id    INDEX     ]   CREATE INDEX index_boards_on_last_message_id ON public.boards USING btree (last_message_id);
 3   DROP INDEX public.index_boards_on_last_message_id;
       public         redmine    false    259            X           1259    25795 #   index_changesets_issues_on_issue_id    INDEX     e   CREATE INDEX index_changesets_issues_on_issue_id ON public.changesets_issues USING btree (issue_id);
 7   DROP INDEX public.index_changesets_issues_on_issue_id;
       public         redmine    false    255            L           1259    25467     index_changesets_on_committed_on    INDEX     _   CREATE INDEX index_changesets_on_committed_on ON public.changesets USING btree (committed_on);
 4   DROP INDEX public.index_changesets_on_committed_on;
       public         redmine    false    250            M           1259    25433 !   index_changesets_on_repository_id    INDEX     a   CREATE INDEX index_changesets_on_repository_id ON public.changesets USING btree (repository_id);
 5   DROP INDEX public.index_changesets_on_repository_id;
       public         redmine    false    250            N           1259    25432    index_changesets_on_user_id    INDEX     U   CREATE INDEX index_changesets_on_user_id ON public.changesets USING btree (user_id);
 /   DROP INDEX public.index_changesets_on_user_id;
       public         redmine    false    250            %           1259    25423    index_comments_on_author_id    INDEX     U   CREATE INDEX index_comments_on_author_id ON public.comments USING btree (author_id);
 /   DROP INDEX public.index_comments_on_author_id;
       public         redmine    false    232            &           1259    25422 1   index_comments_on_commented_id_and_commented_type    INDEX     ~   CREATE INDEX index_comments_on_commented_id_and_commented_type ON public.comments USING btree (commented_id, commented_type);
 E   DROP INDEX public.index_comments_on_commented_id_and_commented_type;
       public         redmine    false    232    232            �           1259    25448 "   index_custom_fields_on_id_and_type    INDEX     `   CREATE INDEX index_custom_fields_on_id_and_type ON public.custom_fields USING btree (id, type);
 6   DROP INDEX public.index_custom_fields_on_id_and_type;
       public         redmine    false    192    192            �           1259    25649 >   index_custom_fields_projects_on_custom_field_id_and_project_id    INDEX     �   CREATE UNIQUE INDEX index_custom_fields_projects_on_custom_field_id_and_project_id ON public.custom_fields_projects USING btree (custom_field_id, project_id);
 R   DROP INDEX public.index_custom_fields_projects_on_custom_field_id_and_project_id;
       public         redmine    false    193    193            �           1259    25648 >   index_custom_fields_trackers_on_custom_field_id_and_tracker_id    INDEX     �   CREATE UNIQUE INDEX index_custom_fields_trackers_on_custom_field_id_and_tracker_id ON public.custom_fields_trackers USING btree (custom_field_id, tracker_id);
 R   DROP INDEX public.index_custom_fields_trackers_on_custom_field_id_and_tracker_id;
       public         redmine    false    194    194            �           1259    25459 &   index_custom_values_on_custom_field_id    INDEX     k   CREATE INDEX index_custom_values_on_custom_field_id ON public.custom_values USING btree (custom_field_id);
 :   DROP INDEX public.index_custom_values_on_custom_field_id;
       public         redmine    false    196            �           1259    27424 )   index_dmsf_file_revisions_on_dmsf_file_id    INDEX     q   CREATE INDEX index_dmsf_file_revisions_on_dmsf_file_id ON public.dmsf_file_revisions USING btree (dmsf_file_id);
 =   DROP INDEX public.index_dmsf_file_revisions_on_dmsf_file_id;
       public         redmine    false    291            �           1259    27557    index_dmsf_files_on_project_id    INDEX     [   CREATE INDEX index_dmsf_files_on_project_id ON public.dmsf_files USING btree (project_id);
 2   DROP INDEX public.index_dmsf_files_on_project_id;
       public         redmine    false    289            �           1259    27547 /   index_dmsf_folder_permissions_on_dmsf_folder_id    INDEX     }   CREATE INDEX index_dmsf_folder_permissions_on_dmsf_folder_id ON public.dmsf_folder_permissions USING btree (dmsf_folder_id);
 C   DROP INDEX public.index_dmsf_folder_permissions_on_dmsf_folder_id;
       public         redmine    false    309            �           1259    27569 $   index_dmsf_folders_on_dmsf_folder_id    INDEX     g   CREATE INDEX index_dmsf_folders_on_dmsf_folder_id ON public.dmsf_folders USING btree (dmsf_folder_id);
 8   DROP INDEX public.index_dmsf_folders_on_dmsf_folder_id;
       public         redmine    false    287            �           1259    27423     index_dmsf_folders_on_project_id    INDEX     _   CREATE INDEX index_dmsf_folders_on_project_id ON public.dmsf_folders USING btree (project_id);
 4   DROP INDEX public.index_dmsf_folders_on_project_id;
       public         redmine    false    287            �           1259    27438    index_dmsf_links_on_project_id    INDEX     [   CREATE INDEX index_dmsf_links_on_project_id ON public.dmsf_links USING btree (project_id);
 2   DROP INDEX public.index_dmsf_links_on_project_id;
       public         redmine    false    305            �           1259    27425    index_dmsf_locks_on_entity_id    INDEX     Y   CREATE INDEX index_dmsf_locks_on_entity_id ON public.dmsf_locks USING btree (entity_id);
 1   DROP INDEX public.index_dmsf_locks_on_entity_id;
       public         redmine    false    293            �           1259    27529    index_dmsf_public_urls_on_token    INDEX     ]   CREATE INDEX index_dmsf_public_urls_on_token ON public.dmsf_public_urls USING btree (token);
 3   DROP INDEX public.index_dmsf_public_urls_on_token;
       public         redmine    false    307            �           1259    27400 -   index_dmsf_workflow_steps_on_dmsf_workflow_id    INDEX     y   CREATE INDEX index_dmsf_workflow_steps_on_dmsf_workflow_id ON public.dmsf_workflow_steps USING btree (dmsf_workflow_id);
 A   DROP INDEX public.index_dmsf_workflow_steps_on_dmsf_workflow_id;
       public         redmine    false    299            �           1259    27409 :   index_dmsf_wrkfl_step_assigns_on_wrkfl_step_id_and_frev_id    INDEX     �   CREATE UNIQUE INDEX index_dmsf_wrkfl_step_assigns_on_wrkfl_step_id_and_frev_id ON public.dmsf_workflow_step_assignments USING btree (dmsf_workflow_step_id, dmsf_file_revision_id);
 N   DROP INDEX public.index_dmsf_wrkfl_step_assigns_on_wrkfl_step_id_and_frev_id;
       public         redmine    false    301    301            �           1259    25430    index_documents_on_category_id    INDEX     [   CREATE INDEX index_documents_on_category_id ON public.documents USING btree (category_id);
 2   DROP INDEX public.index_documents_on_category_id;
       public         redmine    false    198            �           1259    25473    index_documents_on_created_on    INDEX     Y   CREATE INDEX index_documents_on_created_on ON public.documents USING btree (created_on);
 1   DROP INDEX public.index_documents_on_created_on;
       public         redmine    false    198            �           1259    30387 *   index_easy_settings_on_name_and_project_id    INDEX     w   CREATE UNIQUE INDEX index_easy_settings_on_name_and_project_id ON public.easy_settings USING btree (name, project_id);
 >   DROP INDEX public.index_easy_settings_on_name_and_project_id;
       public         redmine    false    311    311            �           1259    30386 !   index_easy_settings_on_project_id    INDEX     a   CREATE INDEX index_easy_settings_on_project_id ON public.easy_settings USING btree (project_id);
 5   DROP INDEX public.index_easy_settings_on_project_id;
       public         redmine    false    311            �           1259    25704     index_email_addresses_on_user_id    INDEX     _   CREATE INDEX index_email_addresses_on_user_id ON public.email_addresses USING btree (user_id);
 4   DROP INDEX public.index_email_addresses_on_user_id;
       public         redmine    false    278            �           1259    25424 !   index_enumerations_on_id_and_type    INDEX     ^   CREATE INDEX index_enumerations_on_id_and_type ON public.enumerations USING btree (id, type);
 5   DROP INDEX public.index_enumerations_on_id_and_type;
       public         redmine    false    200    200            �           1259    25402     index_enumerations_on_project_id    INDEX     _   CREATE INDEX index_enumerations_on_project_id ON public.enumerations USING btree (project_id);
 4   DROP INDEX public.index_enumerations_on_project_id;
       public         redmine    false    200            �           1259    25434 (   index_issue_categories_on_assigned_to_id    INDEX     o   CREATE INDEX index_issue_categories_on_assigned_to_id ON public.issue_categories USING btree (assigned_to_id);
 <   DROP INDEX public.index_issue_categories_on_assigned_to_id;
       public         redmine    false    202            Y           1259    25462 &   index_issue_relations_on_issue_from_id    INDEX     k   CREATE INDEX index_issue_relations_on_issue_from_id ON public.issue_relations USING btree (issue_from_id);
 :   DROP INDEX public.index_issue_relations_on_issue_from_id;
       public         redmine    false    257            Z           1259    25596 6   index_issue_relations_on_issue_from_id_and_issue_to_id    INDEX     �   CREATE UNIQUE INDEX index_issue_relations_on_issue_from_id_and_issue_to_id ON public.issue_relations USING btree (issue_from_id, issue_to_id);
 J   DROP INDEX public.index_issue_relations_on_issue_from_id_and_issue_to_id;
       public         redmine    false    257    257            [           1259    25463 $   index_issue_relations_on_issue_to_id    INDEX     g   CREATE INDEX index_issue_relations_on_issue_to_id ON public.issue_relations USING btree (issue_to_id);
 8   DROP INDEX public.index_issue_relations_on_issue_to_id;
       public         redmine    false    257            �           1259    25498 !   index_issue_statuses_on_is_closed    INDEX     a   CREATE INDEX index_issue_statuses_on_is_closed ON public.issue_statuses USING btree (is_closed);
 5   DROP INDEX public.index_issue_statuses_on_is_closed;
       public         redmine    false    204            �           1259    25790     index_issue_statuses_on_position    INDEX     a   CREATE INDEX index_issue_statuses_on_position ON public.issue_statuses USING btree ("position");
 4   DROP INDEX public.index_issue_statuses_on_position;
       public         redmine    false    204            �           1259    25441    index_issues_on_assigned_to_id    INDEX     [   CREATE INDEX index_issues_on_assigned_to_id ON public.issues USING btree (assigned_to_id);
 2   DROP INDEX public.index_issues_on_assigned_to_id;
       public         redmine    false    206            �           1259    25445    index_issues_on_author_id    INDEX     Q   CREATE INDEX index_issues_on_author_id ON public.issues USING btree (author_id);
 -   DROP INDEX public.index_issues_on_author_id;
       public         redmine    false    206            �           1259    25440    index_issues_on_category_id    INDEX     U   CREATE INDEX index_issues_on_category_id ON public.issues USING btree (category_id);
 /   DROP INDEX public.index_issues_on_category_id;
       public         redmine    false    206            �           1259    25470    index_issues_on_created_on    INDEX     S   CREATE INDEX index_issues_on_created_on ON public.issues USING btree (created_on);
 .   DROP INDEX public.index_issues_on_created_on;
       public         redmine    false    206            �           1259    25442     index_issues_on_fixed_version_id    INDEX     _   CREATE INDEX index_issues_on_fixed_version_id ON public.issues USING btree (fixed_version_id);
 4   DROP INDEX public.index_issues_on_fixed_version_id;
       public         redmine    false    206            �           1259    25796    index_issues_on_parent_id    INDEX     Q   CREATE INDEX index_issues_on_parent_id ON public.issues USING btree (parent_id);
 -   DROP INDEX public.index_issues_on_parent_id;
       public         redmine    false    206            �           1259    25444    index_issues_on_priority_id    INDEX     U   CREATE INDEX index_issues_on_priority_id ON public.issues USING btree (priority_id);
 /   DROP INDEX public.index_issues_on_priority_id;
       public         redmine    false    206            �           1259    25511 '   index_issues_on_root_id_and_lft_and_rgt    INDEX     g   CREATE INDEX index_issues_on_root_id_and_lft_and_rgt ON public.issues USING btree (root_id, lft, rgt);
 ;   DROP INDEX public.index_issues_on_root_id_and_lft_and_rgt;
       public         redmine    false    206    206    206            �           1259    25439    index_issues_on_status_id    INDEX     Q   CREATE INDEX index_issues_on_status_id ON public.issues USING btree (status_id);
 -   DROP INDEX public.index_issues_on_status_id;
       public         redmine    false    206            �           1259    25443    index_issues_on_tracker_id    INDEX     S   CREATE INDEX index_issues_on_tracker_id ON public.issues USING btree (tracker_id);
 .   DROP INDEX public.index_issues_on_tracker_id;
       public         redmine    false    206                       1259    25466    index_journals_on_created_on    INDEX     W   CREATE INDEX index_journals_on_created_on ON public.journals USING btree (created_on);
 0   DROP INDEX public.index_journals_on_created_on;
       public         redmine    false    226                       1259    25461     index_journals_on_journalized_id    INDEX     _   CREATE INDEX index_journals_on_journalized_id ON public.journals USING btree (journalized_id);
 4   DROP INDEX public.index_journals_on_journalized_id;
       public         redmine    false    226                       1259    25460    index_journals_on_user_id    INDEX     Q   CREATE INDEX index_journals_on_user_id ON public.journals USING btree (user_id);
 -   DROP INDEX public.index_journals_on_user_id;
       public         redmine    false    226            v           1259    25794 $   index_member_roles_on_inherited_from    INDEX     g   CREATE INDEX index_member_roles_on_inherited_from ON public.member_roles USING btree (inherited_from);
 8   DROP INDEX public.index_member_roles_on_inherited_from;
       public         redmine    false    272            w           1259    25435    index_member_roles_on_member_id    INDEX     ]   CREATE INDEX index_member_roles_on_member_id ON public.member_roles USING btree (member_id);
 3   DROP INDEX public.index_member_roles_on_member_id;
       public         redmine    false    272            x           1259    25436    index_member_roles_on_role_id    INDEX     Y   CREATE INDEX index_member_roles_on_role_id ON public.member_roles USING btree (role_id);
 1   DROP INDEX public.index_member_roles_on_role_id;
       public         redmine    false    272            �           1259    25447    index_members_on_project_id    INDEX     U   CREATE INDEX index_members_on_project_id ON public.members USING btree (project_id);
 /   DROP INDEX public.index_members_on_project_id;
       public         redmine    false    208            �           1259    25446    index_members_on_user_id    INDEX     O   CREATE INDEX index_members_on_user_id ON public.members USING btree (user_id);
 ,   DROP INDEX public.index_members_on_user_id;
       public         redmine    false    208            �           1259    25513 '   index_members_on_user_id_and_project_id    INDEX     q   CREATE UNIQUE INDEX index_members_on_user_id_and_project_id ON public.members USING btree (user_id, project_id);
 ;   DROP INDEX public.index_members_on_user_id_and_project_id;
       public         redmine    false    208    208            b           1259    25420    index_messages_on_author_id    INDEX     U   CREATE INDEX index_messages_on_author_id ON public.messages USING btree (author_id);
 /   DROP INDEX public.index_messages_on_author_id;
       public         redmine    false    261            c           1259    25469    index_messages_on_created_on    INDEX     W   CREATE INDEX index_messages_on_created_on ON public.messages USING btree (created_on);
 0   DROP INDEX public.index_messages_on_created_on;
       public         redmine    false    261            d           1259    25419    index_messages_on_last_reply_id    INDEX     ]   CREATE INDEX index_messages_on_last_reply_id ON public.messages USING btree (last_reply_id);
 3   DROP INDEX public.index_messages_on_last_reply_id;
       public         redmine    false    261            �           1259    25453    index_news_on_author_id    INDEX     M   CREATE INDEX index_news_on_author_id ON public.news USING btree (author_id);
 +   DROP INDEX public.index_news_on_author_id;
       public         redmine    false    210            �           1259    25471    index_news_on_created_on    INDEX     O   CREATE INDEX index_news_on_created_on ON public.news USING btree (created_on);
 ,   DROP INDEX public.index_news_on_created_on;
       public         redmine    false    210            �           1259    25494    index_projects_on_lft    INDEX     I   CREATE INDEX index_projects_on_lft ON public.projects USING btree (lft);
 )   DROP INDEX public.index_projects_on_lft;
       public         redmine    false    212            �           1259    25495    index_projects_on_rgt    INDEX     I   CREATE INDEX index_projects_on_rgt ON public.projects USING btree (rgt);
 )   DROP INDEX public.index_projects_on_rgt;
       public         redmine    false    212            '           1259    25449    index_queries_on_project_id    INDEX     U   CREATE INDEX index_queries_on_project_id ON public.queries USING btree (project_id);
 /   DROP INDEX public.index_queries_on_project_id;
       public         redmine    false    234            (           1259    25450    index_queries_on_user_id    INDEX     O   CREATE INDEX index_queries_on_user_id ON public.queries USING btree (user_id);
 ,   DROP INDEX public.index_queries_on_user_id;
       public         redmine    false    234            +           1259    25421     index_repositories_on_project_id    INDEX     _   CREATE INDEX index_repositories_on_project_id ON public.repositories USING btree (project_id);
 4   DROP INDEX public.index_repositories_on_project_id;
       public         redmine    false    236            �           1259    25726 8   index_roles_managed_roles_on_role_id_and_managed_role_id    INDEX     �   CREATE UNIQUE INDEX index_roles_managed_roles_on_role_id_and_managed_role_id ON public.roles_managed_roles USING btree (role_id, managed_role_id);
 L   DROP INDEX public.index_roles_managed_roles_on_role_id_and_managed_role_id;
       public         redmine    false    279    279            .           1259    25496    index_settings_on_name    INDEX     K   CREATE INDEX index_settings_on_name ON public.settings USING btree (name);
 *   DROP INDEX public.index_settings_on_name;
       public         redmine    false    238            A           1259    25451 !   index_time_entries_on_activity_id    INDEX     a   CREATE INDEX index_time_entries_on_activity_id ON public.time_entries USING btree (activity_id);
 5   DROP INDEX public.index_time_entries_on_activity_id;
       public         redmine    false    248            B           1259    25474     index_time_entries_on_created_on    INDEX     _   CREATE INDEX index_time_entries_on_created_on ON public.time_entries USING btree (created_on);
 4   DROP INDEX public.index_time_entries_on_created_on;
       public         redmine    false    248            C           1259    25452    index_time_entries_on_user_id    INDEX     Y   CREATE INDEX index_time_entries_on_user_id ON public.time_entries USING btree (user_id);
 1   DROP INDEX public.index_time_entries_on_user_id;
       public         redmine    false    248                       1259    25431    index_tokens_on_user_id    INDEX     M   CREATE INDEX index_tokens_on_user_id ON public.tokens USING btree (user_id);
 +   DROP INDEX public.index_tokens_on_user_id;
       public         redmine    false    216                        1259    25438 !   index_user_preferences_on_user_id    INDEX     a   CREATE INDEX index_user_preferences_on_user_id ON public.user_preferences USING btree (user_id);
 5   DROP INDEX public.index_user_preferences_on_user_id;
       public         redmine    false    230                       1259    25455    index_users_on_auth_source_id    INDEX     Y   CREATE INDEX index_users_on_auth_source_id ON public.users USING btree (auth_source_id);
 1   DROP INDEX public.index_users_on_auth_source_id;
       public         redmine    false    220                       1259    25454    index_users_on_id_and_type    INDEX     P   CREATE INDEX index_users_on_id_and_type ON public.users USING btree (id, type);
 .   DROP INDEX public.index_users_on_id_and_type;
       public         redmine    false    220    220            	           1259    25564    index_users_on_type    INDEX     E   CREATE INDEX index_users_on_type ON public.users USING btree (type);
 '   DROP INDEX public.index_users_on_type;
       public         redmine    false    220                       1259    25493    index_versions_on_sharing    INDEX     Q   CREATE INDEX index_versions_on_sharing ON public.versions USING btree (sharing);
 -   DROP INDEX public.index_versions_on_sharing;
       public         redmine    false    222            R           1259    25427    index_watchers_on_user_id    INDEX     Q   CREATE INDEX index_watchers_on_user_id ON public.watchers USING btree (user_id);
 -   DROP INDEX public.index_watchers_on_user_id;
       public         redmine    false    254            S           1259    25428 1   index_watchers_on_watchable_id_and_watchable_type    INDEX     ~   CREATE INDEX index_watchers_on_watchable_id_and_watchable_type ON public.watchers USING btree (watchable_id, watchable_type);
 E   DROP INDEX public.index_watchers_on_watchable_id_and_watchable_type;
       public         redmine    false    254    254            =           1259    25468 )   index_wiki_content_versions_on_updated_on    INDEX     q   CREATE INDEX index_wiki_content_versions_on_updated_on ON public.wiki_content_versions USING btree (updated_on);
 =   DROP INDEX public.index_wiki_content_versions_on_updated_on;
       public         redmine    false    246            9           1259    25458     index_wiki_contents_on_author_id    INDEX     _   CREATE INDEX index_wiki_contents_on_author_id ON public.wiki_contents USING btree (author_id);
 4   DROP INDEX public.index_wiki_contents_on_author_id;
       public         redmine    false    244            4           1259    25426    index_wiki_pages_on_parent_id    INDEX     Y   CREATE INDEX index_wiki_pages_on_parent_id ON public.wiki_pages USING btree (parent_id);
 1   DROP INDEX public.index_wiki_pages_on_parent_id;
       public         redmine    false    242            5           1259    25425    index_wiki_pages_on_wiki_id    INDEX     U   CREATE INDEX index_wiki_pages_on_wiki_id ON public.wiki_pages USING btree (wiki_id);
 /   DROP INDEX public.index_wiki_pages_on_wiki_id;
       public         redmine    false    242            i           1259    25464    index_wiki_redirects_on_wiki_id    INDEX     ]   CREATE INDEX index_wiki_redirects_on_wiki_id ON public.wiki_redirects USING btree (wiki_id);
 3   DROP INDEX public.index_wiki_redirects_on_wiki_id;
       public         redmine    false    263                       1259    25417     index_workflows_on_new_status_id    INDEX     _   CREATE INDEX index_workflows_on_new_status_id ON public.workflows USING btree (new_status_id);
 4   DROP INDEX public.index_workflows_on_new_status_id;
       public         redmine    false    224                       1259    25415     index_workflows_on_old_status_id    INDEX     _   CREATE INDEX index_workflows_on_old_status_id ON public.workflows USING btree (old_status_id);
 4   DROP INDEX public.index_workflows_on_old_status_id;
       public         redmine    false    224                       1259    25416    index_workflows_on_role_id    INDEX     S   CREATE INDEX index_workflows_on_role_id ON public.workflows USING btree (role_id);
 .   DROP INDEX public.index_workflows_on_role_id;
       public         redmine    false    224                       1259    25793    index_workflows_on_tracker_id    INDEX     Y   CREATE INDEX index_workflows_on_tracker_id ON public.workflows USING btree (tracker_id);
 1   DROP INDEX public.index_workflows_on_tracker_id;
       public         redmine    false    224            �           1259    24704    issue_categories_project_id    INDEX     ^   CREATE INDEX issue_categories_project_id ON public.issue_categories USING btree (project_id);
 /   DROP INDEX public.issue_categories_project_id;
       public         redmine    false    202            �           1259    24750    issues_project_id    INDEX     J   CREATE INDEX issues_project_id ON public.issues USING btree (project_id);
 %   DROP INDEX public.issues_project_id;
       public         redmine    false    206                       1259    24926    journal_details_journal_id    INDEX     \   CREATE INDEX journal_details_journal_id ON public.journal_details USING btree (journal_id);
 .   DROP INDEX public.journal_details_journal_id;
       public         redmine    false    228                       1259    24925    journals_journalized_id    INDEX     h   CREATE INDEX journals_journalized_id ON public.journals USING btree (journalized_id, journalized_type);
 +   DROP INDEX public.journals_journalized_id;
       public         redmine    false    226    226            e           1259    25203    messages_board_id    INDEX     J   CREATE INDEX messages_board_id ON public.messages USING btree (board_id);
 %   DROP INDEX public.messages_board_id;
       public         redmine    false    261            f           1259    25204    messages_parent_id    INDEX     L   CREATE INDEX messages_parent_id ON public.messages USING btree (parent_id);
 &   DROP INDEX public.messages_parent_id;
       public         redmine    false    261            �           1259    24776    news_project_id    INDEX     F   CREATE INDEX news_project_id ON public.news USING btree (project_id);
 #   DROP INDEX public.news_project_id;
       public         redmine    false    210            p           1259    25301    projects_trackers_project_id    INDEX     `   CREATE INDEX projects_trackers_project_id ON public.projects_trackers USING btree (project_id);
 0   DROP INDEX public.projects_trackers_project_id;
       public         redmine    false    266            q           1259    25388    projects_trackers_unique    INDEX     o   CREATE UNIQUE INDEX projects_trackers_unique ON public.projects_trackers USING btree (project_id, tracker_id);
 ,   DROP INDEX public.projects_trackers_unique;
       public         redmine    false    266    266            ~           1259    25655    queries_roles_ids    INDEX     _   CREATE UNIQUE INDEX queries_roles_ids ON public.queries_roles USING btree (query_id, role_id);
 %   DROP INDEX public.queries_roles_ids;
       public         redmine    false    275    275            D           1259    25108    time_entries_issue_id    INDEX     R   CREATE INDEX time_entries_issue_id ON public.time_entries USING btree (issue_id);
 )   DROP INDEX public.time_entries_issue_id;
       public         redmine    false    248            G           1259    25107    time_entries_project_id    INDEX     V   CREATE INDEX time_entries_project_id ON public.time_entries USING btree (project_id);
 +   DROP INDEX public.time_entries_project_id;
       public         redmine    false    248                       1259    25637    tokens_value    INDEX     G   CREATE UNIQUE INDEX tokens_value ON public.tokens USING btree (value);
     DROP INDEX public.tokens_value;
       public         redmine    false    216                       1259    24875    versions_project_id    INDEX     N   CREATE INDEX versions_project_id ON public.versions USING btree (project_id);
 '   DROP INDEX public.versions_project_id;
       public         redmine    false    222            V           1259    25387    watchers_user_id_type    INDEX     ]   CREATE INDEX watchers_user_id_type ON public.watchers USING btree (user_id, watchable_type);
 )   DROP INDEX public.watchers_user_id_type;
       public         redmine    false    254    254            @           1259    25090    wiki_content_versions_wcid    INDEX     g   CREATE INDEX wiki_content_versions_wcid ON public.wiki_content_versions USING btree (wiki_content_id);
 .   DROP INDEX public.wiki_content_versions_wcid;
       public         redmine    false    246            :           1259    25076    wiki_contents_page_id    INDEX     R   CREATE INDEX wiki_contents_page_id ON public.wiki_contents USING btree (page_id);
 )   DROP INDEX public.wiki_contents_page_id;
       public         redmine    false    244            8           1259    25063    wiki_pages_wiki_id_title    INDEX     Y   CREATE INDEX wiki_pages_wiki_id_title ON public.wiki_pages USING btree (wiki_id, title);
 ,   DROP INDEX public.wiki_pages_wiki_id_title;
       public         redmine    false    242    242            l           1259    25238    wiki_redirects_wiki_id_title    INDEX     a   CREATE INDEX wiki_redirects_wiki_id_title ON public.wiki_redirects USING btree (wiki_id, title);
 0   DROP INDEX public.wiki_redirects_wiki_id_title;
       public         redmine    false    263    263            3           1259    25054    wikis_project_id    INDEX     H   CREATE INDEX wikis_project_id ON public.wikis USING btree (project_id);
 $   DROP INDEX public.wikis_project_id;
       public         redmine    false    240                       1259    25322    wkfs_role_tracker_old_status    INDEX     p   CREATE INDEX wkfs_role_tracker_old_status ON public.workflows USING btree (role_id, tracker_id, old_status_id);
 0   DROP INDEX public.wkfs_role_tracker_old_status;
       public         redmine    false    224    224    224            ,   A   x�K�+�,���M�+�,(�O)M.����420��50�52V00�20�26�364530�#����� ��      .   �  x��W�n[7|��"�佐\�o�-H�n���p���V"K�$���w�cGRdm_Z@�G�Y�Ύi���W��F��	�������j�	bb����h2��Wz�n�v6R���B�s�.T�Ե�Q]
�'���&a� 5������)�)�g��r8Kqr�w�'�ǻ�/Z�c�a8X�H���Z��d�|�Y
3���}�P�hI�F_�:K�8fT|�\@�1FH~�Mx��v9_�D�)1�l�<@��Oc�����'���o�5��瘋ƈ�Z�ڱh�&M��G(����xı�. :����<^��b�������#7��x+�A�#��{/��h���%pqjBeӂkv٥uH��4�Qr�i��I�݅	�bb>�&n�w[����n���B��o׋��]^Ͷ��w�D���1
�=[����b��XB�йS��Zu�)p�b�g`ժ�����.j���#����Ӟ�L��f���u���u��k�I^��w����n�f��]A�\n���D\ЊaWP�G0ŗd��B��U�����3���fA�g�'o_�����p,.�L���{�����;����<�p���!�@��	�F=����6��4hm>�ʅM��9��pr�ClP!KwOB5�A<��P�ۢ���t;���~�����S�_9�`�69�T��\�!Z���oG��(bm��k�6����z���?�&%g�u��ټ�6|�ەn�7Z��׫�e��b��|��/FK�_���<$$��hޟ�|�\[goc)6-52x#�FU�%���PSH.B�F�{��`K3�I�-ݳ}�����n_�Λ.�K��Sr_9Fm����]Iť�6|]ű1�9\V�f��e��Il��j��z�eR����$42����=��V77��v�f����e�^�֟���j����v�W�i�^���������st*��8�&՗���	�qOr��{1�ps�BZ�A�c�GmйJP��4mكKu�5���ʗc����e+�ww��F�<�C�S����'�z�G\B뵘mZ��M��'Y��M$�Х$K-6<��Sʡ�lJ�>I�FP8��`z�0  �)$hv��"97D��E�o0�2�����&_�k�/�j{ʷ"�YF��4S6\=��K�wOX�+�	����1�f��5�n��d��Wzm�J$��ܵoc���9?�d��j�G��y,��O����Ef��:b+�l-��}�D@�aH�ϫ���r�1A�n!s�bٞ���Y�`�ӦW����� FQ�%���|����=F�)�]`�g���>cD8��w&�j��d;d�a�)Z��yH�Vm�3|w��B�m"��o1�%=�=A)J��S˜�`��hyY!)���3K��z�WӇ5���`�S�)�gh�����l�'��gogggg־��      0      x������ � �      u      x������ � �      n      x������ � �      �      x������ � �      l      x������ � �      q      x������ � �      Z      x������ � �      �      x������ � �      2   �   x��ұj�0���ɔ�m�:����PSh!��k�b�l�Sh߾g��,�T��@�r'%�k��k��h�;z�v�����@C�
��0(I$%����bi���}S�&�V�/x�}�YN�g�{��dTk��"�й��5E_2��t�Z��$��I�h[M��@����� b��Bu����Q�`>L2�\R-�5M�+%K���_4	;�4�#�Ʒ�8��7�&u�s�}/�c$)c���Œ��3���.�o���[      3      x�34�4����� sT      �      x������ � �      4      x�34�4�24�4�\1z\\\ ��      6   w  x��W�n�0=�_����p��A�^Z��\�ȩ;�9i>��%R�\\�0`əyo6��]=Lw_������v�~w��Cd�KPL�ۜN���jus<�ۇa�=��I��_����b��J��a;�e��x����Q�E1���6`�^������2�k�,��@��"9��a`�N����d�:e��\vZ�����Q�d�NE��̖�C��P�׽��!�]e�#�!˔G�B�'?�#z�%q�K%������Qt�5���4@q+�A�
���f�� ]J ���S* �(���%���g �dhF:���|�����Gྡ�I��cKA2��X�?L�ϳ�;X���g )������$D 7@��ܭ� �%�"��\'č=����V!�Z�K3wyJ�F~�(2�Ti�"C��>�V�9��מ1D�5�Ĳ�Ph�Z��GL��ёF^��^�^"�y�zBĦQߨG4�s���~!��x!B`�:�|x����$K�� ��Q�!�33 (��5�º&34)���.R�kJn�������7)�O�/2�ۼo��Q����FK��1p��ƤA�f%���T�҃Յ)��"�ed��M�D� &�C=�^�24m27/�%\�@�/sQ/��Q����gss:m֛��N��s�I��aYVo�z��!ծ�qµݗ��fdvg����[UE�0�jl�~)!���#/ZUuȃV��Ρ��B�C���ƛ���WV"GZaE�;n�!��wY�.���f�u�%t[�\��L��G��*4���8ɯ,nXݔ�n�q�Vi�Y9 ϖ*����w����6ʲ�}���u%�-xc�a�4����:'o��ߺ��9t!��xdd+^�`iI{t]����_u=(��uЖ�      �   �
  x�}�[��E��Q��\� c9���"��1��~�%	B?ڣ<�!�����q�������\4l�Hu�m����O�����Q{��D��f����ڮ�|F䏾Җ@�ڤ�%T׺�d9����A��֧VL��J���=x^4y2V��j/}��?�V��iq�F��A�Pյ��^$��O�������[@�7�_��S�h-B�c������,r��A���������"��j� ��k�;D\�V���Vw�8�!�ô�J���������Q�VoD|�xڞ����]�Hʍ~r9�5���gD�[�^
�Ѻs�"����<\�J��A$z���ć��~��3"�}���մM����fhO�CD�d�H�"VCƟ��|��/"����ܢ�'t�H�j��PFt���	I_�>d"�2�b�=!-�cߟ�ݕ��Xl��z9���,�Z��.��wN�8�g�Hu��A��V�!R[��P�������8Ȕ�d>�`��	i}���<��
�1����b��O�L�E�U� ҕߥ��7riV&�"]y^
�8������[(�����",.O�!�YQp���.����**#�!.� ����d�+z�����y�������"d��sQ�(@\��^v�l�|X^|�#2��d� ��G�ȳ��ݬ�"�i�R�ۏ���_B��K����ZR6��,6�%��"��� ������ӹkԟ�~B�2p�/Y�Do?�ք�u��3����8g�+!���#R1��D��[F��+�Z�����K�aő�����ө�y\��-�2�{�ѐ"�q����AjzJ�!��Iﰆ�uTn���r	8�cs=���QO�~F��'}Dti��V�Q��N";0lJ�.�#l���5Y�X!Ռ+�RNc���e�_�v��e��&T���#"v�E;�Q����ʴ�1�N�Dh���6jP�ha��nթ�M���F:�j�CT_��/2�_T�O�A/�[���NYz"����FP�
�@gD�w��D
�� ʩ��>������z�'�o�r��%��Q���>j@����Pԟ�e�o��l���\�}�p��(<�j��(ZN�g�,����gD�[�}���D�`�:�Q+��;��{���Lw��C�Y�{�q� j��Rh�4t��U�G�Q���ct�>�Q�O��D�7tw��`�#�D�_����@-S�m�e��s�m��ZZ�Q���#�H�'��v��� z�_�X"j��;y�w$y�P�_�]"�?��+A>Sd�� �|}�G���@:���.w꘠���6��^�-3rZ/}��������,�,24M����}�<\h���{�{B�Ke��R"���Q����쨍�t�]�Z�2!���_B��x���w��h�����6�\�ia��`�_	ka El�.m�`৩�
a��'P��=��N��'����_�7�����
��ۛ�'h��Հ���m�ӭ.V7�o�<I������zKq�ැgnq��{�v�������l����yqw4Ar�"T��
�V1�5o\k��~\�k��Q�z�*}�>���ʔBE{:�3���B	bܹ�sC��!�ɀ�n��qԿ��%����"�Y� ��'!�8��y�Y"���h���`���Z"E}[��EEu�`�O��Nk��{B�Oom��0]5��4�k�Q��!��R����Z�ѿCЗ�^���R��'�;��ⷔ�/%^�7��ޜo�Qa�(E�~]�?!�n5�o��OjW�~D�s��jz�R���wTٶA0��;�h.d;��-�E�*�F�N���Eծ�x�`��� ���x�A��6�%H������iy�:g�Q������d���n}�` 鸭��J�|�'u��e�&�����@�La�Zޜ�4��d�2�K���;�f�j5;ˆ[Yy�>Gڃ���x�/�^�?��%�J��-)�R:jWi��l�jǥ<�e4\��/8#����y�<.~���y�|%4%6�Ϥ?W����1P�z�Fi�mm�ǝ����j��(��l�FH�h���� b_FD�G�A0����R����Z�.������U0��p^�Vwtu^RY�٫��C0`��G*�sx }_=�`�W���G����'uQ�����_�.��j"/������ˈO}|�a�� ��6;��ȟ�wqw���{�p�&<&%@��}�j�D��\]�G6hƹ���~/��RRF�}������Z^�Iy�2��_pF0�SbyIq^���28!�ˌKf����Kp���4��c��?)�[Z7fG�����f�8�7�Y��$Td���L'���E.e���O�@����OH��M[���.�'�:���=����:��Җ��iu�`����d$� �C�K��hM''��x<�U4�;qc@��F��~	�۔_B�_����cL����z���q�S�T�_pS�|�`�o�h�K��'����廷�s�:!�8y݃!�)C2iY\�����Y�2^s����)���w���5�s�K-�>k�`����7�o��;��T͜c៣� P��+��n�k�!�.n9.����g�����6      �   �
  x��Z�n#G}��B�u�,�MoI�Y`��A.�"@�.,[�-�<��ׇ��mI�q2�����-��t�CyX-�����&Wq�c�m�*�CM�����(�4�����w�������Is�ğ��w0Aa���L�������c&����Ԣ�!j��8��bD@�	X���xe-�l*��{��JS=|^�%ެx��y^��:�˒��~d�!r�G���E|��N�1�1���ژ� �5:1��]4�
Vȅ-0e��JT%{[Sv\͔&������o��9_���w���5���n�׽ܓ?��?p�u6(<�K<��f��l�0<����e�
a�t���k���\�6�*_8e���<;�ާ�J�ȫ��������i~>�����2o��[�^�x+Ȏ�2��B�H�s򊗱� ���Е+#;
�]����`�E��)*I"[s�1ɮ��lb��vb��/�77��v��&��ˆ���Z��_mַ�m����z3��W�j�u� ��1��3�����^g/��"�Q+�n�tX�|5N����%���+g�$\T��Y�ld���:er>[�BIS7|�pY�^beТ�xL��O�����#��ų��/��:3f:�O�p�X��`i)���B9�(�K��Ȳĕ���-�K~*�S?Jc���ҽrRu����<�R>�j��G�'5�ov�x!p/y����1Y�?�GԨ��#j���3�f{׳׳��g?��I�/
�1�OW�{�A *��u�{�Q'G`���SEc�d)�\��ʙTb唌���H�0i���� ������,��3�\ u^n�K?���ɀNç�*.�+t�;o͘�켂�*�v�|��=�@ۇ���+�#f�KF�#. h�Ɠ�c�úh�\���<�D9c)Ր�&�W��Y�P�O�L`O��m��K
v
8��������~��x@)K{P�S������M���~��I�۬����Mo�Jy��4f:���br�E;�U�L6;Q �)�"`��!�V��A�*���V�Ѱ����~tٮ��C�Vܼu[t����L2Ϯ�ȼ%�}�����r��0�1J�Wv�tf4�PY|�t��T�E_I����J�\!q������U����))�O�Bha�u^z�5��B�F��D��Pja|��:���Nl�	��'H�V3�Rʺ�0x3f:7�(�I��_�/�u��I}Sd5����6�2h<��ͪt�]n��-��:���j�mo7���y����rx%㔬Y�#�V�۝�Eԧ��z�c�^0���+2��ei	Y&��I-1S|
q�����Sddb1JƣT��%�')M���-�$Kȅ1���Xa��%�؅��t�3��X݈��*��/� �к0d����|�I;���9��C�jA�s�
c�sšk�p��Y�߃!���!�Ǝ�έ�q�����jǛ�輼��Z��>�v��q�tv�"'&m���K�p����`!K�ĩ�~�@~��Y� �:ݟd����
2�;����F2�Vk=k/�8�[���"������2��q:D��*�J�M���:�5��'5�����k�&�>2<���A�Q���CB㎪���s-=?"����;�Bx{�p��;:g���;���{xjUx[M�y8�t;i����9LX`�R.v�'��k���[E���J@�&�d�>S��?�#��� �2��m��j��mw|-�L���:��V��Mk�  u���_�}{��o����~��~�~�p��0��&��EY84f�t@KD���%oZ�)gH5)h�Q�����l$�
8򢴬b_S��8�����e����pޭ7��C��2�X=�����Ot֓Eϣ�-�h[FK&����j�*����y�����\UT��V9�j]��B�V�����\�L��Nl�鹲s����~�ӷ�}3�nn�n��[J��8���߀׃��]%��=�G���n��z#	$鲭1m�W����+�#��!C�:7j:��X�nU<�5�"�PE%�C�%���E��f�6hrR`4���SmZ�����v۷���.��%U����Ș^X{~�:(�8���Ξ�=�IO��N;�ڏ��p"a#��.��kq:GVY+�,�S��O�2��Q2O	)���eiiSm'�������b�f�&n�q��by����:����'�����a��#�*���Ƈ��{�dv�d68�=r�>^�BAGR���SLF=EӨ��S1ښ�~qͩV��%�T*OPd�����\��.�LxuM9��_��ݭ�գ�(e)�lC��UK�<<�p�̣(Qf����ux�t�2$�QQ6k�6R�H����L?E&_�~ bM� H�"g���0GSIHA)XO�D <��]�O��gP��0�nz�t�UJ���x(Q�h��\�'BV���B�qEi�U���4W�H�
��y|�^ ��w�ށtʊ�����'�o׼'���v�����5J�NU6P\�wQ��h��"5�g/�����r�rնX�$�gJ����I��̿�pY�6\כ��O��N�f����ǫ�U�=�A7&z���\��#�^�a�D��N��2z�t؛I�Sҡ�Ĳ����R6��7�I�)$�rG��l�HLbf�J��J�a2�-�K��3i��?2Y)��	�`|��M׉,���������B��TRr�g21F��a��6٩��N���eNLRX�h/aÑ��0���%��/�[{h����L�Β7��L�[?w���w��k�      �     x���ݎ�6��ͧ����u��bt�.�	P�m�+K�D9i����,����H�g���՗�Ջ�S����v�]��rp�;
v͡�� d���rA"b�ϡoC
����|�����[�n����c����ޅ�|
վ|�ƶ.��s�w�>6efط�!4����r�ák���v�)vmI7]��������n��z���er���r���D���!�ї�B�V�`-��D�<=QrQr�#C��� iQ��p�Z ��x��w����]��}���޴�r� w��b��- jf8
%$�����taw7�:;aN8%�D ��x���q������e�1V���%�{��t;����@�׈Sl��J8\�����)�����oW
��H�)���
n��Xi���qC�ocJC
M�g?ߎ���  (�5��?��TzA"�����i��n�uhb�l�l?��[�*���8šT!C+�4f~����@O�J].�+�d�N���@Ǥ4�`L"�V���5���?~����~<}�1�(^�r�3O�� 
0�P�D��nB�C95`�14!�����^f�Lh!5,H�+~ۉ���DO���M���7W~����:P���n�+�.�Y"AYm$s��rէ|N܊*����܁�$�� �bRʟN�_CSp�r �Dp����\�=���Q������~�}ʩ�mrm���6�ZP��aBXm܂D �O����<���xꜲ+�dyR��F�.�p      �      x������ � �      �      x������ � �      �   �   x�u��
�0D�������&�9�=�R�T�j)��ӈ-���<��s���8�]�|���{�}n�S�&�d�H"���J��?
	�i�mK�+���L�Y�y��#��JU�`!�hE�N1���_�0��=���#q��%X�S"N��i�6s�ʲ|��;O      �   U   x�}���  �7L�0��!:����M:@s�P� �X�j+��XH�+�Te�F�פ"c�ȧP=����8�D�'D|��j      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      8   �   x�e��N�0�g�)�C-��/�GT���AL,n��T5Ne��x��n&D�3��\bI_����!���6�0h�ҮnTk��M5KI�����&��%S��Ȁ�O��5��U��(�CpĘO	+���{�ppE�*n����~A��`�_J�r�{�eۄS���,�5�k�^ξ�������?n�@֡N����P�l}�R�q�_8���;.[���w7N�*�����SJ�Wv      �      x������ � �      �      x������ � �      �   i   x�3�4�LL���sH�H�-�I��K-�A#CK]S]#cC+c+c#=s3CcSC<R\F���ɉ� B/9?�,K]Cs##+#s+SC=c33C<R\1z\\\ %#�      {   :   x�3��4�L�-N�22 �2��KS�K���3�ҹ��b%���BF@����L�=... U�      :   ,  x���Kn�0���S� �"^KZP���J�dc'�ؒ3.��p��XMV��&�w�}3�g"�P+�������áuH 8d���Y��bȬk��8T�&UJ��%��yɿ-k�ꄥ�rh*e
H{јM`gK�&���sX�+�%�c�ֆ�e�~��[@(�1?io�k ��G�?;i�6�=]G|��K�e�Ѝ[��7�Ћ$UY�K���'������%=�FZӍ�UӠ���߿J
��Fv����M�(�>"�C|���;i��7�m�W��fa@8�Qz���!�Z�9�����]�ΰ      �      x������ � �      �      x���ͮ�J�f��w<�y�e?��7�Pm��6���:�ʛG@E�r���D��L$����N,�
r�5��9m���~���������������������o��?���������������������?����������������������������o���������������_���������=����?�˿�������^?����^�w���s�h�R>Wʖ+]ϕ��+�ϕ�+��J��J��l��z��Z�d?�~z�U��Z��_���t
�Va=�ªYXO��j��/���t��a==êiXK�P=b��������-�G�	����t1��NO����������o�����+�����z^G�J����d<��:����G�����q�+����#^I}Q[G���&�3���_
M��.�e�6�f�>�g�F�h�N�i�V�j�^��k�f��l�n��m��n��m��n�|�=��fu[?��fu[?��fu[?��fu[?��fu[?��^�m��n{U��#��U�6��Wu�8��^�m�n{1B8��^�m�n{U��#��U�6��Wu�8����m�n{W��#��]�6��wu�<����m�n{W��#������n{W��#��]�6��wu�<�ێ�yD��m�n;��^Gt�Q��:�ێ���vT���趣��uD� �#���n{�mGu��n;��^Gt�Y��:������vV���足��}D���m�#���n{�mgu���n;��足��}D�]�m�#���n{�mWu�qD�]�m��vU�Gt�U�v�mWu�qD�]�m��vU�Gtۅ�uD���j��vk?�o���~���#��TǝGt\���;�h��S=w�s����<���Ou�yD׵�j��k?�w�}�o��V}w��w���:��Z��uFߵ�댾k�w�}ת�3��U�]g�]�����V}w��w���:��:��3������wg�~�������/�������Y}#8���9��5��G�"?g�_g����C���cgg�����сw�������3��3�g�hg$�lG��Ƞ����B�C;#�f;�vF͈��Y4#�fg��,w���L ��H��4;#�f���T�K�3riF0��H��4;#�f���t�O�3�iF@��H�5;#�f�^��LL��ȩA5;#�fD�쌬�V�3�jF\��ȫ�5;#�fD��̚Z�3RkFl��ȭ�5;#�f�^�Lx��H��5;#�f���a�32lF���H�16;#�f��$�e�3�lF���H�q6;#�fc/DvF&�fgdڌP���j3bmvF�����6#�fgdیp���n3�mvF����	7#�fgd܌����r���<�t�3�nF���Ⱥa7;#�f��쌼�x�3oF���ȼ�7;#�f���ܛ|�3�oF���Ⱦ���с����s�ovF��ɿ�8' gg$�����s"pvF����!8'gg������sbpvF�m/�~F&gg$�$���s�pvF����a8'gg��4���s�pvF�����8'gg$�D���s�S�с}oMqF&�gd�|o�vF&�����8'�gd�L����s2q~F&�����8'�gd�L����s2q~F&�cotF&�gd�L����s2q~F&�����8'�gd�L����s2q~F&�����8�۳����A��8�h;��M�����ަ�L�������ު�L��������ޮ�L������9�8?#�d���L����32q~�m2���d���L����32qN&����9�8?#�d���L����32qN&����9�8?#�d���L����32q~תּ���d���L����32qN&����9�8?#�d���L����32qN&����9�8?#�d���L����32q>�v�gt`2q~F&�����8'�gd�L����s2q~F&�����8'�gd�L����s2q~F&�����8'�gd�L����s2q~F&�����8'�gd�L����s2q~F&�����8'�gd�L����s2q~F&������ �gd�L����2q~F&.����� �gd�L����2q~F&.����� �gd�L����2q~F&.����� �gd�L����2q~F&.����� �gd�L����2q~F&.����� �gd�L����2qqF&.����� gd�L\���2qqF&.����� gd�L\���2qqF&.����� gd�L\���2qqF&.����� gd�L\���2qqF&.����� gd�L\���2qqF&.����� gd�L\���2qqF&.����� gd�L\���2qqF&.����� gd�L\���2qqF&.����� gd�L\���2qqF&.����� gd�L\���2qqF&.����� gd�L\���2qqF&.����� gd�L\���2qqF&.����� gd�L\���2qqF&.����� gd�L\���2qqF&.����� gd�L\���2qqF&.����� gd�L\���2qqF&.����� gd�L\���2qqF&.����� gd�L\���2qqF&.����� gd�L\���2qqF&.�����$gd�L\���K2qqF&.�����$gd�L\���K2qqF&.�����$gd�L\���K2qqF&.�����$gd�L\���K2qqF&.�����$gd�L\���K2qqF&.�����$gd�L\���K2qyF&.�����$�gd�L\���K2qyF&.�����$�gd�L\���K2qyF&.�����$�gd�L\���K2qyF&.�����$�gd�L\���K2qyF&.�����$�gd�L\���K2qyF&.�����$�gd�L\���K2qyF&.�����$�gd�L\���K2qyF&.�����$�gd�L\���K2qyF&.�����$�gd�L\���K2qyF&.�����$�gd�L\���K2qyF&.�����$�gd�L\���K2qyF&.�����$�gd�L\���K2qyF&.�����$�gd�L\���K2qyF&.�����$�gd�L\���K2qyF&.�����$�gd�L\���K2qyF&.�����$�gd�L\���K2qyF&.�����$�gd�L\���K2qyF&.�����$�gd�L\���K2qyF&.�����$�gd�L\���K2qyF&.������L\����������L\����������L\����������L\����������L\����������L\����������L\����������L\����������L\����������L\����������L\����������L�uF&�����������?~�����ۿ���?~������_��o��9ſ����_����������zN������'|~��UW�?���O��׺�+巗5�_����|~��������󏍟�Z��Z�\k5]˫X�.ƝaM���"���X�w�t�E�x��΋��ޤ���V����_������Ϻ������T��;�����˽�.��������< �n�[ygVT�Ժ_����u���Ge���ߞ�Yw�7�-W�-;5��n��7=�����iwU�Ƿ
��£��/>$6=��*��V�W4=���{��au��M��]�_4}X����o���n�m��ǿ[wO6}V����o_��{������M��/�n��n�l�������u�e��7��˦����jz|�j ׷����jz|�j WS� ��0�45�Q�jj ���� f5����j ׷����n�gݿw�|V����p�4 �1>��oNf����]�j��]<�b���fu��������f��n�U7���4_�FS7X�FS7Xuˌ�n���/��w��M���s4=�Wݿ���]������S`�4���M��6q�T��'    ͎�j&����?�LfO3���̞��Ou��i4w�T3�����O�������˳�ku�������gӝhu'���mu��O_�o>���[��Y����U��/��8�>X����`5�V��jz&z�"��C������޾u�O#�{�:?M�,�i�K���I�
d?�%�@������'����%�~�nC�$�i�ђ��a�Ť&3�~ͤ�V�դ&7��nR��to9�z���[oj���75	N���Ә�ނ�}�Ժաo�Ѝ;d=��s5nDo�s;��f�7b�y�}���(j�y����5qOz�=�e�tO�QY�Gu�Q�7=�1��IźQ���źq�,Z�����˚���چrӧ �/��Oxc�$�݈c�d�ݘc֤�ݨc��ݸc�M���,���5�g7��eS+�`���k3�w���a`�Y��v��Y��v�Y��v#�Y��vc�Y~k�j֤���jv55�7k2�n�7kR�n�7kr��R�&�8kv}�\�8g�$��Hg�d��Xgv5}W�[�&q�F\�&s��\�u����͚�g͚��i��o��܁�o�;V��-���w{͚��}͚����7ևfMڍ�fMڍ�f����7T�Bf��͂f��Ͳvz��p{�`��4��Q6>}F�M6?��@;�&�h�Y�x4�l
c���fO_J6{��'kr���}���2lG�{Z�@��&j`@��v�b0��]�/)��Ic�z����+� [=��j���e߼���eM��@ܲ&sk`nY��5|�Q��|�r��5q`���'Cr�%��䯁��M��@��o��@��o7"��|�D9P��Gh�*o�~�7�U�ʛ��{������U��W�*o�~�7�U�ʛ���_�M~��������,!�\�^bM�d//դ���rT�"6޵��z�^|��1{�F/�Ǟ��K�����j���^^��{}�&/l�������M^���/������^��$�c��y�6��<�|�����4�_��{䯯1�o�x/��ށ"��^���ZG��)��S9���r��T����T�hHS�_��>%wS�50��Ɍ�Q޴�@��&�j�Vy�Z5P��I��UޤV�*�Q��ޕc�x��� �˛�(`~}w\~}{�"`y��5��I�X�$`,oZ;l�V���bFy�50�\��ח��ū���Q��(�j�Vy�Z5P��I��U�M��UޤV�*oR�j����3n�7�6���i����M+��������?�Ӽg���j����&������\�^ң�}l��{D�ϯ��ԳP���D�j��&��7�~��{|��oz�l�q���h�qh�ޤN�B�YY�뻋��M���G��%�&��S%'���������jj>����#N|D_�f�Лt��{�����{���O>�Ѵ��G�&q�])�|ĉ�?M�C�i5��M��M��hZ�nb[F�m9�-�ɶ�ؖ�d[��;�5�l�h�-'�e4ٖ�2�lˉmM��Ķ�&�rb[F�m9�-��rxY2�-�7q��u�����:�ܻ6�ɸ���:N\�hr'�c4���1�\ǉ�=���/d��dDN��h2"�޴�Ɉ�{��oۖνmi��8�}K��޸�GU�\洕�����!��u�{�Ԧ����#�i���7Im� 'd4��7�(�g����	"f4��7q4�iK�y���>�`vF�Jy�3�Vʛ����N|�h�A'>h4��4�|ЉM>�������3�tΉ�ק��{s��;�2�<ˉgMK�MD�hZjobzF��91=�������dzNL�h2='�g4-�7QE�I���Ѥ�NT�h�v�F�k���E��A%h4I�	4�6��X��c�~�a������ �y�4�&�t�F�{:qO��=�����N��hrO'�i4���4>������i|[Aq�F���C�i	ƅ�MK0.��h��uakF������
MN��錦5�f4ٚ[3�lͅ�M���֌�}pf|�w�QF�F��(�i�ƅ�M6.�h2,�e4��2�V|\(�ѣh~m��#r~~M�V�I�\�٤{.t�l�=�g6��3�tυ��֯\ؚ�dk.l�l�5�f6ٚ[3�lͅ���)���-�G����P2�I�\(�٤d.��lR2Jf6)�%3��̅���6^�ٴ~�B�̦�+Ng69��3��΅ә߶8^(�٤d.��lR2Jf6)�%3��̅��M�O.d�l�-�e6ɖ�2�d˅l�M�O.l�l�5�f6ٚ[3�
��(�I�\h�٤Q.4�l�(e6i��2�4ʅF�M�B��O�Q>�̝��7.��l�~c6���1��ƅߘ=~���X��߾ 1f�ĸ��Ib\H��$1.$�l�c6-j�� �ǂ�Z渒�͕\����J.\�lr%�d6��W2�\Ʌ+�M���&Wr�Jf�+�p%�.e�"�I�\��$D.��l"Bd6	�!2�Z|s�Mf�⛟_ͧ��\ؙ�dg.��lZ�s�Yf�g��,�ǳ�\Kt�&sac�m�2e6ɔ�2�dJ����o;M?��ߴ�s9n�&���|�k�\���I�|.�]�i���m�Ǧ�-��qC6���帥�L��r��߶��T�lR��qO���_s?(�٤<>�����9����,|.��դ>���jr��qs5Ʌ��]�M@>���\��	�o�ms5)}�垗�k�4�������������.�ϥ��Rׯz2�\��U���K��BWϥ�s���R��h�T�<��=���R��R^��Oϵh=�"꾲���0��cDu�iQ-�zzFTϰ���4��kdu�iYm�z�FV�𞾑�7��o$�3z�FV�𞾑�7��od���Y}�{�FV��qU��qU��qU߈��qU߈��qU߈��q���o\�7��o\�7��o\�7��o\�7��o��7��o��7��o��7��o��7��o��7��o��7��o�L6z��]}#{��]}#{��]}#{�ƨ��=}cT�Ȟ�1�o\=}cT߸:��_O?o?��紣����dF5���Ɍj2WO��O{�̨&s�4�QM��i2�����df5�����j2wO���d�'�����7f����o��wOߘ�71/=}cV߸{�ƪ�q��U}��������������������������������������b��7���h��S}c�����1[���Tߘ-}�~�o̖�a?�7fK�x��׵Z��󁾮��7�u����4��VO�0T���a�7fO߰���oX����7�����V}c�����z��U�X=}êo���a�7VO�����o8�XO��-��4�-���t�R��i�����;PK���y ��OO�@/�����`j?=���~z����t����x��z�=d��=��mӴG5�������]��Զl�c�ڶM{tSۺi�oj�7�N��z�S�8���r��=��z�SC:��԰N�G;5�S��N��z�SC<���0O�G=5�S�qO��z�SC>��Ԯ�v�� ���������j=�a�Z��j(�����H���j=�a�Z��jh�������v��\O�D��PQ��E5\T�Q�zlT�F���Q��G5|TkR���Az$SC2��԰L�G3��3�=��zDSC4���0M�G55TS�qM��zdSC6��԰M�G75tS��M��z�SC8������t�S�qN��z�SC:��԰N�G;5�S��N��z�SC<���0O�G=5�S�qO��z�S[{取�ا֣�:������j=�#�Z����֣�:
��8���j=�#�Z���X�֣�:��x�n{���j=&�c�Z��ꨨ��:.��Ȩ��j=6�c�Z������:>�����j=F�c�Z������ � �� 8����^��Iu�T�qR'�{�T�I�'�qR��Iu�T�qR'�{�T�I�'�qR��I��k�t�T�qR'�{�T�I�'�qR��Iu�T�qR'�{�T�I�'�qR��I��j���{�'��eP{:�^��I��j���{%�'��R�=N��P{�Tߋ��8��WC�qR'�{�T�I�'�qR��I�k����ApR��Iu�T�    qR'�{�T�I�'�qR��Iu�T�qR'�{�T�I�'�qR��Iu�T�qR�ދ��t�T�qR'�{�T�I�'�qR��Iu�T�qR'�{�T�I�'�qR��Iu�T�qR'�{�T{?�����=N��z���8���:N��8����=N��z���8���:N��8����=N��z���so	��ApR��Iu�T�qR'�{�T�I�'�qR��Iu�T�qR'�{�T�I�'�qR��Iu�T�qR}�]�Z:H�z��8���N��8����=Nj�z��8���N��8����=Nj�z��8��㤆��z:N��8����=Nj�z��8���N��8����=Nj�z��8���N��8����=Nj��۲����ܲ���F��8���Nj�8���=Nj�F��8���Nj�8���=Nj�F��8���F��q{:Nj�8���=Nj�F��8���Nj�8���=Nj�F��8���Nj�8���=Nj��b����F��8���Nj�8���=Nj�F��8���Nj�8���=Nj�F��8���Nj�8���=Nj�F��8���Nj�8���=Nj�F��8���Nj�8���=Nj�F��8���Nj�8���=Nj�F��8���Nj�8���=Nj�F��8���Nj�8���=Nj�F��8���Nj�8���=Nj�F��8���Nj�8���=Nj�F��8���Nj�8���=Nj�F��8���Nj�8���=Nj�F��8���Nj�8���=Nj�F��8���Nj�8���=Nj�F���8���&Nj�8���=Nj�F���8���&Nj�8���=Nj�F���8���&Nj�8���=Nj�F���8���&Nj�8���=Nj�F���8���&Nj�8���=Nj�F���8���&Nj�8����=Nj�f���8���&Nj�8����=Nj�f���8���&Nj�8����=Nj�f���8���&Nj�8����=Nj�f���8���&Nj�8����=Nj�f���8���&Nj�8����=Nj�f���8���&Nj�8����=Nj�f���8���&Nj�8����=Nj�f���8���&Nj�8����=Nj�f���8���&Nj�8����=Nj�f���8���&Nj�8����=Nj�f���8���&Nj�8����=Nj�f���8���&Nj�8����=Nj�f���8���&Nj�8����=Nj�f���8���&Nj�8����=Nj�f���8���&Nj�8����=Nj�f���8���&Nj�8����=Nj�f���8���&Nj�8����=Nj�f���8���&Nj�8����=Nj�f���8���&Nj�8�Nj�8�Nj�8�Nj�8�Nj�8�Nj�8�Nj�8�Nj�8�Nj�8�Nj�8�Nj�8�Nj�8�Nj�8�Nj�8�Nj�8�Nj�8�Nj�8�Nj�8�Nj�8�Nj�8�Nj�8�Nj�8�Nj�8�N���^��_��n���������z~�w��:�/��������O��o����i=?�U��9��˞s-ѹ���?�����dU����?Y��&���
z�Ac�IT	�V���U��5��?Y��*8��MT�Y%�t���YU�*̪§O���tQe��ө�c��*��WQ��sՂ�u[?���t{Y�;�w�?���UoA�ނ�ނ�M��W�zB�\<D��Uo��j���W��!���z�Ct3_U:!*��n���w���k�m�w���.콻惘=�*��_�{{�U)����):Y�R�J�RJQ)�UJ)*�Q���RUJ)ji�����QE�t\;��:�D-m�TTģ����󱨈G�%*�QE|��xV_�"�U�O���C��:�Eu8�oQ3�Uķ��g�-*�YE|��xVߢ"�|G�"~�~^\U���W��-j���x��xUQ�*�!*�UE��z?����!��Uu8Du�����M3�������*�)����xj����"��"�������*����O����玸��'�r��U�SS��O��Ӯ}�����_��窆)��jX�j������|_i��K�Z��O�����ޓ���z�\���+���ޓ%zO�ޓ%�^o��g��d�~��̟=�$D�(3i���̴�Gt�2ն�Z����3���<ln��#�(��#*)F��#�)���#j1{�.����|]T�{�.���{�.��{�n��]��ЋF��ыf����?<��鞳?<�g���?]8އ �r��o��梢�Mm|� uᢺ`�n.���梺`�o.�H��P�}QT.ꑀ���`"�p�,D��0���M!E���Bt/ >LD>nȇ����0��a&�7�Þ.�gs臥��a'&�'7��RTΠ����T]T��ї�b"�r�_L�_n��� ���Z��j	D1E��(v��c"saLDan(��0�=�c�b�:@E���w_p��x�1�"v�>/�SL�Snx����P1Q�!*&B*7H�DL冩����:�ͥ{��:@Eޢ�����ܠ��6b�����7L8n ����ʭ��/AV����%���K�� ome���8k�ec���<�tޝjJ��\M�������{���h~>���h�>�[��|_,���w#t��'���&��	��ݽky���47�`�o�	�`�o����RTKTT��M4��mi�� �U$ ��+T��J��Da�L�oUT��MV͇���ꥻ��ŋ� A�A�W#^�E��yݗ:�����}J �i����`��Y�`��Y�`��Y��m)�:�|���|���|���|���|���|	��"�~@�D���Eblc_� �+���m�����y�;m �cgD�b줃�_�������	����p���q���^�_��_���k���Y� �Eb� �y����K(���"A�<?��;��G��~O������>�|��~Ҽw�)�@Z��+����OG�9��.�k��.�k��.J��.���.���.���.���.����v��u��x��x��0Q��)��?1w�P|0w�P|0w�P|0w�P|0�����w2�v�\{0�vQX`̝����]��P|0w�P|0w�P|0�������P�EC��P�EC��P܇��1QwQj`0�wQl`0�wQn`��g֔�"�(90A.�L������"�1�.����������Lȉ��Ʉ����LȉOѽ vqv�`a�	vqMr���s����_����������_����������������������9"�3!<."<���sn�oUH�"H3�4�D��q�Uc�@A��{�Q��^�B�ˁ4!
�LO���'�τ�ď���C!J�L�R�*0��ʄj��jM�V��քj��j������V��քj��jM�V��քj��jM�V��քj��jM�V<���0���{����	[
[��2U$l)DliBĖ&l)DliBĖ&l)DliBĖ&l)Dli�U�Dli�e�����������.�%*�2�ͽ��(�2��^"�4��^"�4��^�x��k{���L�(3�Z!��L�X��1��l̼�*u�{��l��(3a�!b��"�8a�!b��"�8a�!b����gnz5��!��g�{��ꢨ"!y!"y��`��(X3a�!b��"�8a�!b��"�8a�!
�L d� �@�@ι� �����	����17ɝ ��	�ђez"z9��!��z"z9��!��z"z9��!��s�%u5张��<|�f� ��c��*R�1Dq�C�1Dq�C�1Dq�C�1Dq�^�Y�ac�C�1D��-�`<Ϗ�����-�^��[*"*���!��*"*���!�}-�b����j���}�y.*g�b���)�).�b���ڻ����)�KF���)��*�"*    ���)��*�"*����<?�4+������-�^�����-�^�����-�^>Ϗ��ۂͥ(q� {)J�-�`����
��
��{R��e������K�[�����K�[�����K�[�����K�[����T0ETp]{�ѽ �����dz�R�`/E`o�R�`/E`o�R����G���0�y~�����G����2H��ֻg��;�M�Dln�]�Dln�m������ڵ��ᵵ���^K^[��^KQDo��R��l.�G�z��coc%�H�\��܂ͥ��-�\��܂ͥ��-�\��܂ͥ��-�\>���ι�k)�k��"���k)�kk�m�D�^K^[��l.Eln��R��l.Eln��R��l.Eln��R��l.ǻ�G��Exm��͠�������p�"5���lT���=g�"5l�9�as�٨����PT��������lT��=g�"�G�1���޽R�$��Q�D������~@d�	�=g�$�6�wש#�Պw��:B]i8�s6�Jù��QW������p.��s��s=g�[��w��:��EՕǯ�����>{>?���P�|=?KϹ��\�nDT?�_{ߣ��<�o�̀����$zW��9��=	{�%����E'�����J�@��dUF��*�w��:P���4�ɪ
5��9Y��&|�?�$�/�yNVE�t���U�J���dU�O���]���������\{��:P񴮽�U���?7�������#U�Ѱ
���y���W��_WC���;�����	cӪ�R�����d<.D��U�j$�i�Z�<'��а��dUMT�?wU��<'�J��d�益���-��@U�&�󜬪�i%{K��d}_Y|_�??R�U#���s2>S�>��U_���s��/���9Y՗&W�?��K3�NV���?'���L���U]?���}J��M���9YU�fl����X���s2>V��xTk��ɪ�5�~��UŚq�s��⧉��@U�f`���*Q3�NVU���<'�*֌���Uk&��ɪ�5���d|�U�*~{#����J�LꟓU%j��ɪ�5s��dUŚ1�s��b͔�9YU����f�u�*Q3gNV����<'�JԌ蟓1 ��Ӛ�޿4��y��_��~M$�_��~���_��~M��_��~���_�Ѝ׌��d��ѶT�?7��U���|Mb�Jo�Mj�Ƽm���ց��l���߿D�����~�Ml�@�����׼�.joj[�y��ޗ��;���Z��;#n�׾��h�]_�_��v}C��b�Mp����������$��m�$)ܘ��&I��{�ﶻu���f�57f릉R�1�7M�ټi�nL�M�p��yр�������^4��=���m��59�=�M�mO�E#~�#����-��#{N�t�Mu�&k���i����Ƹ�4������}����ID�1�7M"Q�inp�$"܀&�e0M"�Da"Fa0
A
R�&��0�k�Lѽ �0�:in ������D��`$&�$$��l�~J���&�b1c1�i�n���7��3@�DD� 2&B2�1�1�����e�鳫��P�"�b��*��2&3�1�1Ȍ�Ќ�-�\��VG����{��PW"JbPa��&�c1d1 ��(�AYL�Y�b"�bp���S`��:��?{e�&5�7x����LLDLbR�xw�:B]����=L>�Q�x?9���tT���&������u��w��������ۅ����;i���t��������mޝẉ���t�D�uv��U]��v��VֿLdγ�/���t�D�yg<_w�~�<Gl���{��h��L��K��V��D��3�7р��fu)w耉�{-T����:B]���LD�`"��WQM��c5�U�Qu��#��"6�	�	�MT7ޟ��z��Q�'�:�_��秜:�`�3�O*���.��;s~���9����[�+��.����/���9����Μ�Es~vh��9�3�w����ο\$�7�/�,�侓 "6�;
 b��D��������k��0�������k�O�����k��^����5\�5��"��pq�v�Ft/�5\�5�QO.��o��	�	�M��M8l��>{[�:BMj�kr�M�dOd�u��NU��QA����Ϝ�����3�SS�N7��+�	E��{ʰ��ͽ���������Eigf�33w��ܙ��hf���]43wf�.����Q>�s�y��ά�����;���N6�N���E3sgf�33w��ܙ��hf��̫��x�(soͽ����>w�tOQ���6D3sgf�33w��ܙ�W'��>}���hf���]43wf�Y�ǝy���ά�EYg������s�\7؄���pQ� `".b"q	���2OqO	x��xJ�S\�S�⚍><l� �`1.b1�qQ�"�8.�8�q�	8N=�l�m��q�	X��V@
8��8N�q��c{gX��XL�^CT�����$,&D��ㄈ��^}I�q���:<`@!ʧ�(D���=�(�p�q��[��[�*D�*b/#��V!�V�
�
�U��U��Bĭn"np�q��[��g[>u�z�l��n�~"~��Ƚڑ�&�G!�G?
?
�Q��Q��Bď~"~�����^�Kďb/�U�w���=��]"��]�z��w�P�ſDٖث��Q��D�(��_�lK��D�)`O!���*D���y�(��b��kى�8]�r1�Q.&��!��`��`�C�>">��z�l���P�"�0�z�l㹎��E5	��8]�25�Q�&��!��`��`�C�>�LM�C��"�s�T*���!���'��*��Y���%�.".p�qɀK��K\2D\2��!���������s�%��c{/rO�b=AvZ��P�">��������&|0D|0�!�	L�`��`�C���zК�	L�`��`��	��du�z1���(���������&|0D|0�!�y%l1Dl1}/p.�g߫�����"�����ń-��-���E��XOۛ�{�S�>�">��������&|�� ;=[G�r������%�/E�/a|)b|	�K�K_�_���	�S��8]��e	�KQ�,�)�	L���W���#Ԥ��%�/E�/a|)b|	�K�K_�_��R��Ɨ"Ɨ0�1���(#��������-�{>XO���A��E�/a|)b|	�K�K_�_��R��r�$b|���l�ޓ�s�$�t��q�|��罡�����QH��ro)��]�9������X[��R��֖"֖��e�N�"N�p�z
�UZ����JT�p�q��ӥ��%�.E�.�t)�t	�K�K8]�8]���)`�f�	kKkKX[�X[��R��r�]�D�kKkKX[�2�	�K�K8]�8]��R��N�"N�p�q��ӥ��%��� �������\{BM=_��z�����kKQ��ӥ��]p�q�N�"Nw���)��~��-E�킵���]�����VOw��l�o���,E�삗���]���.X[ur����eՍ����y��y]0�1��"�u��Rļ.�W����{���|�/�wNՕǯ�{a����׿���o��?~������o}~�������=�������_��_~���ÿ�[u�|���X�{=��x���.v?ˏ/l<�{}��������_u�p�<�R�᧋�s1E��t1��U����=�h���V���}�Z݁��ju��{0�N�P�O/��b	��t����ޗ�e5	!����1��/6g����]�����
���Y��C��κ�7ǔ�k����wn֝��Y7�|~f�7����x�}�I�_�����O/��	��t����X�Wպ�C~zi|��z�]u�����@�8?����$����F� �/W��F� �OW�;9>>��5$��O/��    �����[C�K?�4�^)���V���~�Z�[��juoIb�_�6�ޒ��OW�{K��?]������j�
��O�Qw��$zi�
$���ժHV��t5f5]�`T+�P�OW�V ��_�6�H(���U+�>>fg�����՝,�쟮V�@��t�j���j�
$���U+	�d��u�Ϻӷ?���U�|yi�nT�>��ju�K�OW�;]�|�Z��=����N_�@��u�J�O/�nTI�����N�����izf�?�
$�+�F��O5����/PC�~��V$ɧ�@��a��j�V$I�OW��4�M��j+������� qS>�;�+�/���Ef��o���ꞔ�-_��V���}�t���ַ/޷��"��ziu��o��۪�חZ�^#uG��w��!�j>�iꎐX7_��u�H��OWÆ�����m3�|�mlI��ӿAE�t9|IV���~>>^��L����� I��N�$*��r�I&�����$l��r��]���*J]Ma;J]�ҽ%�.K�ޖ�d5�O���t�N���L�{�N�m�uoS�>~���G��F"���.�-)Y����6��B&Y�@����$+&_w�d�Oo#w�de�O�����n�*�����r�.'��ɲ.)�F�2�����˺����r�'�/��s��r��.��� �.��F!�.���!�.��F"���}�:$q|zq��.k��Z3�2 �.GS��>7�{G�Z~�u	r7��ur7��u)r7��u9r7����ց�f]�ۍ�f�U>]������xr�%�݈r�e��c'�>� L7��%3nL5�R�nT5�r�n\5�,	��r��]�ۍ�f]�ۍ�f=����3Z�uyq7^�u�q7b���|\4�,f�{�pެKz��ެ�z���l|d�Xk֥��hk���xk�Q\��l|Dx�g6>��c6>>��~u�-�F��j�W�巩������u�J[ɺt����wQTY)�$k�|�'P��Ey>]���2�f�}R��߶�eoz�)����^
��߂W�(ЧǍۣN}��L��ǧ�w�dU�O�����o�-���u\�˺��{���;��M_��^C�Ca�5��$���]�@��@۫q}�mV����	r�`���xV��Y<+����wyV#��/]O\<+����wyV�ʻ<��g�]�����.�j�YyגRQ�{֔���U$��|�]�K{)�.Ul��Quu��U�kV����Xdu9�J�6�jY]~���mu�ac/��凍�dW�6�ü��a�����*]�׸�wGI?̻���������KH`�#�}���y|�cz�dK�/1T1Ϗ#BL/O�
���?�B�����)���{�Je�r�UyJo9�)�/3�����2���wRCʻ�(VޥX+�R���w)V�ʻ��b�=���s�r��4`ryעe̯�@�˯�[D,��"�w�X˻D����]+�+��yCʻ��!咵�>]n�z�� d�YB�@��.!k dy��5����5��K�Y�%d�,�l���r��]k��0�Z�l��y�jdc��L�6�Y��.GS�Zjl��y�Zcu9�J�����O�=�����(:TϢd�_}���X��e	N,A�����v����� ���!#z��8��g���0Σw9��ѻ���،�q�����=2��7�6$����_�6��<N�G_�feѻ���{����{��OC(�ѵ���y�.�q�0��ǉ�?]�M�&�k弉u]+�M���Z9obtF��91:�����et��[�uu���2:'Fgt��3��Ή�]F���.�sbtF��91:���{!3>.�7�)�˧�����S��v������SN|���)'>et���2�|ʉO=>��olX��e]N���.��)�˺�{�ԏ{�νWj�49��R�Z��-�G��^�4��E��ޖ�˧�{_֮E��ޘ�kQ��wf�R-'�et��7q5�gU�Ϸ�gt��7�@�k��y����>�`�Fת|�4�V�8���N���rN'�it9��4��Ӊs]���9���j�e4��щ2׷<�{���{�3�\Ή�]��Md��Z�ob�F�M:�I��&�ؤ�e�Nl��I'6it-�7�Q�KG��ѥ�Nt����v�F���\S5MD��M'�itmx;1U��T��%�5�|։�]>��g�.�u�F��:�Y��g�����.|���J��j_H��q-ǅs]���9��� 6it-��D�k�څ=���?ty�O4�V�\��e�.��2@ht�4���]���q#߅�]j�B͌��&ngt-4��6���\X��em.���Z�r�}F�����#�F���EU��.�t��f�B�PH�K!](�٥�.��RH
i~\wsa�f��0@�� ]��e�.��2@hڷ��B����s_C��.�s�yf���<�K�\h�٥y.4���<�g~� yaif׺��3���\x���.<���D�h~ܢy�yf���<�K�\h�٥y.4���<�gv-��8�K�\��%p.��8gv-��0@�� ]��e�.Ќo*�B��.5s�ff���P3�K�\��٥f.���R3jfv��53?-�Y��=��L.���r&�dv9�g2��Ʌ3�=��篓����# Ff��#�K�\���%F.���#bdv-ƹ0+�Ǭ�\����ѿ\����_.����/�ev���2��˅�]��¿�.�r�_f���/�V.˽�,�K�\H��%Y.$��,�evI��2�Z4t�bfϢ��_m���\��e|.���Z�t�rf�˹p9����^P��.�sa|��M��fv	�a3����A��o�j?�͝ߴ~�s9��&��w~���\�;���|.ǝ�ic��mnɦ�8��qK6I��帧�$��r��߶��lr$��qO�8��ϋ��o2)��q�7���\�;}"��os�~[���mn�&�7n��\��I�|.Ǎ�$B�"d6��\�;}
V<�]����>��_�{A������n�������[纟s��s�<���w[��y>?������Uu(�v����k�eϙ������W��u2�LE{��U%H\�:Y�����U=�}�x]��$&V���D�Y�ɪ�%��='˪a�dU'�"~��ǻT%J$�:YU������u��"���jz:�ޥ�TEx��hׁ���w흮�@��~�{Qׁzg$.�s�������u����NV��4���;�n[]ꝑ�u2����w�iC{��:P�DĨ�UEH4�:YU�D�xNvW5I�:YU�ӆ�F�u���i%{��:P!YߩNV������v���X u2>A(>t�ɪ�$�NV�$��u��&ɊE��FU���ɪ�$P�NV�(A�u�����=��@U�Y�ɪ�%@�NVU,���NƇhQ��b	E��UK�s�YU,!�u���z��qaV%Jl��*Q�W�dUŒ�k�dU�lZ'�*�@�:YU�y���)��YU�<*��O��*Q�
�dU��W'�*� �:YU���ɪ�%�NVU��{�����J�`�:YU�d��:YU��i���h���TK6ī�UKpU���X��dU��T'�*���:YU�dɏ:Y���)���SU��{���J���:YU�Ӳ=�33�k����U�Q'���@�:YU��v=�}���J�k��ջ��N�x�^����<�=P����<��@�3�Q{���� �9�׻*����>��b?G�����"�=��R����1�����l0%)�:̟*���en����l�0%9�:CLI��9�x����l�1%!�:5)��;���ǋ�򢉼퉼$z]g�^��m��E}�}�g��d�Sy����4ܣ�����8d�n��1\7I@�ζ9�?��}"2^7IƷ�FmH"�u6jC�Э�Q� n���1�)�$>[g�&ED� &B�0I���F=���]\����I���q/�x��CLD b""b!�T'�|?��DL�۬�Q�"�b��6�lԳ��ؽI���2&"2�1�1������dLe(SO��K&d�Dh�@+&	�٨g�1���ȌAfL�fll-�����w}�J=<�o�@Q����    ��$9�:5)�,d1e1(��0��YL�Y�b"�b��z
x�S��}���w�0ID�9��D�� &&"&1�n��~�{��{��D�� Ս�z�iB>����~�_TW���6������igm)��{��C�;�u�G�;?�ɯ�=����0~��+�tݙ��h��׫K�5�W��J����w�$)�l�G��3��;ݯ]�n��܃�p�D�}g�_]����RW��@������I�[��Q��A���,�p�����Dp��&�]0^p�:��&}����*ר����ɾ-{�p؄�؄�&���E����z����~��AuE���s~���9����Μ�Es~�-;��s~���9����Μ�Es~g��9�3�wќߙ���w���}�/�d��:���M��El��Ԁ�^ر��9q߁��vq�Q	��q�A�p������5\�5��"��׎Ј�������5�����M6�"6�	�	�M������&|�%�N�٨�
l��9�B=|�ߘ`��}�����W7��~�wF��\7��u�۫+�x?�13�����\���Esog�����3�vQ����hf���]43wf�.��;3s�̝���f�>vpO�d��@�3�������ü�����S13w��ܙ��hf���]43wf�.��;3��l>�.���Esog�����ϝ&�>��df�.��;3s�̝���f��̼:�������E3sgf�33w�B\u6jR5pf�.�8s~�|�x����ࢸA�&\�7���q	����H�D\�9x��xJ�S\�S��"���l�����Z�{�"�E-��"�pq���ԓk߫u�z���Ÿd��:�,�8ǩ���,�X��XL�^�BT��W��$,&D��ㄈ��^kI�q����l,�!ʧ�(D���=�(�p�q��[��[�*D�*b/#��V!�V�
�
�U��U��Bĭn"np�q��[��gj�#�se[f�G�I?
�Q��Q�^�HT������(D�(�G!�G?
?
�Q��Q��Bďb��%�G��ꪵ����^�KĀ�]�KT�{�.��ԗ(�{�/?��ؗ��^�K�m��ܗ�=�)D���[�(0��b^�\L�{�:ѽ �Q.&`|!��|0D|0��!��`��`�C�>XO�=�#Գ�����>{
VG�R����Ӆ���.D����(S�������|0D|0��!�Q�&`�!b�[[���%�p�qɀK�dϯ��,b�[�f���d��d�%C�%.".p�qɀK��K���j�9������Pϕ{Z?�jR�>">��������&|0D|0�!�	L�`��`�^�Y�y#�!�	L�`=A6#�#Գ��%�/DY����&|0D|0�!�	Q�+a�!b��{9sQ=�^]Tϰ��ń-��-&l1El1�N."����z�l�_GXM]�>�">��������&|�� �6��؋��jƗ"Ɨ0�1����%�/E�/a|)b|	�'ȶU�#p���Ɨ�|Y�S�>�">��w���ԩ#Ԥ��%�/E�/a|)b|	�K�K_�_��R��Ɨ"Ɨ0�1���(#��������-�{>XO�����Y��Ɨ"Ɨ0�1����%�/E�/�~@"Ɨ{3�ʦ�M
r�$�t��q�|��罡�����QH��ro)��<�#{_ k˽1���%�-E�-am)bm	kKQ/�t)�t	�����o����JT�p�q��ӥ��%�.E�.�t)�t	�K�K8]�8]���)��:BM�X[��R��֖"֖s��&�gX[�X[��R�L8]�8]��R��N�"N�p�q��ӥ��%�.E�.�t��y�:B=�X[�����/X[=Av^��P����K���t)�t�.E����S`gE�u%bm�-E�킵���]��z
�d�s�����>y��R��.xY�x�/KQ�U'ߩ�:B]�x�9u��1��"�u��Rļ.�W����J�|o��ތS�;�������eo�~��gSK�����y^~?��7�T?����M!�����~sD��z~�+���e͹�W�2͹����73x3E="�D��*#������Ձ*$����P�k��B%�����+���η�qu�*Q�j��J|z�N����������\;9W�"<��[������>P������zgD����?�pu���"�p]�Έ��u�;�t�x����zgD��x\����էŻ[�uջ*b�U!B�U�$"�]�$�]���.ց����ĻY�uWE��9�]����x�׾������!�HzW5�&��]�$�_wU�(Ws��&Ѩ�UM�I�5�E��kT_�&W�@U�hT��bѤ�UŢE���hQ��bш�UŢ	�5��E�kV_��%ׁ�Dш��U��	�5��EA�kV�f�׬*���YU,��_�*�U�ϣ"����U�(�_�*Q4��VU�h0��b�\�ZUŢ������v���תJ֯U�(ʿ\�*Q4���M?���E���?UŢY��SU,��?UŢI��SU,��?UŢ���Sw����Oԁ���G�{�*Q4���k���mL��\P�TE�&�UE��UE<mw�VQ�]�Nn�w���q�g�wf� E�wf]�u���(��zgD����]�ogh���.���|��+V�Ɨ���$�D��9��27�p{�I������M����$�D��9��27�xe(nf�&�P�{/���8^T�{/��{ /���{"/�_�{�/���{�/��{��4��sdO����h�5Y��w��ٺ���7�u� �ܜ��5k��!�A���M����(qCL���A
&b
7L�D9� a""qC$L�$n���r7<�D@�6����v�CL�Cnx����1�!"&B"7HĊ��{s�0� n�����Q1Q��ǘ����&�zȘ���!�$c"&s�dLen���zWv�#Գ�ܠe(n������1��!3&B3��Z����WG���)�~�7��D�䆒���`� n�� �d1e��,&�,7��D�决��܀��wm�:�}��]߰�P��,�/10�&&"&7�Ğn�Wc�#ԕ�{�p���aE>���&�Þ��W{�#�����os0��bc�R�xW]�Kh��?WN�#���zW?�#���ϻ�a�o%����&���V�����`Fn"O~0`7�'?�Λh<?�[��߽��msPs��&�����t�xwH��M���&��&ZWj@L��Ђ[xwo���\0]���[gդoV�Ye�5��{��:�+b6�"61`^l��W�L�s�+��lڧ�ƻK���`�P�0�wќ0�wќ0�wќĖ�E��9�����9�����9�����9�����9�����9����#p��?�.Z�il�_�&��Elb��ѽ�c"�1v�@�5�,����i�;*!�cg%D\c젅�k����ƀk��k����Ƹv�Ft/�5\�5\��z��#Գ�M؄��ĀMx��vy�&Ek4؄W`c�����<���u�ڨ��x?�3��������H建w���t�x�4����l��i>�{�h�=�{�h�=�{�(�0���hf>���hf>���hf>���hf>���hf>����]h����N��S?����W��G�I��|03w��|03w��|03w��|03���Ż��`�����`������;Mz����jC43��]43��]43�̽f�����f惙��f惙������]5��]�5��]6kǣ5�<�.�L؄�����������L����Ȅ��(s0�).�)��"�2�).�)��=>���D�,�E,f�b\��pq�	�qǙp�Z)?�ݍ',�E,f�b\��ф㸈�L8N���>��"3}/~!�I߫_�j��Ʉㄈ�̽֒��L8N���0��S&�(D��	{
Q>e­Bĭ&�*D�j­Bĭf�`D��*D�j�    �Bĭ&�*D�j­Bĭ&�*D�j­Bĭ&ܪ�P�w�	{�E���mvBď&�(D�h�^�HT���	?
?���	?
?���	?
?���W�񣹗ꪵ�ޝ��^�KĀ�X����j]"4�R_�l��k}���܋}���ܫ}��-s/�%bO��\̄[�(3a^!��LxY�r1��+׉�8]�r1��\̄��N�`������N�`������N�`Y����=a|!b|�WFy�{%ϱ�b�$�.D�n��B���0�ej&|0D|p�C�'|0D|p�C�'|0D��	[[�����9����{.".9�e�ƻC��-��-N�b��0�p�q�	���p�q�	���p�q�	���k/�����,�#�]�l�l�x�#^����\����\����\����\�W�|�X����\�����ݱz��B���/DY�\����\���l1Dlq�^�\TϾ�B�3l1Elq�S�l1Elq�\Dlq���e��>�">���)�>�">���)�>��$�=�W���E5	�K�[0�1��K�[0�1��K�[0��5��ݴ�.E���KQ�l�S�|0E|p�ޭ�ߧ]�&E�o��R���/E�o��R���/E�o��R���/E�o��R���/E�L\�޺Et/����yw!_0�1��K�[0�1��K�[0�1���1��7�lڻ��;�8���	�8�z��罡��ӭ����ӭ�����w������7��kKk[����kKQo��R���.��@��B��7��$�.E�n��R���.E�n��R���.E�n��R���.��@�{s/X[�Xۂ����-X[�Xۚ{�6Q=��R���-E��K�[p�q��K�[p�q��K�[p�q����	��^�֖"ֶ�ކPR�����y��a�&59��lԤ��=g�&5��95��t�٨"Y{�Fi��s6*PC֞�Q�s������,%]�9��c�٨@{�Fj�s�٨������ٞ޻g�u�����lԕ�p=g��4��9u�!\��&jRC����-V��ud�*�+���]TV�u2��u����s��N��T��: *)�u�������E���D5���9�S �r�]N�z
��t{~PPH�����@��l��=g����;�	s=���H$z�FIڟة���i@�s6�Jz���>%� ����H7$�#ԕ�<g��4��9u�����u��5�sQWO7�X��P�򜍺� ��lԕ�<g��4��9�~xHr�%�+M��9u�.�٨+py�FM>u����M]i��s6jRM�����w�H�ǖ��ԕ�<g��2^x\G�'+Q]�ԕ|<g��4��9u�	(=ߝ�I4y�FMj��s6j��y�~�&5��95y����u���٨ɂ�~�������:Bm<]q�:���t�џ#��H���]b�:����N���Κ��s6��5����L��O�ٰ���z���u���f0��m7݃��H3���j���x5���lԆf0�����Ɵ�QW�_���PW���s6jR3�~�F=kB(�٨g�`�9��W��K�6�uDS��v��Oɣ�PW��vmc]g�W��#��tԍ}��x:�V7���ӝ�~QGx�D�����v�BF��	Vdm�\g�G�{4�>��h�[[3��4�~m�\g�W(�#���pD���u6ͽ_�+��Dueԕht\�:��2I����e�:��u�gJm�̼���k���i�i{�k��Aڞ�&l�����I���i�2K�Sd�d���4a��=�6MZ!mϡ�/o9�������q����y�&����E�h{�Ѣq���h�<��y�&�����޾U���;����Ik�i{�m������sGveٱ���`ʈO���J	eVJ�_�p��e0�R
�s7A$��0m%ږEJD�M�m���4�p�F ~,���թ�_H#7Q&nd⦙`H#P7Q�n$�&�ԍH�>Y�%�S��((�NRu��F�n�\���M���iF�H�M����ry#�7�4C���R}#տ���h�>���bԢ�$��q�^��D���(c72���q��%�D��I�m����������%ޭx���K�-Z7�F�-�7��JG�!���,q��%�零�z���vΧ���Χ�Ջ�Χ�ߦ�/Q%�s�҈�E�������i@Q�e����A��>� �\�h��O�4�5�A��>�`?Pa"Ra�����K����+��z�n�fB�^*!KSa506p��m���KT�8��DZ�CL��;��4X��;L��;��D�ġ%&2��b"��V�����-&�Rc"T���՘��t@��H�CzL�z�c"�㰞�������l�������ъ*���U6��E��kZ@ę��"��p&q&�3��39��>��⼗�xE��aE.bE+r��V�p&q&���E�g��b�%��{80�z/Qa"���������8��E�+r+rX��X��새y?�X���~𚋾���;������a1�!������x�@|M�q��R�1�������8��6�d{:��6ZF��%���۾�jX%��5�x�������Ļ�5-F��%��_�b�^�v-�]�ppx��x��n��]eN��L���]��;<�5�ϚKʚ��x�E���\��p�#��%x���{�%��%8,�5g��!\��;�6f��%*L�0��"��F�D�	�p͉E���Y�;��6wuk������?\�?�q�4G0�KT�W��,�a�F�a�UrN=K`	.b	�k�Q�N�\���"����%��%8,�E,�a	.b	KpKpX���ᢹ�a�h��Ok>�p����
؉��I�N\�Nv�"v����������E�$`'.b';q;	�yzѧv�"v����������>В�=������>��%(0���>X8��ø�g���Ȇ�,�Ku�¯NJ��8<�k�wt/q��ߙE\����qhѽ�9�l=��E��3��E��3��E�3��E�3��E+�3��E;�3����ry�V�2}�Z�x�mZ��z3��o��j`��������!��\>D�|��߆7~�^��E+�3�N�U��m����ryѦ�ryѪ�ryѮ�r���ǯM������u:�h�"�x"��DQ���0��E��W���0��7�aD�o$�È:�H4�u:���D�$�.wm�πوV�g�lD��3`6������}*�=�������x�h�|�H�A>V$Z!�+��Q��G�>����"��X�}(�:�8VI'�Nx�h�|�G�J>�#�%�+-�π���g��D��3�L�}�0*�B���h�|lL�R>c�I��OlL�T>6"6�1�J�ؘh'}lL��>6&�J���π����g���Ciԑ�ߺ�Q��ǩ�D5ՙ0�1��Q��Q%�J��>F%ZO�	���τQ��g¨D�3aT���0*ю�L�hI}��ټ�oD��3�L�]�p�q��3�6�g¨D��3aT�]��0*Ѳ�L��h[}&|K��>6&�W��u��S�m�τ�V�g�E;�3ሢ����MD� �"�0H���L�h�}&R��>yJ� _	GLG̨���{�W�:E,0a������@���L8�h�}&R��>�"�0�1ȄA��A&2E�A	�L�̬M�O�2E��%�3E�3a���2jaU�/S�/~��s���"�����τ}��}&�3E�3a�)b�9j����a�)b�	���Q��~y,�6�%R��>)�k�	�-�Ϭ-"���E� �6��d֊���aD�YbD2kC��Af��1Ȭ1y�(_֢GL8b�f��"�0�1ȄA��A&2E�j�j�����)�	�L�L�e��e�/S�/~�"~����˄    _އҨ��	�L�Ld�d� S� s�V/Qe� �e��Ʉ#��#&1E1�)�	GLGL8b�8b�S��x,�n&,0Esr	GLќ\�Z:���"9`���0j#�#��#8b�8�#��#8b�8�#��#8b�8�#��#8b�8�ڧ(�ld��A��A�}��ڔ;��)���"�8��)���"�8��)���"�8����2j���Z�)�NX���qY����fyov��F���{AT������m������OTW������,w\����$-�D��r����%p_��)('Q=ū�[��v�+(}�*J�F�r�������W�"z6�U���|%i��%��F��ᯑ�Mѯ��)��7_M�ٽ�W<�Օ_�|u%������<�ڥ<�Օ�]���ꫂQ���x�!�Oc����1^]�����D�i�_]��1^]�����Dssc���͍�j�먣�m���J��|5)bNc���:���c��Q�1_]��ј����U}ՙ|���|u%�>c��A�1_]���X�&Ecgc��Ѣ�^Mޡ��?֫I����׍G��Օ�،�j��ն����?�F������Gm�뽿_g��z�������=��Ө��c���םF�����Ea��w� ��c������q��������<���V�l~�>���G�9�q��+���y�(��Ն(��Օ(���պuU����J�ޏ�jR44ΫgQ�?ΫgQ�?A��9t��{5)
�������k�����������l��;�߫������;�6�m�^W���u�Q�?��G�{��{�u�QK����E�4�{d�=�:ۨ}���{$
�'��h�e�{�}g�k��+J������^]���i��D��W�_w��r��� ג���u�g����/��rV�+�瘤�&��d�&�瘤�&��d�&�瘤�&��d�&�瘤�&����v�9jU�4Y'�q���+O͂�
�Ei��4ZGϊ�Ey��<Z42+��z��5��iQ$=+��sL�lړ@�D���"%��&�6�<�$7Q >	���s�>�I�m�y�I$n�L|���h�c��(Q�$�&��'��}�Qے'�����9
���T�D��$V7Q�>��M�O�u͂LRy��X�D��$�7�,�$�7Q�?I��{���G<���f�x�bԢ�$�����~����(b�D�&��'�ݹ�Zo<	�M��O�r�Qym�D�6n�Y��v���7	�M�xOo�(�U:������k��$l���ׂ�I�n��}��hV`�՛hV`��״Fmӛ��&:�l��hV`�	L4+0�&�s��"�C`q�	g0h����	i0j����	k0ќ�T��TLH��9�Z
9�v����f`�̱Z�8O	Y�
[Pa�6��j)��,���|�u0��@&����D������,h��|�j1kYV�����-&����D�f�jL�j��D��c"ҳ =&B=�c"ֳ`=vn���ؘ��_����4ZQe{9��ʆ3��3��q�grgZp&q�grgZp&�������-X��Xт�h�Ђ3��3�(�\T�p&���V{��ܮ�fu�
��q�Y���q����-X��XѪ�+Z5� bE���íNb�U#w�����a1�UC�)�US"޳j������D�f����,��Mz���w��]j����x�w�~�V��t}%��״F�r^0��J�.���{�T�r^���~�]\� �pX� ��~�]\d�.�����T�p��Nk�\��N�R�p���`	.b	��y+���"�~�\�,�Eg4-8�������0j��a��a,���Z5�&�N���YZ����`'�5�Q�W��E�c�?\�?��/������w\���.��;$Q�8��r��KpKX��E�4,�E,a�\�,�E,a�\�,�E,a�\�,�E,a�\4���.�{X0>�S�ÚOņ��l���v�l؉��Ɇ����l؉��Ɇ����l؉��Ɇ����l؉��Ɇ����l�yzѧv�"v�a'.b'v�"v�a'�=�f������p����J�Wo��V6�ÿˬM�����a����ud�����r{�������q��k��6sm2��ݿ��Kc�#����9�l}���([�d�!�����!��7�|�r�M.�\~�ˇ(��Q�s���|��?6�~�����kZ�֣l2���֬}&�\>D��&�Q.���C��or���\>��7��+�z����u������o�nM.�\~�ˇ(����!��7�||~�2�M.�\~�ˇ(��u:�h�b��D��v�O$����(��:�H4���x#�Ʈ�Ds�8�a�:I�]v�$�.��9��	��0�1��	�ٳ��}*�=!�=�"޳�=!�ٰ���+
+ڰ���H�Ȃ����lxO�vrlXQ�XцŸ�w��#�D�	�	�����	Ѭ̆��mXQ�Xц��T�g
ќ͆Q�h�f÷B4g�ac!bc{�I��Ol,Dll��B��6l,Dll��B��6l,Dll��B��6l,Dll��B��6l,��F*}�o��`��1d�:HTS�F"Fu`T!bTF"Fu`T!bTF"Fu`T!bTF"Fu`T!bTF"Fu`T!bT��l��7k�́3��38S�8Ӂ3��38S�ft�*D����BĨ�*D3:�"�u�[!��9�����c�E�
�s�E�
x^��{,0E�=�����S�DD�� S� 2E�� S� 2E�� �{(�ڐt��)����Ci�Z�L<����L�lЁ#�h6�� S� 2E�� S� 2E�� S4t��)�'kӀ�S�L�9r��"�y`��=Xf-�:�����L�9r��"�y`�)b���"�y`�)b���"�yF-�U6�3E���>3ne��e~�Y�2E�� S� 2E�Ԏ�<��E� Omx1�S+^D���#��Z#b��6Ĉ�1"yjGL�l�wjы�#8b�f�2E�� S� 2E�� S4�vV�>U6�2E���/S�/�2E���/S�/�2E���/S�/�2��Ҭ���"y`�)b��"yvm�U62�ˬ����"�x��)���"�x��)���"�x��)���߃e���Lќ܁#�hN�Z:'���L��nGe��F��թ����NG�nGuj8�w;�S���Q���ݎ��p��vT��#~���5q���)�*��ݎO��A~��S�=X&�r�KT��#~���5�������lG�nGek8�w;*[����wƎe�ߥZ�)�N�}���,�F��������2��¸DuyI^�}�X��]X����.����7Y��]8���..I�?Q��-�D�t	�����)('Q=ū'�5yS����}w{��o��^9j��w�W�vr���+H=�|��g��^I��n����+�����JC���ѧD��|5yg��W�]yu��W��^]i&������z������4�j�ƫ���άo;�Ն�>}w{u��O��^]i��w�WW��ݍ��]�\_%ƫ+<����J37���Օfl�۫ɯ�ͬ��Օ:}w{5�aN��^M~�����d���P��n��4��۫��uU_u&߬Du5_]i��w�WW����Օ����z5�;���jRC������CgYߝ֫I����j���3���zu�!6��^Mޡ��o����of=g׫��+�Qϲ��߯��QO���#����ޣ�;�Q=y���ם�N���YC������z�~�{�ި��~�{�ި�z���&����o#�gp��H3G4~罿�����{5��w�W����۫+M�����պuU����J��w{5�����Y��w{������ܞC��K��MM�}��n�眺��+Ql7v߻��d��K�6��;���{��9lbw���r�Bԕ��컼�����,Mw�����wu����=��}�m��q���7��G��#��0B�{\�!��}�h    >�wy����wG߻���^]���u����쳴r�������wV7p�:�3�~~y���cV;�W3�1��4��ȀM3�1��4��ȐM3�1��4��ȠM3�1��4��ȡo_����&��d�'�*��d�,Ȱ
�Ei�U-����hQm�GkfA�U������;F%ҢH�*���s#�6Q�m�&J�-���*�H�4����M���}��U�FI�M3�1�H�D�����f�c���u#Q7Q�nD���0W�2 7Q0n� ��:I�M����ru#W7Q�n릙F*o�Xވ�M����ifA��(�7R��`��~��߇�\�;r�U'��m�s�o?vE�F�n�����o���~b��()7��ۤ窟lD���U?�Ȼo�����x�(�6o��([�#��Y�m�s�6������0i���v#n7ͬ�0�z��
#�Mk�zґ֛漲aD���'0ͬ�0 ��(��2^D}�`"�`p�4��4��D��@&bk0͜�0@��H�A*n�����������0��6���yJ��T�CL�lp��d)�p�����L��;��D���,L��;��D��KLDKZb"��A-&b-n��*�b"��!5&B5�1�qX��f�c"�������8��6���� ���1͹aý4ZQe{9��ʆ3��3yM�8�Ù\ę��"��p&q&�3݇�d��pX��X�Ê\ĊV��C��L.�L���*�t,����aE��0Y�=��"���������8��E�+r+rX��X��새y?�X���~��Il�5�pgX�>�fD��k�A4��5� �=^#w�Ӈ����xA����ln��,Lw��v��{8��6�y�'�a��N�W"��mZ�Գ�q���U�Cܦ�~��`	��_<�E<��.���c�����]��;���T}���|��撮�Zep�����%��~U�� I�KpKpX�k�h�p���0��<֯Z�E�a.b�j�MT�0ל�4��s���Nn�]��������8��6���g	��6���g	�6�e�,�C�f���%���5�(z�a	.b	KpKpX��X��\���"����%��%8,�Ec�p�܃�0\4���5�������5;WF�N\�Nv�"v����������E�$`'.b';q;	؉��IX�Ӌ>����������E�$`'���X�=��"���܇Ҳ�0�)X	��}�,�'`�0��a�|y�ב�4�R����vf��x�m���\#��o�\l�Qg }o�4h�z��� [Q�d�!��r���A.�\>��C���|�r��:�C���C4�d�!��x�mZ��(#��o�Z^�\>D�|�ˇ(�r���A.�\>��o�[^�l=D�z���h#��o�]^ݚ\>D�|�ˇ(�r���A.��z4�ˇ(�r���Q��f)��'�Ou>�h#�P$�u��h#�x#�F��F�9���DsQ�#��K��H"�p��a�&D�&`6!b6�	��Yg~�>�������'D�#+
+
XQ�XQ���my�Ȃ�����'4;9F��BĊVtJ���#�D�	�	�	xO�xO�{B4+��������(D�b�)Ds6�
ќM��B4g�����uR��S�X��X��B��6"6���������l,Dl,`c!bc���>·�ey����Tg¨BĨF"F�0�1��Q��Q%�*D�*aT!bT	�
�JU�U¨BĨF"F�Vg��U�e�"Δp�q��3��3%�)D3:	�
�JU�U¨B4����o�h�'ac!��I�c�E�
�s�E�
x^��{�����#�h�'k���A&2E2a�)b�	�L�Ld�d� �CiՆ��#��#f�q�����+P�"������������٠�A��A&2E2a�)b�	�L�Ld�f�~�"~����˄_����}��}&��>XV-�J�e��e�/St�\�>S�>��"�����τ}��}&�3E�3G-�U6�3E�3a����jYZ�/�eՆ��A��A&2E2a�)b�Y;ZD2k���Afmx1Ȭ/"���È��Ĉdֆ��Z#b�Y;b�f#P��E/"��p�͜%2E2a�)b�	�L�Ld���r��#Qe�/S�/~�"~����˄_��_&�2E�2�)�	���U��"�0�1ȄA��A殭^�ʆA�˪��	GLGL8b�8b�S���"��p�qĄ#��#&�>XV-�LX`������9�<�tN�w2Er� ��a�F�GLGp�q�GLGp�q�GLGp�q�GLGp�q�a�OQT�0�1��L�0��`Y�)w�S�1Eq�S�1Eq�S�1Eq��e�2���ST�^�>E�yY�W>�f��?��������=���{^k�׊�ך�����}���Z����y�s_k���Ś�o�����:=�����y1GO��9��u�k��;���i��t�x��z�G��a=�#_������XO��A����� ��A��=$߇�{:H��=$_����xO��A�����A�����A�����A�����A�����A���~��t��:H�t��:H�t��:H�t��:H�t��:H�t��:H�t��:H�t��:H�t��:H�t��:H�t�I��A�� ��A�� ��A�� ��A�� ��A�� ��A�� �����AFOY�����^=d�2z:�"D�� �u���A�� �����AFOٯ����_�=d�2{:�~d�t��:��� �u���A�� ���l8LOٯ�̞r^�=�2{:�yd�t��:��� �u���A�� �����AVO9����r^Y=�r[:����Z:����Z:����[:����[:����[:����[:����[:����[:����[:����{:�a��t{d�t{��t{��t{��t����A�u���A�u���A�u���A�u���A�u���A����xe=-�\�_O�F�_O�G�_O�H�_O�I�_O�J�_O#�K�_O'�L�_O+�M�_O/);�GO���Ԟ^R~j��:KP�1Tg�=��,E��Q���H��$�Ku��ڣ���T{<Չ�j=��DT�Suf��=�U�z\Չ�j=��DV�[ub�Z��:�U��W����#�N�U�1V'ƪ�(�e�z�Չ�j=��58��K�V�G[�h���N�U�W'����s�z�Չ�j=���]�yu"�Z��:�W�G_�����YSx=���z։�j=
�Da��u�Z��:�X��b�X�֣�N4V��X'������zL։�j=*�\5���KpY�Gf�Ȭ�c�NlV��Y':������z�։�j=F��h��u��Z��:qZ�Gj�H��c��]����V��Z'^������z�։�j=j�Dm��u�Z��:�[��n�ح֣�N�V��['~�����a#-�da�Z��P\��q]8��#�.$W�\���h���z<ׅ�j=��Bt��ua�Z��P]��u]V'��dW�]������z|ׅ�j=��Bx��ua�Z��P^��y]8��#�.�W�^֫�h�����^�uZO/�{��u�)�=���{��u�z����^��{]x���.�W��^ޫ�x���{�ׅ��=��:T����z����^��{]x���.�W��^ޫ�x���{�ׅ��=���{��u���=��Z{����֞^RG��x���h��^W��㽮:���{]uLk������u�A�=���{��u�z����^��{]��{��%x���.�W��^ޫ�x���{�ׅ��=���{��u�z����^��{]x���.�W��^׬��{z	ޫ�x���{�ׅ��=���{��u�z����^��{]x���.�W��^ޫ�x���{�׵jEO/�{��u�z����^��{]x���.�W��^ޫ�x���{�ׅ��=���{��u�z���vm���%x���.�W��^ޫ�x��� o  {�ׅ��=���{��u�z����^��{]x���.�W��^שY-�d�z����^��{�x���n�W��^7ޫ�x���{�׍��=���{��u�z����^��{�V��zz	ޫ�x���{�׍��=���{��u�z����^��{�x���n�W��^7ޫ�x���{��������xm���%x���n����^7�k�x��5z�׍�=���{��u�F����^��{�x���n����^w�*��^��=���{��u�F����^��{�x���n����^7�k�x��5z�׍�=���{��ug���%x���n����^7�k�x��5z�׍�=���{��u�F����^��{�x���n����^7�k�x��5z�׍�=���{��u�F����^��{�x���n����^7�k�x��5z�׍�=���{��u�F����^��{�x���n����^7�k�x��5z�׍�=���{��u�F����^��{�x���n����^7�k�x��5z�׍�=���{��u�F����^��{�x���n����^7�k�x��5z�׍�=���{��u�F����^��{�x���n����^7�k�x��5z�׍�=���{��u�F��z�^��{=x�������^�k�x��5z�׃�=���{����F��z�^��{=x�������^�k�x��5z�׃�=���{����F��z�^��{=x�������^�k�x��5z�׃�=���{����f��z�^��{=x�������^�k�x��5{�׃��=���{����f��z�^��{=x�������^�k�x��5{�׃��=���{����f��z�^��{=x�������^�k�x��5{�׃��=���{����f��z�^��{=x�������^�k�x��5{�׃��=���{����f��z�^��{=x�������^�k�x��5{�׃��=���{����f��z�^��{=x�������^�k�x��5{�׃��=���{����f��z�^��{=x�������^�k�x��5{�׃��=���{����f��z�^��{=x�������^�k�x��5{�׃��=���{����f��z�^��{=x�������^�k�x��5{�׃��=���{����f��z�^��{=x�������^�k�x����-���j����{5zI�������{�^�^��~�F/i�^�W���x�߫�KZ�����%-���j���u��^��{�^�^��~�F/i�^�W���x�߫�KZ�����%-���j����{5zI�������{�^�^����k�x�߫�KZ����^/-��������?����      �   �  x��Ko�F���P�T���>�S.E}iiP �3fKI�Ϳ�Pʣ];�*���A;�$gw>�ce�x�/���aZ���f
t��}�!�
Q��b�Z��c���O��[>���z�����<���'<�۸K��U��x���Z�V��&?�M�W~�g�=�j�^��+��0���:�yz����Y������~6��~`�/�ty�M})y���Y�y���r?�~�q���[���,��-:��x��t=�P��a����0Ny�C����v��Ώ��wC?�a�:"/��n� ��!ϋz����� ?���mN�i�2{������x|�_�V����a^��EG����M?��Zz��aڤ�L8�8�w��<N=�=�p���9�F�m���Ϧ¼#%�bZ� �j���IՁi���U�#Ѣ�R�*��/tN��HNf#U�N� )B�^�ήVg�L�@� ��ŁN�C�|�#l%�0P���V(4�芆l����BU2:Iҧ D�%��p��,=}�0�.��$���#4�d�c��l���`��U����d�vV(EPr��P*�f���b��K�>p�wl*jn��F�&�*n��>�{�u�P׮G�	h��Q�*�ː|IA��@$ �	B����(r�u
�%[iPc]�'&�ШS�)���9q0I>I� S�.eg��z���)j)��V�#m�*�Ȋ���0X+�@"�l0'	��S�Ik�.@�5,�7UR�F#ijTE�RFg�ED�%E!2?�h�C�Z=<>#��/���X���w��U�r�2�i\��ଉ��Vc%)�3�0���<���>�ܻ���
�+X��`���VXk����L2I^h,^I��XvHE�S�/�>���9���~�%b'š�pI�*9��P�D�9��� B��6+s���&�~��?���H4����ψ�2K��F+�Uԁl*E����P���qv�>���pN�A�bU9���.�7�G�a���dl�:t�Jhr�@ݟBbC�R,YJ>H�[��ǀ��q~�T������lm�t�	�J�D��
tN2�uː	�+�S�-��^Z��q:��|����Ԇ�!SJw�[�:�Q����Ya�m�n<2��ț����\#��f6�F���=��:a��V1�Z�@6�i�?��4"      <   �   x�%�A
�0��ur
�H&M�v[\�q�ML������w?o�[e�qj�j�`#�+�_���z�;a�3:P��U�a���1������yIeNa-�d�d:й�g�I�SbO�-�{��M'��.s(s      s      x������ � �      >   h   x��K@0���*�@�;'AL��D���t@;��w�'�Ā�I�]�r�4�����<���1
�=��e=�2���f��G%S�ц��M���S����      @      x��\[o�8�~��
���X-��P�,�N:��ҙN_�rLۚ�r�Kg&8?�<�oا~�;U�l�*�����]��,��U�H#9��t�o�`˴�t�U�y��G{E�'�.Ӣ�Ӽn�k����b�.k6�^2�줘�ǿ�f\��F�^��q�1���#��d�'cΙ�PM�ȋes���/�D �Õ���H��9��������4�
���ߗ�B�l�>�1H�\�G�`Z"x �����F�����Ƭ̪�I��Q�M::� �ؓ\�܅aǤ�"� b�@�i��.q�h�\�w�w��պb��N�l�X�_qȦY��Tdp�[�B��y�)KNg5���^�FWL� 8XYg��&�ۿ@@�y�'q��B-���C�(�� ��.�|��|�����8��Ń�yS4>A�$�o�vpr��K��f�D�7�z�s�PB���Z;�p,���P?x��i�QW���I?V���1�`" 8 ���r�Nu�gU�a�*�y�]��ΪS��Bp?o
`�$A!ɞˬ�˰l�ަ�����J�X)��I�EHpI�ˆ�����U�.�W5,0�����M��{�Z��K��KZb���A��K�Y�0O x����J�f�o�c�z��/ˇoU6˚;<���@�Wu�^���z��ժ���u]���l�iv�.�٢)�:�O!� �-tu�!}��r���3��M__�e͊�����d0T5��d���[�qŅpE�������R�a����u��i�i��қt���h�������>R"4l=_�]�4�p;Yrh$^F*p-��ށ��X��x�SVi�C�_��L�z��Մ���fi�3p����r���_;��0���s&^�'ܗϛ��Hv�)���k�F�]��+�'��4������K��f���A;�]�{��a��º�Y������m�R2NB�S�L��~�X��Kg��.ޱ�5
���.�3�܏�ޅ�aK��w;L��1�Sl��BKps��5���Z�7)��Ȅ���;��<t&^�8t�NjRc.z�'�4Z����o,�W�^�� [�{�lGv�b1�)Ӆ�����Mp3�˄����_�@���;x�~3^ˎ@c���>�������6�bY׷��͛��;o�u��d��g͛�N˩�`�}����AI�vy�Y�C��o��%����	�}�H�lQB��o��?=���Ƨ!2��B�i�MX�N�ҡe髟�ش�o��o!�y͌�H%���=��(��0RN-'�5�i	��u��R�p7�Dʵ�z&^v8(��a?]�(1���Zc`�|!;"�	@2�����H��R^h)�Ĥ���3T��Oȿ�t1���ڒ~���(�;���u�1?A�F�0�����;��UZ�߂x� �Ώ��M~��+k�Ë��!Y����?-K��f�́?� H��/� t]��f)���3�Blq����~��wPz��,�)|��z��>d�uS�{B.���F�d{p�i��G�~��kX�[�o��gMq��-�w�w�i}�)�1�(d��kv���)�AøU�^ ��b����U�Um"��l��;҆s}��Y*�@^7VQ"��z&e�0��*��*��?�g�`�޾}:K�{�Cpw�5)�||�I��CK�[iN�eo�b	=����;ZM�Gf�LK�<d�	;hnsL�Ql��eM�z�K�Bm�XM��L��@������z����pH��7��߿f����f����ڬ+~>�sH7�Х�z&;4�Q��mȨ������1a!��j�����g��UwYI��|��X���<Ԥ0=1a�����$���XV1���u��FO�띉~��w<�`v#r�3&��$U�UW{��򜽜�Q�����3����>��ɖg &@jc��d���PB9+/=���@�$Y�ޛ�pZ�pA�94p�NWL�{�l���P�,#��1��z,.Jڑ%�
�C߮��~͸ĉT�;u&e

�?(KGѦ�ZG�G̫![�l���`&�����XPϡ�YҀ9�^f��8>B��wi�B�&���,ą'�
.XԤP������Y��Hq�網i�<�K!�!����1 �d�\.NM1�a��CG�$�l�:�h���V�-�N���z9Mg&�C{C�9�	�C��S�A�IA6�q>�q�
������x[b����cA0�HB�%#�
���H�Xl!�4��6���p��-fuf����Y�6@������� `�S,Cq\z��K�P �{**�q<�q4��z��Ӈ�˼�[�~���7�7�u�54]���x��"���M�V�n��4W�������n
��1%�Xn��[��^]�|�^ C廂5���&%�8x�+�hH��R��+5��ǒtb��p����Ճ �Q!v�6�hϘF�xS����A�\߀�`�`��%�Ȑ�Dl���^�PD���-5ŭ:&夘�@�5[&��Y:]��ut�3,��-��׌_ ��������a�\���I�`�����{�_��"�ڸ�y�˙��p$r`8;S�T�s9Jq�o�ڿ<�ezn��۽���/w����5�� k��<1��`n-�z�7��e�����Ӽ2̀�����/\���R=�."AeeA1��nn�O���J�PI�{u&s{����_ʝ������<�꺑&���0Ѷ%��J�m˶ay�-@"�Y��76�ׅ������0�&��WVDwId8D�0:apW�CMf�h�(�(IbƦ�:�l7Eؠ�~Xݤ�÷\���m���B/Q@��`E��?E�	.�31���`R)V�!��I�5N�����"y>ۍ<,F��"5ŦtK䚢\��`y>_��M�l�L��P�R��bW�ܪ�ì��b���7�j����08�T�Fؙ,B�#(�Y*v�&������+�{6�7�k�V�x`E����"�ɉ<S�Δj3.����|��ji��He+/XW�^�{q��
r,�Ǯ�LMJHZOP���e���%�S"�R[.���y(�dw·���
gMM��E)+���w��1��7�^���E92�&v�����T��-6�CΑ�>|kfz�	�YJ7CU��8���"C�D�$���i��O��R�~	�	�����jtk�l�J�Or;9���]
N(&��e��Ūyn�� Ʃ�t�[���ˇC���N��"̜�	���McP�m����,���a��"���+��i���F1)9v�eg�IJ(w$A�JauH�)�=�I�+��[э����0"]{ z���㉊H(�$�F��p��v���$� =N��x4���X���?V��K=���x�& ��	%}�LQ��� S�I,�H��]��Nq��hmO���7o�fu]��h�<��Be;��tc5ѥݦ[�~�P@J�p���<�O�zB�(�tN�snاe�1nA���Y��Ɠ����b(�#�X
�o-a[���Q�I,�g�r�����F7�������/����D�=�3X�5Ny&Iv`ͳ\WT�^��E0UQ�3=FD&�[����&dSMo6�=��ӓ���z�}}�$<�0�t����D�Y��:���-˺��/�ip'%��Ϙ
Bbxc����S^�f�9cTQ��Ֆ�^4g*�B�3��ݞС�l�����b��O8��ݘ���/�ڽg2Wq�F�	
K �A�@���K��uz{�Rǌ�b*�6��3<�NQ@�mL5�b"~��\7k�n_���{*T�C��L� �B
+�Q�����	ܒb�y�*�� 5��؄��kwI�bݗGXD�m�7v��k+{o`�r����7�Ј	��͕+"QV�h��X�M+$�f5�w�2%�P?Ny4�]���$�1�&A�Ө(��	P���FkL�7�I<�A �nJjy�Fp�;:Z�u��^p��"��8��,�N���	���m=�OB�72���>�B�2��Rg�12� h E  �b{#��{0�f�����ܙ6����E>6�r�6X ���Y����o���Z��xA�@�
��Z�d�H�-�!��n��9�^&v��}�.ָ���d$�PzEc�@0"����)4%F⒜R�l\�MA�4%��k��a�IP��MA�ھMưwR�a�a�$��W�dAr�����~m��}~�?~��fv����y��֜g���$f��|��,s_��`?��|��ݜ��r���d,�3�H���ã'��g��L�F�Tq��.Ʀ.���f#�����1:�]e*%?��������(�Ī-#oe��=�K�
�d	�����Փ�����[o}�"���aWP�஍�=�c���x�$�sܲ�W����to���x���Ld�*=�c��%�'�^d�́7d�6{+�}!��cq�i�� ��|',�a��f�MR`�i�}�@��.��L/5(��]Z��A1���m�x����Y
zQ	�NM�I�)tJw���'�]LG�L���˵i���r��UY�:<�� �'��v��u3*zc"�|?tV�z&�>�ƪ�\�2�@	��/{�6[��t�t�#�C�(r��]�En��nH��^bU�6lھre��`����[>�5���"��!��m��]2��l
��S��a��кH���&�FA�N�JM��	�XP�Ѧ���x�אm�e'�^�GA�Ux�iS`��D�JL��;���{��1*�3�[���:�ؗu>�q����<)}�dzj2(A�LJEb��u��������k~�`)�T����e�yl$�=�8��0���w����v��LA{�E��lC�.m"4a��&؇�o�J�~�ۆ$q�Gf��>]�&������kH�f3�;��p�U�/y)<�}҂��z�H;汴��Μ�jIg�.��}H��*u	o6���ra�ܟuyg�=V����2JWl���*Xv�=��)L��b���jSP~�s�@S�՗�i�|$w��Kۣ�0�N�. �M�����q��C�=�6e�	̖��� Y�ۓ��g�iбe�P�����$��$O�r�0)0�k�x/6��10��~9��|���1��Bȯ�W���	n>��ÍݮOs,�L��ny�hK�P�CrO	Wi��yR]�T�?������*��F:��B��6�"Ȉ�^�f����m �Gb��^(��Q����.e3ٞ|3g�Q}�$g���+E�fb�\�{&3����W��6^�y��YX�ݏc���[���?�k���5��V^���)��q�_ḿы"?r�R�6#����Fř)+ F��(���7m�L�xhO&��gz&�sT=�?\�?\�Wlk!���u������p�X�4�6�v�C�=r���3IÑ� �l��P�Uݠ��¬�'�&��y]�ĺ߭��3d׉�/�>����Qp�t[��@p�g}�!�{&i��7�v��R�l���3�4�-�0[����ɀ����D�c{x�	{���ˏKoo���_O�b�nJ3J��T���"-�m���%���a��&��i���-]��Ϻ��l�����r�=7>�v�l{����.�����[�صa�g��Hś�xG�n�!$��3D��x���@)���s�%��nO%�r�|��,�������lJ�I���=��-o��P�)X�Q m��A�+��v�w���/|{x�q9	�Ix���DQ��΁�a��2{�6�-E����@�����RS܆�}x���×$�)���ė��U�]0�l
�1�	u+FR0m��f#�D8�$�V��(X� g" Q�&gs�a�_�~��� �m�      V   m   x�M��
�0E�w>F�y��'�H���@����n���{�D�i�9��"0��oi!�݁���&#� Xj-x���S������Bd6j��M͌�} t~�      T   #  x���KN�0D��),���ږ8 w`�CeD2 n�4��H�_����EK�aYNY�����#�b�`�I@�{~[�n8Ly��8�OE���آh�d1�7��$�����_���"j��!�3DD
l�\�ٙ�_a�;NX��XYڱK��k>�_��9�-EM�HD�9
n�x�|�G�OS?�A%e5Ą1�hXa�)[R���4?��8/kB�*ݦ��ؿ�{��F��X2V,�n�뾄�|����p��%�`J(F��lp(�����}�s�h��#��N~4M�|�"��      �      x�3�4�4������� i      B   3   x�3�4A#]C]#K#c+S+=S3##S�4�?J����� �;�      w      x������ � �      D      x������ � �      ~      x������ � �      �      x������ � �      F   f   x�5�9�0k�| �����ZJ�����N�Ҁ�4��s�0m۲��覱'��g�"��M�jW[S.�g�
������h��H
����>ō�9�%qQ      |      x�3�4�2�4bc�=... I      \      x������ � �      �      x������ � �      ^      x������ � �      H   D  x��X;s�0��_�N��&i�x�лvh����`�5*II��/I�)1N�.W� ?����b΁�@��)���bq'�eC�ET��_��.��&`7��נh ���K���1�a�m�nE$�V: ^\JR;8�Z����cP�X��ֽ��#�ʚ��\)�j@
*���jE����g�(a�kf
̘����v��x'LKɎ��^[�O/���P�Jq,��u�!يL�L�\�}l6����&�n��M�&B�J�Gk
U
֠@����(ǔ���3�l.hU3l�Y�Nϣ%00v+���`��-\UJ6�ũn�ڭ�ETR]1�Gz��$�L}�����{
u��*s�B�e r-r��S�u0�5��.Y�H�+������'V�6�$��[d�Qb���~���޶r[m�]M��� IE~t0��������H/\f�fz�v����W�W[���bh�ꪒ�,��-����QҵosB�@q��-dM!�0��\�fn>�
g9���p�P<=�T���)H�c��ψ����/_�^wH��H��s�\v��m� ��.^'��ē��\�ϲ_�0S�dŅ�L���3�w&���m͓^	qY֣��t	�Pw�}��D��W�����LLAUB�r�D7x��X�g��?"��/�f��	�q�+��(G���`o�Tb��Q�P��.mp,�c��r7�2�;����fu�d��u���k����3��]�ؽ�Ч��?|nG�9>i�����C6���l�m��m< �Z�p�g��\(�uq#��sYk�o�u�Vϟ����_<W�|�,<��C��L��eO߽��f?�@�b      �      x������ � �      +   X  x�}�K�7E��K~Ei/2�d�����T�%w�9��Y���$e!%#��FI�	�����I#I�N2H�TH��H�4Hi�v�A�dB�-��ɂ��%Y'�L.�J��<�y�w�A�B�F�E�H�N1�15��Ԍ�S�}��֩J�J�4J�J����rPg�B]�u��ux۩LCh(��4�F��4�����# ��0B	#(��0g��2���D�T��\8\b\���߅j���BgQ�)/4{Y�YҴ�I��U���Ę'ˉq�U�!�qa����0N\�#D��B��T8�yG�����k���~�w��� 3�~�ka�͏�Sk�;w�;^IY��ݭ^r��q�����ӊ��ǹ!�M���}��X�qa	��g蜇��H��.+��*��WUY)w���y9�u.P/L�>f4 1�(Ü��}j���?�����S6}c�ֲ0��_`GI%�[c��+e�t��p�������q��?��c���a:�_�z�h/L����q?�ʅП]�������ٱo�v}��V�g7�+�-�����:����/\����ՑJ#u�֊���Z�6�}�ku���V�8Qrar��}�DT���B9Q_�c���sG�X��J�R�F4�\��K�l��ꄅJ�UR�V7o�AUW�_@��O�j��B�*tV��.;��
G^(��<�_ք�46�:H��B��}C�-�ވ���e�H�okU���J;ď��1O0J�} �/�*�_�[��%����eŔ�'�\/y�t[��9E�vʀ�.�WМ��^��c_��R\�,��Np���kX��N8^��J��=q�(�>#/�X��ynD>�v�br�����w�7��#m4�}��V���UZ�,9�}�Q*��F�d%7����s	����o��rx!�
��q]6Bh�K�	��}#���Vgh���������/�����n|`���p�:�C��@��tO��q�n3���Y��Ku��Ʊ^V-������k�\y�z` �z�8��p��:�R=��x����������~����?����/��D?1Nl'�O;W'�gbv|a�/l���?lVs�g�|��с�|����y��]��u��պO��M=,�D�A���|�����j�P�O���^/�[̩�J�lX���V�Fj�>���	�d����d��W�O[�#I�i]�0e��Mk0<�]�z1?ų*�f�Ö�w�+�e�d4���T;��,��D��a.賭^�qO�{�?�lV����w����g[���͚}��:O�&8����s4ۣ>�>=�`=��\� e�J�s%2��3�������H��      `   _  x��Umo�6�L�
-���z׷)�C$Ê�Z�e���T\�~����%�����;�{�Hѓ5�gB�bRF8�h��IE�U��qN�5b�G��y+���!�0q\y��p��A�6f�;�b�qm��3�=�y�$q����P�mE#��4.�8ȑu|p�l�N���<6NHj���w�]@&iBiP"'�� Ӝ�$ x�ڞ�ވZv\���2�g!h�bX<6r�Ns�Q�yq�9ϋ" ���7�=��$�i@b�c��\*��㴄=�ŎI':�	=2�׈^�(I��:�{����CZ�U�A-0-���gN:%нh:��O����U�I�턪P�ėcI^CK��	�$G=p�{!��I�-����%�w�y-@
d�-�\@��-q��,I@J�k'��۳���"h_pK�2'�hk������\��_:f��<���q�6
m���CO2�ȳu�����@\I
�Q����{'���3���
cJ1'A;�Y2�M�Y���&O(N`B4�)�I�@g���1%s[� ����&!��4��bE�2���jl���MJbMg7(����a\�o��n�[_"�0��\U��>I�}��وAhw[א���ެ�_�F*��^�T����Ԙ�>g����oqj�O𘣾Ā:�uf��m� jX�p�x���Uxu����e�j��F�|R?�^7����v(0ė���_�Ze�\��y�+H�q�@  ��`s�(;s�`�G�S��L3m��������-W~��n�2�.g5����-t�{�ʉy��3g^@vb��gO��/�� 7A�~z�������������?^�d�� �]���#�����u���*���o~��U�����+0��a�b�=���x���z���>q���A�?A��`O�0�A��t[V�Э�v���>"�GB�+
=�WE��I@�钂X�kQy�	�X�#�|i�(����<�zI"��0�chM���"��1���֌�</a ��k�>��GW�;�gt�-%e�|����=����Ҩf�X�x%D����������e^��yN���8��J��`��?r��8x\A�/0�߼      j   �   x���Mn1F�p���X���"�f�h����՜��^��(R�h��'|�"h08}l������u}�B\V��h�ԇ�"Rc�LA�3�a�;��v��x=�Nu�-T�PUbN,�z�;��Ҷ�v�ʩ��Ky������|���-=w�ʁ�<��?�}����mm��S�U�YKV�!L����(��0�Ӌ֔��[@�_�j�      J   (  x�}TI��P\ۧ�,zt�l��$�H;�`�D�(T���[|��^k~\ \���Gwm�c��5|�akGb=.�w�;���8���7r���΂9��X:);%��+��"���t�-�6�� w�tE²��>>���q��2(��z}�(��R��m/}��w1��d7���T>����!;EV���Vԓwc����wYY׈�o���d��	r"�N�o�)|�`T�H�"Ʃ�|tM���3MF��}%�*7o��d�[٪R�`�7��2:�4��^Ĭ�d��>��� 7Ci0hQ_���1O�;I�/Ϊ�|�C]9����b)QC�W�
�c��%KW�'���wV&T�oP�=�4-c�%�G#jH;���Sy�D
8�������>�q"{CjB$�$����v�3�Uk&�:�ux��w�$aֲ}��Z
���;$'��Q�p���؀�sN�,8[��������U��Ӎ��Xum�w(��:���q�]����=���B�V��N�OPT�7�������!���B�r�v\��_���      L   C   x�3�tK��I-�,�4�L�4�4�2
%���Ō�$f��W�ZT\rxOIUi^:P�1Ty� ��      X     x��R�j�0='_�[O��v�q���$[��-��9-t6��f==K�=�P=TMӴl9�P聓��Sg�n�+�K1�����k:W/�C \
3._��^3!􉵀W�p ��Ym���χ�x��E���i�����wF��H��y���B��k�?Z������s7ddUxK)Zs�Ա�3�cAЩ_�� .MTV4���C�A�ŋ/y�s$Br�9mBUt�y�<���1yG֜"}�<�����`5��/?���_�x}g�����u�	��hs      N   �  x����n�0���)x���ؘ[N=5�J�E�l<(���l�}�V�t�nT$#`<���Ɛ)�ü��i9��i��jF����l� C�׀=p��:+���җ�r�yU}�˭������S������RډE��zC�7�7���z��g�����BZ���y:?�/���+W̏�e��b���))dFVʊ�D��Ɣ��fo�&r1����ٵ��֚�B��&����-9���Ջ���:콹�[5�򘐥��С5���c�G��?D+41O��� ��Ɏ9�0��2�߁�$�c��.�o��W󰷽�9	���z��2[�����4�²X�1�Y�eL�u:�ƌ��a`����L�{E;@9��	�k�w���zj������q      P     x�}�;r9Eci��@�x;p�T��G�V��z(�)��y$�g�Z?��Q�����`����G��0��f]��$[���?__��|�ʆ��0C䪣i'd�w9"�������Jk�����rܻ�"u��|�.�4$�2�yV�9zf�G	@�q����M�R���gK)�T-�ɭ	�wR@�ˆw�:��;��tgbd������J��qz�"�N
HvG��Au�̅��W��7�c�,�9s�-�;�m@�[�]��O�D�>���R@��<:!R�L���R@�)�<��i�R@�m����Z!rKyE����b�����+��}������49G���0ʖR�,ml) =z`@J��5�D[
H�<���e�r�KyE�zkq����r8\�֙���r�W�DjΧ�Y��R@.�Ktp5g�^���R@�;F˳3�D[
H��|�`�*�^��ktu-k��6Y[
�z7Y�B�%���V�c�6~k�h$�#ds�ǶuM�jI�8R@v/fx�}ҕ����Nl�:^��X9ѥ��w?�Ls�Ƌ{�?[	@�:��-��l���^sP �2i�Y�{��To���
K�,O�9<j:F�Jڒ�9R@z�A��n�D�[
H��<�����:[
H�9h��uv��K�tl�.[���Ȑ[
�q����:s�>��o) �D���kde���^qTo[f#�dsđR	�J[7Ij����^q��ԕ�dGzEN��`j�z�j�[
�z'��ݺöf�[
H�<�v��j���[	�'r���"d?�{K�o��{�1ؤK��ZG79�����-�wko�BL�#]
�ugt��YHe�,el) �Y9�,<X���H���Kˇ�&�-�"ݹz�,�����R@�K̕����,il) =v*Jm�Tl)1G��ͫ#ڥ�H��=�) �_��m���Х�_n) ����K������M�J ��،���s����[
��Up�ڥ=��R�A(�ٺ����x��T?8pt��fQ�&�#�"�{6^y��(�;G
H��	���_�Ѳ�{��do]�y���.���#����g�;�N:�#d��+��i�f}���+z ak'�R��) ��%2O�������ߨ�.��ϙ�o% �yW��G
�'vrVK�ٱ��T�R����$�4���'p�+����+�$���^�"|ڀ�ue��R@6G"Z�^�Y�l) �����Y����rK9��(i���bgK镧!�2ǚ3)G
ȧ���+Sڮ) ����Qi���) �F2hX,%�,�m���� �T�d�ϜhK�z��u"SVf�-�ӵ!�Vm-G��O�Y9���t�� ���k�D�� �G֧) �[���9�ĉ���w	�q��W6�) ��K�K.�y��R@z��v��Y�����:x+4gˈ��
�޵	hmY�s��[	�+r�/�ȁ��h5�u��|�:�5v3��Б�ߧ��H�zխ��eD�-�:z�֎��wQ0�غ�3{�J z��f�6��g���H�|EH+ҔyGzA�.�����m$�      p      x�3��,..M�05�4����� .�      h      x������ � �      f      x������ � �      d      x������ � �      y      x������ � �      b      x�3�4�����4����� ��      R   �  x���;n1��x�0����(Q�D�`Cd���b1�����4�l*_��S��^>?O��^~������������������QĂX�UbU�kb�XW�Ll8'�)��F�I3��z1D�A�^y�-����hr�����c�q���Dn�a���0G�s�"��'���U9�[ir��kq�!�9�w$�%�9"�a�boq|�(n�B���^�N��&����w|�YŐ�S�s,9�Q�樻���X����gp��'9G���C�sLW�ܒ�#0�a��O#���*�-�Z�U9�[kr��k�q�!�9�W3�%�9z�ú�}������ȅ?_��N��M�g�~ŝ~Շw9�)�9�w��Hr�c�w�C�΀u!�9���c�5�����c�q����Is�=ub���=��-��rX���8GW�8ǐ�ӫ�ܒ�kJ�ú��{`w.���M�ܹ
�k��S������o�[~+�rJ�p!̾!!<�,��	a�����l@�=n#��N��'d�9yDF�=�"<O�0<�"�C6���Q���yŭE��g�%y�Ax�7��axAX=8 l�v��Y����iOP����4���aqDx���Ո��� ����5��]�py}�g� �^���#��"�W�	����x��#U�     
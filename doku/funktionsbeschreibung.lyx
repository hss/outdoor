#LyX 2.3 created this file. For more info see http://www.lyx.org/
\lyxformat 544
\begin_document
\begin_header
\save_transient_properties true
\origin unavailable
\textclass article
\use_default_options true
\maintain_unincluded_children false
\language english
\language_package default
\inputencoding auto
\fontencoding global
\font_roman "default" "default"
\font_sans "default" "default"
\font_typewriter "default" "default"
\font_math "auto" "auto"
\font_default_family sfdefault
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100 100
\font_tt_scale 100 100
\use_microtype false
\use_dash_ligatures true
\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize default
\spacing single
\use_hyperref false
\papersize default
\use_geometry true
\use_package amsmath 1
\use_package amssymb 1
\use_package cancel 1
\use_package esint 1
\use_package mathdots 1
\use_package mathtools 1
\use_package mhchem 1
\use_package stackrel 1
\use_package stmaryrd 1
\use_package undertilde 1
\cite_engine basic
\cite_engine_type default
\biblio_style plain
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\justification true
\use_refstyle 1
\use_minted 0
\index Index
\shortcut idx
\color #008000
\end_index
\leftmargin 4cm
\topmargin 2cm
\rightmargin 4cm
\bottommargin 2cm
\secnumdepth 3
\tocdepth 3
\paragraph_separation skip
\defskip smallskip
\is_math_indent 0
\math_numbering_side default
\quotes_style english
\dynamic_quotes 0
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Section*
Funktionsbeschreibung Lesezugang (IST-Zustand)
\end_layout

\begin_layout Standard
V36 Stand: 11.02.2022
\end_layout

\begin_layout Standard
Die Tabelle PLANVERZEICHNIS 
\begin_inset Foot
status collapsed

\begin_layout Plain Layout
Anlage A
\end_layout

\end_inset

wird mit 30 Datensätzen mit 7 Spalten dargestellt.
 Die Sortierung ist in der Default-Einstellung nach der Spalte1 absteigend.
 Weitere Funktionen: 
\end_layout

\begin_layout Enumerate
Filter/Volltextsuche über alle Spalten 
\end_layout

\begin_layout Enumerate
Möglichkeit der Sortierung für jede Spalte 
\end_layout

\begin_layout Enumerate
Paginierung (Seiten blättern) nach 30 Datensätzen 
\end_layout

\begin_layout Enumerate
Spalte4 enthält bis zu 2 klickbare Links auf einen Ordner oder eine Datei
 
\end_layout

\begin_layout Standard
Nach der Tabelle folgt ein Absatz mit statischem HTML.
 Das bisherige Oracle-Datenmodell ist hier
\begin_inset Foot
status open

\begin_layout Plain Layout
Anlage B
\end_layout

\end_inset


\end_layout

\begin_layout Paragraph
Datenquelle 
\end_layout

\begin_layout Standard
ist entweder zukünftig eine Datei (XML oder CSV) oder der content kommt
 per SQL aus einer Datenbank (IST-Zustand).
 Standard SQLProdukte des SCC sind wenn möglich zu verwenden, die SQL-Befehlszei
le muss verfügbar sein.
 Bisheriger Bestand: 15000 Datensätze 
\end_layout

\begin_layout Paragraph
Ein webServer 
\end_layout

\begin_layout Standard
liefert die Inhalte per https-Protokoll aus.
 Je nach Einstellung des Browsers startet entweder der Download oder die
 Datei wird nur online angezeigt.
 Beim link auf einen Ordner wird die Dateiliste zur Auswahl ausgliefert.
 
\end_layout

\begin_layout Standard
Das Verfahren bzw.
 die Anwendung ist seit 11 Jahren in Betrieb für Handwerker und Dienstleister.
 Anlässe sind Störmeldungen, Instandhaltung und Reparaturaufträge.
 Der bisheriger strukturierter Datenbestand umfasst 60GB.
 Vorrangiges Datenformat ist PDF mit ca.80 Prozent, der Rest ist DWG oder
 DOC.
 
\end_layout

\begin_layout Standard
SOLL-Zustand: Sichere Versorgung im Intranet für 3 Organisationseinheiten
 Bauplanung, FM und IM 
\end_layout

\begin_layout Standard
Authentifizierung noch unklar: singlesign on oder ldap oder ...
 
\end_layout

\begin_layout Standard
Muss ein eigenes vlan beantragt werden? 
\end_layout

\begin_layout Standard
optional bei SQL: Ein trigger 
\begin_inset Foot
status open

\begin_layout Plain Layout
Anlage C
\end_layout

\end_inset

protokolliert / erzeugt pro Änderung zwei neue Datensätze in der Tabelle
 PLANVERZEICHNIS_AUDIT 
\begin_inset Newpage pagebreak
\end_inset


\end_layout

\begin_layout Standard
\begin_inset CommandInset line
LatexCommand rule
offset "0.5ex"
width "100col%"
height "1pt"

\end_inset


\end_layout

\begin_layout Standard
Anlage A: <screenshot> 
\begin_inset Graphics
	filename screen01.jpg
	width 6cm
	height 6cm
	keepAspectRatio

\end_inset


\end_layout

\begin_layout Standard
\begin_inset CommandInset line
LatexCommand rule
offset "0.5ex"
width "100col%"
height "1pt"

\end_inset


\end_layout

\begin_layout Standard
Anlage B: 
\end_layout

\begin_layout LyX-Code
CREATE TABLE "PLANVERZEICHNIS" 
\end_layout

\begin_layout LyX-Code
( "ID1" NUMBER, 
\end_layout

\begin_layout LyX-Code
"OID" NUMBER, 
\end_layout

\begin_layout LyX-Code
"ID" VARCHAR2(30), 
\end_layout

\begin_layout LyX-Code
"BEZEICHNUNG" VARCHAR2(255), 
\end_layout

\begin_layout LyX-Code
"REGAL" VARCHAR2(30),
\end_layout

\begin_layout LyX-Code
"ZULETZT_GEAENDERT" DATE, 
\end_layout

\begin_layout LyX-Code
"LINK" VARCHAR2(255), 
\end_layout

\begin_layout LyX-Code
"STANDORT_2" VARCHAR2(40), 
\end_layout

\begin_layout LyX-Code
CONSTRAINT "PLANVERZEICHNIS_PK" PRIMARY KEY ("ID1") ENABLE
\end_layout

\begin_layout LyX-Code
) ;
\end_layout

\begin_layout Standard
\begin_inset CommandInset line
LatexCommand rule
offset "0.5ex"
width "100col%"
height "1pt"

\end_inset


\end_layout

\begin_layout Standard
Anlage C: 
\end_layout

\begin_layout LyX-Code
create or replace trigger "bi_PLANVERZEICHNIS" 
\end_layout

\begin_layout LyX-Code
 before insert on "PLANVERZEICHNIS" 
\end_layout

\begin_layout LyX-Code
 for each row 
\end_layout

\begin_layout LyX-Code
begin 
\end_layout

\begin_layout LyX-Code
 for c1 in ( 
\end_layout

\begin_layout LyX-Code
  select "PLANVERZEICHNIS_SEQ".nextval next_val 
\end_layout

\begin_layout LyX-Code
  from dual 
\end_layout

\begin_layout LyX-Code
 ) loop 
\end_layout

\begin_layout LyX-Code
 :new."ID1" := c1.next_val; 
\end_layout

\begin_layout LyX-Code
 end loop; 
\end_layout

\begin_layout LyX-Code
end;
\end_layout

\begin_layout Standard
\begin_inset CommandInset line
LatexCommand rule
offset "0.5ex"
width "100col%"
height "1pt"

\end_inset


\end_layout

\begin_layout Standard
Zusätzliche Unterlagen: 
\end_layout

\begin_layout Standard
- Pflichtenheft für die Migration / Entwicklung from scratch für eine webAnwendu
ng (kommt noch) 
\end_layout

\begin_layout Standard
- Betriebskonzept 
\begin_inset CommandInset href
LatexCommand href
target "https://pr2019-all.hashbase.io/konform5.pdf"

\end_inset

 
\end_layout

\begin_layout Standard
- workflow Diagramm 
\begin_inset CommandInset href
LatexCommand href
target "https://pr2019-all.hashbase.io/schnittstelle_bauplanung.svg"

\end_inset

 
\end_layout

\end_body
\end_document
